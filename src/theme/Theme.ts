import { xo } from '../observable/xo';
import { SettingKeys } from '../data';
import { System } from '../system';

/** Theme manager */
export class Theme {
  /** Dark or light theme */
  @xo.observable
  public darkTheme: boolean;

  /** Dark footer and header */
  @xo.observable
  public darkFooterHeader: boolean;

  /** Blue background */
  @xo.observable
  public blueBackground: boolean;

  /** Header and footer color */
  @xo.computed
  public get headerFooterColor(): string | undefined {
    if (!this.darkFooterHeader) return undefined;
    return this.darkTheme ? 'light' : 'dark';
  }

  /**
   * @constructor
   */
  constructor() {
    const prefersDark = window.matchMedia('(prefers-color-scheme: dark)');
    if (prefersDark) {
      this.darkTheme = prefersDark.matches;
      prefersDark.addListener((mediaQuery) => {
        this.darkTheme = mediaQuery.matches;
        this.applyTheme();
      });
    }
    this.darkFooterHeader = true;
    this.blueBackground = true;
    this.applyTheme();
  }

  /**
   * Toggles light/dark theme
   */
  @xo.action
  public toggleTheme(darkTheme: boolean) {
    this.setTheme(darkTheme);
    this.applyTheme();
    this.saveTheme();
  }

  /**
   * Toggles dark footer header
   */
  @xo.action
  public toggleFooterHeader(enabled: boolean) {
    this.darkFooterHeader = enabled;
    this.saveTheme();
  }

  /**
   * Toggles blue background theme
   */
  @xo.action
  public toggleBackground(enabled: boolean) {
    this.blueBackground = enabled;
    this.saveTheme();
  }

  /** Sets theme */
  @xo.action
  public setTheme(darkTheme: boolean) {
    this.darkTheme = darkTheme;
  }

  /**
   * Applies current theme class
   */
  public applyTheme() {
    const body = document.getElementsByTagName('body')[0];
    body.className = this.darkTheme ? 'dark' : '';
  }

  /**
   * Saves theme to storage
   */
  public async saveTheme() {
    await System.userSettings.set(SettingKeys.theme, {
      darkTheme: this.darkTheme,
      darkFooterHeader: this.darkFooterHeader,
      blueBackground: this.blueBackground,
    });
  }

  /**
   * Loads theme from storage
   */
  public async loadTheme() {
    const theme = await System.userSettings.get(SettingKeys.theme);
    if (theme == null) return;
    this.setTheme(theme.darkTheme);
    this.applyTheme();
    this.darkFooterHeader = theme.darkFooterHeader;
    this.blueBackground = theme.blueBackground;
  }
}
