import { ViewProps } from "./ViewProps";

/** Route match interface */
export interface RouteMatch<T> {
  /** Route params */
  params: T;
}

/** Route props interface */
export interface RouteProps<TParams, TViewModel> extends ViewProps<TViewModel> {
  /** Route match */
  match: RouteMatch<TParams>;
}
