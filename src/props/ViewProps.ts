/** Basic view props exposing view model */
export interface ViewProps<T> {
  /** View model reference */
  viewModel: T;
}
