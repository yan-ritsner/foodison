import { PageInfo, PageViewModel } from '../pages/page';
import { Theme } from './../theme';
import {
  IUserSettingsService,
  IGoogleAnalyticsService,
  ISubscribeService,
} from '../services';
import { ServiceType } from '../enums';
import { User } from './User';

/** Global system functions and services */
export class System {
  /** Singleton services map by id */
  private static services: Map<ServiceType, any> = new Map();
  /** Transient service factories by id */
  private static serviceFactories: Map<ServiceType, { new () }> = new Map();

  /** Application pages list */
  public static pages: PageInfo[] = [];
  /** Active page view model */
  public static activePage: PageViewModel;

  /** User info */
  public static user: User = new User();
  /** Theme manager */
  public static theme: Theme = new Theme();

  /** User settings service */
  public static get userSettings(): IUserSettingsService {
    return this.getService<IUserSettingsService>(ServiceType.UserSettings);
  }

  /** Subscription service */
  public static get subscription(): ISubscribeService {
    return this.getService<ISubscribeService>(ServiceType.Subscribe);
  }

  /** Google analytics service */
  public static get googleAnalytics(): IGoogleAnalyticsService {
    return this.getService<IGoogleAnalyticsService>(
      ServiceType.GoogleAnalytics
    );
  }

  /**
   * Default index page
   * */
  public static get defaultPage(): PageInfo | null {
    const page = this.pages.find((page) => page.default);
    return page ? page : null;
  }
  /**
   * Register singleton system service
   * @param type - service type
   * @param service - service instance
   */
  public static registerService(type: ServiceType, service: any): void {
    this.services.set(type, service);
  }

  /**
   * Register transient system service factory
   * @param type - service type
   * @param factory - service factory
   */
  public static registerServiceFactory(
    type: ServiceType,
    factory: { new () }
  ): void {
    this.serviceFactories.set(type, factory);
  }

  /**
   * Gets service instance by type
   * @param type - service type
   */
  public static getService<T>(type: ServiceType): T {
    const service = this.services.get(type);
    if (service != null) return service;
    const factory = this.serviceFactories.get(type);
    if (factory) return new factory();
    else throw new Error(`Service type:${type} not found`);
  }

  /**
   * Register application page
   * @param page - page to register
   */
  public static registerPage<T extends PageViewModel = PageViewModel>(
    page: PageInfo<T>
  ) {
    this.pages.push(page);
  }
}
