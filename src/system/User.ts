import * as firebase from 'firebase/app';
import 'firebase/auth';
import { xo } from '../observable';
import { Anthropometry, SettingKeys } from '../data';
import { System } from './System';

/** User information */
export class User {
  /** User is initialized  */
  @xo.observable
  public isInitialized: boolean;
  /** User is authenticated */
  @xo.observable
  public isAuthenticated: boolean;
  /** User is verified */
  @xo.observable
  public isVerified: boolean;
  /** User has an active subscription */
  @xo.observable
  public isSubscribed: boolean;
  /** User settings are loaded */
  @xo.observable
  public settingsLoaded: boolean;

  /** User info */
  @xo.observable
  public info: firebase.UserInfo;
  /** User anthropometry*/
  @xo.observable
  public anthropometry: Anthropometry;

  /** Auto sync activity with health repo */
  @xo.observable
  public syncActivity: boolean;
  /** Auto sync activity was requested */
  @xo.observable
  public syncRequested: boolean;
  /** Show anthropometry alert */
  @xo.observable
  public showAnthropometryAlert: boolean;

  /**
   * @constructor
   */
  constructor() {
    this.isInitialized = false;
    this.isAuthenticated = false;
    this.isVerified = false;
    this.isSubscribed = false;
    this.settingsLoaded = false;
    this.syncActivity = false;
    this.syncRequested = false;
    this.showAnthropometryAlert = true;
  }

  /** Sets initialized state */
  @xo.action
  public setInitialized(value: boolean) {
    this.isInitialized = value;
  }

  /** Sets authenticated state */
  @xo.action
  public setAuthenticated(value: boolean) {
    this.isAuthenticated = value;
  }

  /** Sets verified state */
  @xo.action
  public setVerified(value: boolean) {
    this.isVerified = value;
  }

  /** Sets subscribed state */
  @xo.action
  public setSubscribed(value: boolean) {
    this.isSubscribed = value;
  }

  /** Sets settings loaded state */
  @xo.action
  public setSettingsLoaded(value: boolean) {
    this.settingsLoaded = value;
  }

  /** Sets user info */
  @xo.action
  public setUserInfo(info: firebase.UserInfo) {
    this.info = info;
  }

  /** Sets sync activity */
  @xo.action
  public toggleSyncActivity(syncActivity?: boolean) {
    this.syncActivity = syncActivity == null ? !syncActivity : syncActivity;
    this.saveSyncActivity();
  }

  /** Sets sync requested */
  @xo.action
  public setSyncRequested() {
    this.syncRequested = true;
    this.saveSyncRequested();
  }

  /** Sets user info */
  @xo.action
  public setAnthropometry(anthropometry: Anthropometry) {
    this.anthropometry = anthropometry;
    this.saveAnthropometry();
  }

  /** Sign in user */
  @xo.action
  public signIn(user: firebase.User) {
    this.setInitialized(true);
    if (user) {
      this.setUserInfo(user);
      this.setAuthenticated(true);
      this.loadSettings();
      if (user.emailVerified) {
        this.setVerified(true);
        this.checkSubscription();
        return true;
      } else {
        this.setVerified(false);
        user.sendEmailVerification();
      }
    } else {
      this.setAuthenticated(false);
    }
    return false;
  }

  /** Sign outs user */
  @xo.action
  public signOut() {
    firebase.auth().signOut();
    this.setInitialized(false);
    this.setAuthenticated(false);
    this.setVerified(false);
  }

  /** Loads user settings */
  public async loadSettings() {
    await this.loadSyncActivity();
    await this.loadSyncRequested();
    await this.loadAnthropometry();
    await System.theme.loadTheme();
    this.setSettingsLoaded(true);
  }

  /**
   * Saves sync activity to the storage
   */
  private async saveSyncActivity() {
    await System.userSettings.set(SettingKeys.syncActivity, this.syncActivity);
  }

  /**
   * Loads sync activity from the storage
   */
  private async loadSyncActivity() {
    const syncActivity = await System.userSettings.get<boolean>(
      SettingKeys.syncActivity
    );
    if (syncActivity != null) {
      this.syncActivity = syncActivity;
    }
  }

  /**
   * Saves sync activity requested to the storage
   */
  private async saveSyncRequested() {
    await System.userSettings.set(
      SettingKeys.syncRequested,
      this.syncRequested
    );
  }

  /**
   * Loads sync activity requested from the storage
   */
  private async loadSyncRequested() {
    const syncRequested = await System.userSettings.get<boolean>(
      SettingKeys.syncRequested
    );
    if (syncRequested != null) {
      this.syncRequested = syncRequested;
    }
  }

  /**
   * Saves anthropometry to the storage
   */
  private async saveAnthropometry() {
    await System.userSettings.set(
      SettingKeys.anthropometry,
      this.anthropometry
    );
  }

  /**
   * Loads anthropometry from the storage
   */
  private async loadAnthropometry() {
    const anthropometry = await System.userSettings.get<Anthropometry>(
      SettingKeys.anthropometry
    );
    if (anthropometry != null) {
      this.anthropometry = new Anthropometry(anthropometry);
    }
  }

  /** Checks for subscription updated  */
  private checkSubscription() {
    System.subscription.onSubscriptionUpdate((subscription) => {
      this.setSubscribed(subscription != null && subscription.isActive);
    });
  }
}
