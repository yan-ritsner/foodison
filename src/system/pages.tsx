import React from 'react';
import {
  appsOutline,
  bodyOutline,
  bicycleOutline,
  fastFoodOutline,
  medalOutline,
  trashBinOutline,
  refreshCircleOutline,
} from 'ionicons/icons';
import { System } from './System';
import {
  AnthropometryView,
  AnthropometryViewModel,
} from '../pages/anthropometry';
import { MealsView, MealsViewModel, MealsToolbar } from '../pages/meals';
import { DashboardView, DashboardViewModel } from '../pages/dashboard';
import {
  ActivityToolbar,
  ActivityView,
  ActivityViewModel,
} from '../pages/activity';
import { ResultsView, ResultsViewModel } from '../pages/results';
import { CleanupViewModel, CleanupView } from '../pages/cleanup';
import { SubscribeViewModel, SubscribeView } from '../pages/subscribe';

/** Application pages registration */
System.registerPage({
  route: '/subscriptions',
  name: 'Subscriptions',
  tab: false,
  icon: refreshCircleOutline,
  viewModel: () => new SubscribeViewModel(),
  view: (viewModel) => <SubscribeView viewModel={viewModel} />,
});

System.registerPage({
  route: '/anthropometry',
  name: 'Anthropometry',
  tab: false,
  icon: bodyOutline,
  viewModel: () => new AnthropometryViewModel(),
  view: (viewModel) => <AnthropometryView viewModel={viewModel} />,
});

System.registerPage({
  route: '/cleanup',
  name: 'Clean Up',
  tab: false,
  icon: trashBinOutline,
  viewModel: () => new CleanupViewModel(),
  view: (viewModel) => <CleanupView viewModel={viewModel} />,
});

System.registerPage({
  route: '/dashboard',
  name: 'Dashboard',
  tab: true,
  default: true,
  icon: appsOutline,
  viewModel: () => new DashboardViewModel(),
  view: (viewModel) => <DashboardView viewModel={viewModel} />,
});

System.registerPage({
  route: '/meals',
  name: 'Meals',
  tab: true,
  icon: fastFoodOutline,
  viewModel: () => new MealsViewModel(),
  view: (viewModel) => <MealsView viewModel={viewModel} />,
  toolbar: (viewModel) => <MealsToolbar viewModel={viewModel} />,
});

System.registerPage({
  route: '/activity',
  name: 'Activity',
  tab: true,
  icon: bicycleOutline,
  viewModel: () => new ActivityViewModel(),
  view: (viewModel) => <ActivityView viewModel={viewModel} />,
  toolbar: (viewModel) => <ActivityToolbar viewModel={viewModel} />,
});

System.registerPage({
  route: '/results',
  name: 'Results',
  tab: true,
  icon: medalOutline,
  viewModel: () => new ResultsViewModel(),
  view: (viewModel) => <ResultsView viewModel={viewModel} />,
});
