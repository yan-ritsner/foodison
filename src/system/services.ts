import { System } from './System';
import { ServiceType } from '../enums';
import {
  UserSettingsService,
  PhotoStorageService,
  PhotoClassifierService,
  FoodDataService,
  FoodNutritionService,
  FoodPredictionService,
  ActivityDataService,
  DataInputService,
  GoogleAnalyticsService,
  SubscribeService,
} from '../services';

/** System services registration */
System.registerServiceFactory(ServiceType.UserSettings, UserSettingsService);

System.registerServiceFactory(ServiceType.PhotoStorage, PhotoStorageService);

System.registerServiceFactory(
  ServiceType.PhotoClassifier,
  PhotoClassifierService
);

System.registerServiceFactory(
  ServiceType.FoodPrediction,
  FoodPredictionService
);

System.registerServiceFactory(ServiceType.FoodNutrition, FoodNutritionService);

System.registerServiceFactory(ServiceType.FoodData, FoodDataService);

System.registerServiceFactory(ServiceType.ActivityData, ActivityDataService);

System.registerServiceFactory(ServiceType.DataInput, DataInputService);

System.registerServiceFactory(
  ServiceType.GoogleAnalytics,
  GoogleAnalyticsService
);

System.registerServiceFactory(ServiceType.Subscribe, SubscribeService);
