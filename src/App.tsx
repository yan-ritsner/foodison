import React from 'react';
import { Redirect, Route, RouteComponentProps } from 'react-router-dom';
import {
  IonApp,
  IonIcon,
  IonLabel,
  IonRouterOutlet,
  IonTabBar,
  IonTabButton,
  IonTabs,
  setupConfig,
} from '@ionic/react';
import { IonReactRouter } from '@ionic/react-router';

/* Core CSS required for Ionic components to work properly */
import '@ionic/react/css/core.css';

/* Basic CSS for apps built with Ionic */
import '@ionic/react/css/normalize.css';
import '@ionic/react/css/structure.css';
import '@ionic/react/css/typography.css';

/* Optional CSS utils that can be commented out */
import '@ionic/react/css/padding.css';
import '@ionic/react/css/float-elements.css';
import '@ionic/react/css/text-alignment.css';
import '@ionic/react/css/text-transformation.css';
import '@ionic/react/css/flex-utils.css';
import '@ionic/react/css/display.css';

/** Driver js */
import 'driver.js/dist/driver.min.css';

/* Theme variables */
import './theme/variables.css';
/* Calendar styles variables */
import './theme/calendar.css';
/** Firebase ui */
import './theme/firebaseui.css';
/** App styles */
import './theme/app.css';

import { xo } from './observable';
import { System } from './system';
import { PageContainer, PageInfo, PageViewModel } from './pages/page';
import { Login } from './pages/login';

setupConfig({ mode: 'md' });

@xo.observer
export class App extends React.Component {
  render() {
    /** Page routes */
    const routes = this.renderRoutes();
    /** Tabs for pages */
    const tabs = this.renderTabs();
    /** Toolbar color and visibility */
    const toolbarColor = System.theme.headerFooterColor;
    const toolbarDisplay =
      System.user.isAuthenticated &&
      System.user.isVerified &&
      System.user.isSubscribed
        ? 'flex'
        : 'none';
    /** Render app */
    return this.renderApp(routes, toolbarColor, toolbarDisplay, tabs);
  }

  /** Renders app */
  private renderApp(
    routes: JSX.Element[],
    toolbarColor: string,
    toolbarDisplay: string,
    tabs: JSX.Element[]
  ) {
    return (
      <IonApp className={System.theme.blueBackground ? 'blue-background' : ''}>
        <IonReactRouter>
          <IonTabs>
            <IonRouterOutlet animated={false}>{routes}</IonRouterOutlet>
            <IonTabBar
              key={toolbarColor}
              color={toolbarColor}
              style={{ display: toolbarDisplay }}
              slot={'bottom'}
            >
              {tabs}
            </IonTabBar>
          </IonTabs>
        </IonReactRouter>
      </IonApp>
    );
  }

  /** Renders app routes */
  private renderRoutes() {
    const routes = System.pages.map((page) => (
      <Route
        exact={true}
        path={page.route}
        key={page.route}
        render={(props) => this.renderPage(props, page)}
      ></Route>
    ));

    /** Login route */
    routes.push(
      <Route exact={true} key="/login" path="/login" component={Login} />
    );

    /** Default page route */
    if (System.defaultPage) {
      routes.push(
        <Route
          exact={true}
          key="/"
          path="/"
          render={() => <Redirect to={System.defaultPage?.route} />}
        />
      );
    }
    return routes;
  }

  /** Renders app tabs */
  private renderTabs() {
    return System.pages
      .filter((page) => page.tab)
      .map((page) => (
        <IonTabButton key={page.route} tab={page.name} href={page.route}>
          <IonIcon icon={page.icon} />
          <IonLabel>{page.name}</IonLabel>
        </IonTabButton>
      ));
  }

  /** Renders Page */
  private renderPage(
    props: RouteComponentProps,
    page: PageInfo<PageViewModel>
  ) {
    if (!System.user.isAuthenticated || !System.user.isVerified) {
      return this.renderLoginRedirect(props);
    }
    if (
      !System.user.isSubscribed &&
      props?.location?.pathname !== '/subscriptions'
    ) {
      return this.renderSubscribeRedirect(props);
    }
    return this.renderPageContainer(props, page);
  }

  /** Renders page container */
  private renderPageContainer(
    props: RouteComponentProps,
    page: PageInfo<PageViewModel>
  ) {
    return (
      <PageContainer
        info={page}
        location={props?.location?.pathname}
        search={new URLSearchParams(props?.location?.search)}
        params={props?.match?.params}
        history={props?.history}
      />
    );
  }

  /** Renders login redirect */
  private renderLoginRedirect(props: RouteComponentProps) {
    return (
      <Redirect
        to={{
          pathname: '/login',
          state: props?.location?.state ?? {
            from: props?.location?.pathname + props?.location?.search,
          },
        }}
      />
    );
  }

  /** Renders login redirect */
  private renderSubscribeRedirect(props: RouteComponentProps) {
    return (
      <Redirect
        to={{
          pathname: '/subscriptions',
          state: props?.location?.state ?? {
            from: props?.location?.pathname + props?.location?.search,
          },
        }}
      />
    );
  }
}

export default App;
