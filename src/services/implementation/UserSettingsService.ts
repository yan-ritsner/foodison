import { Plugins } from '@capacitor/core';
import { IUserSettingsService } from '../interfaces';

/** User settings service implementation */
export class UserSettingsService implements IUserSettingsService {
  /**
   * Sets user setting
   * @param key - settings key
   * @param value - setting value
   */
  public set(key: string, value: any): Promise<void> {
    const { Storage } = Plugins;
    return Storage.set({
      key,
      value: JSON.stringify(value),
    });
  }
  /**
   * Gets user setting
   * @param key - setting key
   */
  public async get<T = any>(key: string): Promise<T> {
    const { Storage } = Plugins;
    const value = await Storage.get({
      key,
    });
    if (value?.value == null) return null;
    return JSON.parse(value.value) as T;
  }
  /**
   * Removes user setting
   * @param key - setting key
   */
  remove(key: string): Promise<void> {
    const { Storage } = Plugins;
    return Storage.remove({ key });
  }
}
