import { Plugins } from '@capacitor/core';
import { isSameDay, startOfDay } from 'date-fns';
import { xo, ObservableArray } from '../../observable';
import { IFoodDataService } from '../interfaces';
import { FoodData, FoodDailyData, SettingKeys } from '../../data';

/** Food data service implementation */
export class FoodDataService implements IFoodDataService {
  /** Key in the storage */
  private static get storageKey(): string {
    return SettingKeys.foodData;
  }
  /** Max id in the storage */
  private static storageId: number = 0;

  /** Data ids array */
  public dataIds: number[];
  /** Data observable array */
  public data: ObservableArray<FoodData>;
  /** Data map by id */
  public map: Map<number, FoodData>;

  /** [Lazy Load] chunk size*/
  private chunkSize: number = 20;

  /**
   * @constructor
   */
  public constructor() {
    this.dataIds = [];
    this.data = new ObservableArray();
    this.map = new Map();
  }

  /** Returns food data splitted into days */
  @xo.computed
  public get dailyData(): FoodDailyData[] {
    const dailyData: FoodDailyData[] = [];
    let currentDay: FoodDailyData = null;
    this.data
      .sort((d1, d2) => d2.timestamp.valueOf() - d1.timestamp.valueOf())
      .forEach((data, index) => {
        const prevData = this.data.get(index - 1);
        if (
          prevData == null ||
          !isSameDay(prevData.timestamp, data.timestamp)
        ) {
          currentDay = {
            timestamp: startOfDay(data.timestamp),
            calories: 0,
            data: [],
          };
          dailyData.push(currentDay);
        }
        currentDay.data.push(data);
        currentDay.calories += data.calories;
      });
    return dailyData;
  }

  /** Returns food data splitted into days as map */
  @xo.computed
  public get dailyDataMap(): Map<number, FoodDailyData> {
    const dailyDataMap: Map<number, FoodDailyData> = new Map();
    this.dailyData.forEach((data) => {
      dailyDataMap.set(data.timestamp.valueOf(), data);
    });
    return dailyDataMap;
  }

  /** Gets all data from storage */
  public async get(): Promise<void> {
    this.dataIds = await this.getDataIds();
    const data = await this.getData();

    this.data.assign(data);
    data.forEach((d) => this.map.set(d.id, d));
  }

  /** Gets initial data from storage */
  public async getInitial(): Promise<void> {
    this.dataIds = await this.getDataIds();
    const data = await this.getData(0, this.chunkSize);

    this.data.assign(data);
    data.forEach((d) => this.map.set(d.id, d));
  }

  /** Has more data to load  */
  public hasMore(): boolean {
    return this.data.length < this.dataIds.length;
  }

  /** Smallest date timestamp */
  public startDate(): Date {
    return this.data.length > 0
      ? this.data.get(this.data.length - 1).timestamp
      : new Date();
  }

  /** Gets more data from storage */
  public async getMore(): Promise<void> {
    if (!this.hasMore()) return;
    const data = await this.getData(this.data.length, this.chunkSize);

    this.data.push(...data);
    data.forEach((d) => this.map.set(d.id, d));
  }

  /** Gets data by id */
  public getById(id: number): FoodData | null {
    const data = this.map.get(id);
    return data ? data : null;
  }

  /** Add new data to the storage */
  public async add(data: FoodData): Promise<void> {
    data.id = FoodDataService.storageId;
    FoodDataService.storageId += 1;

    this.dataIds.unshift(data.id);
    this.data.unshift(data);
    this.map.set(data.id, data);

    await this.saveData(data);
    await this.saveDataIds();
  }

  /** Update existing data in the storage */
  public async update(data: FoodData): Promise<void> {
    const index = this.data.findIndex((d) => d.id === data.id);
    if (index < 0) return;

    this.data.set(index, data);
    this.map.set(data.id, data);

    await this.saveData(data);
  }

  /** Deletes existing data from the storage */
  public async delete(data: FoodData): Promise<void> {
    const index = this.data.findIndex((d) => d.id === data.id);
    if (index < 0) return;

    this.dataIds.splice(index, 1);
    this.data.splice(index, 1);
    this.map.delete(data.id);

    await this.deleteData(data);
    await this.saveDataIds();
  }

  /** Asynchronously gets food data ids from the storage */
  protected async getDataIds() {
    const { Storage } = Plugins;
    // get data ids array
    const dataIdsValue = await Storage.get({
      key: FoodDataService.storageKey,
    });
    const dataIdsString = dataIdsValue ? dataIdsValue.value : null;
    const dataIds = (dataIdsString
      ? JSON.parse(dataIdsString)
      : []) as number[];
    if (dataIds && dataIds.length > 0) {
      FoodDataService.storageId = Math.max(...dataIds) + 1;
    }
    return dataIds;
  }

  /** Asynchronously gets food data from the storage */
  protected async getData(start?: number, count?: number) {
    const { Storage } = Plugins;
    const startIndex = start != null ? start : 0;
    const endIndex = startIndex + (count != null ? count : this.dataIds.length);
    const dataIds = this.dataIds.slice(startIndex, endIndex);
    // get all food data by id
    const data = await Promise.all(
      dataIds.map(async (id) => {
        const dataValue = await Storage.get({
          key: `${FoodDataService.storageKey}#${id}`,
        });
        const dataString = dataValue ? dataValue.value : null;
        const dataJson = dataString ? JSON.parse(dataString) : null;
        const dataObj = new FoodData(dataJson);
        return dataObj;
      })
    );
    return data;
  }

  /** Asynchronously saves food data to the storage */
  protected async saveData(data: FoodData) {
    const { Storage } = Plugins;
    const dataValue = data.data();
    const dataString = JSON.stringify(dataValue);
    await Storage.set({
      key: `${FoodDataService.storageKey}#${data.id}`,
      value: dataString,
    });
  }

  /** Asynchronously deleted food data from the storage */
  protected async deleteData(data: FoodData) {
    const { Storage } = Plugins;
    await Storage.remove({
      key: `${FoodDataService.storageKey}#${data.id}`,
    });
  }

  /** Asynchronously saves all data ids to the storage */
  protected async saveDataIds() {
    const { Storage } = Plugins;
    const dataIds = this.data.map((data) => data.id);
    const dataIdsString = JSON.stringify(dataIds);
    await Storage.set({
      key: FoodDataService.storageKey,
      value: dataIdsString,
    });
  }
}
