import { IDataInputService } from '../interfaces/IDataInputService';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { SpeechRecognition } from '@ionic-native/speech-recognition';

/** Data input service interface */
export class DataInputService implements IDataInputService {
  /** Scans and return barcode */
  async scanBarCode(): Promise<string> {
    const data = await BarcodeScanner.scan().catch((e) => {
      console.log(e);
    });
    if (!data) return null;
    return data.text;
  }

  /* Recognize speech and return results */
  async recognizeSpeech(
    onMatch: (matches: string[]) => void,
    onError: (error: any) => void
  ): Promise<void> {
    // Check feature available
    const isAvailable = await SpeechRecognition.isRecognitionAvailable().catch(
      (e) => {
        console.log(e);
      }
    );
    if (!isAvailable) {
      console.log('Speech Recognition is not available ');
      return;
    }

    // Request permissions
    await SpeechRecognition.requestPermission().then(
      () => console.log('Permission Granted'),
      () => console.log('Permission Denied')
    );

    // Check permission
    const hasPermission = await SpeechRecognition.hasPermission().catch((e) => {
      console.log(e);
    });
    if (!hasPermission) {
      console.log('Speech Recognition user has no permission ');
      return;
    }

    SpeechRecognition.startListening().subscribe(
      (matches: string[]) => onMatch(matches),
      (error) => onError(error)
    );
  }
}
