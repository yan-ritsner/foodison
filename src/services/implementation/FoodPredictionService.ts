import * as Clarifai from 'clarifai';
import { IFoodPredictionService } from '../interfaces';
import { FoodConcept } from '../../data';

/** Food prediction service implementation */
export class FoodPredictionService implements IFoodPredictionService {
  /** Api key */
  private apiKey: string = 'ca63c3a53e6946a2a97f8e440509d7fc';
  /** App reference */
  private api: Clarifai.App;

  /**
   * @constructor
   */
  constructor() {
    this.api = new Clarifai.App({
      apiKey: this.apiKey,
    });
  }

  /** Predicts foods and its ingredients by base 64 encoded image */
  public async predict(imageBase64: string): Promise<FoodConcept[]> {
    let response = await this.api.models
      .predict(Clarifai.FOOD_MODEL, {
        base64: imageBase64,
      })
      .catch((e) => {
        console.log(e);
      });
    if (
      !response ||
      !response.outputs ||
      !response.outputs[0] ||
      !response.outputs[0].data
    )
      return null;
    const concepts = response.outputs[0].data.concepts as FoodConcept[];
    if (concepts != null) concepts.forEach((c, i) => (c.index = i));
    return concepts;
  }
}
