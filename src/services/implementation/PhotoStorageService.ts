import {
  Plugins,
  FilesystemDirectory,
  CameraResultType,
  CameraSource,
  CameraPhoto,
  Capacitor,
} from '@capacitor/core';
import { isPlatform } from '@ionic/react';
import { IPhotoStorageService } from '../interfaces';
import { Photo, SettingKeys } from '../../data';
import { ObservableArray } from '../../observable';

/** Photo storage service implementation */
export class PhotoStorageService implements IPhotoStorageService {
  /** Key in the storage */
  private static get storageKey(): string {
    return SettingKeys.photoStorage;
  }
  /** Max id in the storage */
  private static storageId: number = 0;

  /** Data ids observable array */
  public data: ObservableArray<number>;
  /** Data ids set */
  public set: Set<number>;

  /**
   * @constructor
   */
  constructor() {
    this.data = new ObservableArray();
    this.set = new Set();
  }

  /** Gets all photo ids from storage */
  public async get(): Promise<number[]> {
    const photosIds = await this.getPhotosIds();

    this.data.assign(photosIds);
    photosIds.forEach((id) => this.set.add(id));

    return photosIds;
  }

  /** Gets photo by id */
  public async getById(id: number): Promise<Photo | null> {
    if (!this.set.has(id)) return null;
    const photo = await this.getPhotoInfo(id);
    if (photo == null) return null;
    await this.getPhotoData(photo);
    return photo;
  }

  /** Adds new photo to the storage */
  public async add(): Promise<Photo | null> {
    const photo = await this.getPhoto();
    if (photo == null) return null;

    photo.id = PhotoStorageService.storageId;
    PhotoStorageService.storageId += 1;

    this.data.push(photo.id);
    this.set.add(photo.id);

    await this.savePhotoInfo(photo);
    await this.savePhotoIds();

    return photo;
  }

  /** Deletes existing photo from the storage */
  public async delete(photo: Photo): Promise<void> {
    if (!this.set.has(photo.id)) return;

    this.data.remove(photo.id);
    this.set.delete(photo.id);

    await this.deletePhotoInfo(photo);
    await this.savePhotoIds();
  }

  /** Takes new photo, stores it and returns */
  protected async getPhoto(): Promise<Photo | null> {
    const cameraPhoto = await this.takePhoto();
    if (!cameraPhoto) return null;

    const photo = await this.savePhoto(cameraPhoto);
    return photo;
  }

  /** Asynchronously gets photos ids from the storage */
  private async getPhotosIds() {
    const { Storage } = Plugins;
    // get photo ids array
    const photoIdsValue = await Storage.get({
      key: PhotoStorageService.storageKey,
    });
    const photoIdsString = photoIdsValue ? photoIdsValue.value : null;
    const photoIds = (photoIdsString
      ? JSON.parse(photoIdsString)
      : []) as number[];
    if (photoIds && photoIds.length > 0) {
      PhotoStorageService.storageId = Math.max(...photoIds) + 1;
    }
    return photoIds;
  }

  /** Asynchronously gets photo info from the storage */
  private async getPhotoInfo(id: number) {
    const { Storage } = Plugins;
    // get photos by id
    const photoValue = await Storage.get({
      key: `${PhotoStorageService.storageKey}#${id}`,
    });
    const photoString = photoValue ? photoValue.value : null;
    const photoJson = photoString ? JSON.parse(photoString) : null;
    const photo = new Photo(photoJson);
    return photo;
  }

  /** Asynchronously saves photo info to the storage */
  private async savePhotoInfo(photo: Photo) {
    const { Storage } = Plugins;
    // Don't save the base64 representation of the photo data,
    // since it's already saved on the Filesystem
    let photoValue = photo.data(this.isHybridPlatform());
    const photoString = JSON.stringify(photoValue);
    await Storage.set({
      key: `${PhotoStorageService.storageKey}#${photo.id}`,
      value: photoString,
    });
  }

  /** Asynchronously deleted photo info to the storage */
  private async deletePhotoInfo(photo: Photo) {
    const { Storage } = Plugins;
    await Storage.remove({
      key: `${PhotoStorageService.storageKey}#${photo.id}`,
    });
  }

  /** Asynchronously saves photo ids to the storage */
  private async savePhotoIds() {
    const { Storage } = Plugins;
    const photoIds = this.data.data;
    const photoIdsString = JSON.stringify(photoIds);
    await Storage.set({
      key: PhotoStorageService.storageKey,
      value: photoIdsString,
    });
  }

  /** Asynchronously gets photos data url if running on the web */
  private async getPhotoData(photo: Photo) {
    const { Filesystem } = Plugins;
    if (this.isHybridPlatform()) return;
    // If running on the web...
    const file = await Filesystem.readFile({
      path: photo.filePath,
      directory: FilesystemDirectory.Data,
    }).catch((e) => {
      console.log(e);
    });
    // Web platform only: Save the photo into the base64 field
    photo.base64 = file ? `data:image/jpeg;base64,${file.data}` : '';
  }

  /** Asynchronously takes new camera photo */
  private async takePhoto() {
    const { Camera } = Plugins;
    const cameraPhoto = await Camera.getPhoto({
      resultType: CameraResultType.Uri,
      source: CameraSource.Camera,
      quality: 50,
    }).catch((e) => {
      console.log(e);
    });
    return cameraPhoto;
  }

  /** Asynchronously saves new camera photo */
  private async savePhoto(cameraPhoto: CameraPhoto): Promise<Photo> {
    const { Filesystem } = Plugins;
    const timestamp = new Date();
    const fileName = timestamp.getTime() + '.jpeg';

    let base64Data: string;

    // "hybrid" will detect Cordova or Capacitor;
    if (this.isHybridPlatform()) {
      const file = await Filesystem.readFile({
        path: cameraPhoto.path!,
      });
      base64Data = file.data;
    } else {
      base64Data = await this.base64FromPath(cameraPhoto.webPath!);
    }

    const savedFile = await Filesystem.writeFile({
      path: fileName,
      data: base64Data,
      directory: FilesystemDirectory.Data,
    });

    const photo = new Photo();
    photo.id = 0;
    photo.timestamp = timestamp;
    photo.base64 = base64Data;

    if (this.isHybridPlatform()) {
      // Display the new image by rewriting the 'file://' path to HTTP
      // Details: https://ionicframework.com/docs/building/webview#file-protocol
      photo.filePath = savedFile.uri;
      photo.webviewPath = Capacitor.convertFileSrc(savedFile.uri);
    } else {
      // Use webPath to display the new image instead of base64 since it's
      // already loaded into memory
      photo.filePath = fileName;
      photo.webviewPath = cameraPhoto.webPath;
    }
    return photo;
  }

  /** Returns base 64 encoded file data */
  protected async base64FromPath(
    path: string,
    noPrefix: boolean = false,
    prefix = 'data:image/jpeg;base64,'
  ) {
    const response = await fetch(path);
    const blob = await response.blob();

    return new Promise<string>((resolve, reject) => {
      const reader = new FileReader();
      reader.onerror = reject;
      reader.onload = () => {
        let result = reader.result;
        if (typeof result === 'string') {
          if (noPrefix && result.startsWith(prefix)) {
            result = result.substr(
              prefix.length,
              result.length - prefix.length
            );
          }
          resolve(result);
        } else {
          reject('method did not return a string');
        }
      };
      reader.readAsDataURL(blob);
    });
  }

  /** Return true if running on hybrid platform (non web) */
  private isHybridPlatform() {
    return isPlatform('hybrid');
  }
}
