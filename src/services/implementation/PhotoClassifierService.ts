import * as tf from '@tensorflow/tfjs';
import { IPhotoClassifierService } from '../interfaces';
import { Prediction } from '../../data';

/** Photo classifier service implementation */
export class PhotoClassifierService implements IPhotoClassifierService {
  /** Models map bu url */
  private static models: Map<string, tf.LayersModel> = new Map();
  /** Model loading promises by url */
  private static loading: Map<string, Promise<tf.LayersModel>> = new Map();

  /**
   * Loads tf model asynchronously
   * @param url - model url
   */
  public async loadModel(url: string): Promise<void> {
    let model = PhotoClassifierService.models.get(url);
    if (model != null) return;

    console.log('model loading started');
    let loading = PhotoClassifierService.loading.get(url);

    if (!loading) {
      loading = tf.loadLayersModel(url);
      PhotoClassifierService.loading.set(url, loading);
    }

    model = await loading;
    PhotoClassifierService.loading.delete(url);
    PhotoClassifierService.models.set(url, model);

    console.log('model loading finished');
  }

  /**
   * Get pre loaded tf model or loads and returns new model
   * @param url - model url
   */
  public async getModel(url: string): Promise<tf.LayersModel> {
    const model = PhotoClassifierService.models.get(url);
    if (model) return model;

    const loading = PhotoClassifierService.loading.get(url);
    if (loading) {
      return await loading;
    } else {
      await this.loadModel(url);
      return PhotoClassifierService.models.get(url);
    }
  }
  /**
   * Classifies image using specified tf model and classes map
   * @param model - tf model
   * @param classes - classes map, class id - class name map
   * @param image - html image element
   */
  public async classifyImage(
    model: tf.LayersModel,
    classes: any,
    image: HTMLImageElement
  ): Promise<Prediction[]> {
    console.log('creating input tensor started');

    console.log(`number of tensors:${tf.memory().numTensors}`);

    const pixels = tf.browser.fromPixels(image);

    tf.engine().startScope();

    const tensor = pixels
      .resizeNearestNeighbor([299, 299])
      .toFloat()
      .div(tf.scalar(255.0))
      .expandDims();

    console.log(`number of tensors:${tf.memory().numTensors}`);

    console.log('creating input tensor finished');

    console.log('prediction started');
    const predictions = await (model.predict(tensor) as any).data();
    console.log('prediction finished');

    const top: Prediction[] = Array.from(predictions as Array<number>)
      .map((p, i) => {
        return {
          probability: p,
          className: classes[i],
        };
      })
      .sort((a, b) => {
        return b.probability - a.probability;
      })
      .slice(0, 5);

    console.log(`predictions:${top}`);

    console.log(`number of tensors:${tf.memory().numTensors}`);

    tf.engine().endScope();

    console.log(`number of tensors:${tf.memory().numTensors}`);

    return top;
  }
}
