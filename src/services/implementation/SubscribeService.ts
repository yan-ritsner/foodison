import { PACKAGE_TYPE, Purchases } from '@ionic-native/purchases';
import { OfferData, SubscribeData } from '../../data';
import { OfferPeriod } from '../../enums';
import { ISubscribeService } from '../interfaces';

/** Subscription service implementation */
export class SubscribeService implements ISubscribeService {
  /** Revenuecat api key */
  private ApiKey = 'gSrHJinnvbwuAjSScqxhmirAliWQoJZR';
  /** Revenuecat entitlement identifier */
  private EntitlementIdentifier = 'Premium';

  /**
   * Inits subscription service for current user
   * @param userId - user identifier
   */
  public init(userId: string) {
    Purchases.setDebugLogsEnabled(true);
    Purchases.setup(this.ApiKey, userId);
  }

  /**
   * Returns offerings data
   */
  public async getOfferings(): Promise<OfferData[] | SubscribeData> {
    try {
      const offerings = await Purchases.getOfferings();
      if (!offerings) return [];
      const current = offerings.current;
      if (!current || !current.availablePackages) return [];
      const offers = current.availablePackages.map((p) => {
        let period: OfferPeriod;
        let duration: number;
        switch (p.packageType) {
          case PACKAGE_TYPE.WEEKLY:
            period = OfferPeriod.Week;
            duration = 1;
            break;
          case PACKAGE_TYPE.MONTHLY:
            period = OfferPeriod.Month;
            duration = 1;
            break;
          case PACKAGE_TYPE.TWO_MONTH:
            period = OfferPeriod.Month;
            duration = 2;
            break;
          case PACKAGE_TYPE.THREE_MONTH:
            period = OfferPeriod.Month;
            duration = 3;
            break;
          case PACKAGE_TYPE.SIX_MONTH:
            period = OfferPeriod.Month;
            duration = 6;
            break;
          case PACKAGE_TYPE.ANNUAL:
            period = OfferPeriod.Year;
            duration = 1;
            break;
        }
        return new OfferData(
          duration,
          period,
          p.product?.price,
          p.product?.price_string,
          p
        );
      });
      return offers;
    } catch (e) {
      return new SubscribeData(false, false, null, e);
    }
  }

  /**
   * Purchases package
   * @param offer - offer data
   */
  public async purchasePackage(
    offer: OfferData
  ): Promise<SubscribeData | null> {
    try {
      const { purchaserInfo } = await Purchases.purchasePackage(offer.data);
      const entitlement =
        purchaserInfo?.entitlements?.active[this.EntitlementIdentifier];
      if (!entitlement) return null;
      return new SubscribeData(
        entitlement.isActive,
        entitlement.willRenew,
        entitlement.expirationDate
      );
    } catch (e) {
      return e.userCancelled ? null : new SubscribeData(false, false, null, e);
    }
  }

  /**
   * Gets existing subscription
   */
  public async getSubscription(): Promise<SubscribeData | null> {
    try {
      const purchaserInfo = await Purchases.getPurchaserInfo();
      const entitlement =
        purchaserInfo?.entitlements?.active[this.EntitlementIdentifier];
      if (!entitlement) return null;
      return new SubscribeData(
        entitlement.isActive,
        entitlement.willRenew,
        entitlement.expirationDate
      );
    } catch (e) {
      return new SubscribeData(false, false, null, e);
    }
  }

  /**
   * Subscribes for active subscription updates
   * @param callback - callback to be called on subscription update
   */
  public onSubscriptionUpdate(callback: (data: SubscribeData | null) => void) {
    try {
      Purchases.onPurchaserInfoUpdated().subscribe((purchaserInfo) => {
        const entitlement =
          purchaserInfo?.entitlements?.active[this.EntitlementIdentifier];
        if (!entitlement) return callback(null);
        callback(
          new SubscribeData(
            entitlement.isActive,
            entitlement.willRenew,
            entitlement.expirationDate
          )
        );
      });
    } catch (e) {
      console.log(e);
    }
  }
}
