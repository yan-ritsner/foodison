import axios, { AxiosRequestConfig } from 'axios';
import { IFoodNutritionService } from '../interfaces';
import { NutritionInfo } from '../../data/NutritionInfo';
import { ExerciseInfo } from '../../data/ExerciseInfo';
import { GenderType } from '../../enums';
import { NutritionInstant } from '../../data';

/** Food nutrition and exercise service implementation */
export class FoodNutritionService implements IFoodNutritionService {
  private AppIdHeader: string = 'x-app-id';
  private AppIdValue: string = '9800d887';

  private AppKeyKey: string = 'x-app-key';
  private AppKeyValue: string = 'adc3b40c88b614cfbed26b9779a36482';

  private AppUserHeader: string = 'x-remote-user-id';
  private AppUserValue: string = '0';

  private NutrientsUrl =
    'https://trackapi.nutritionix.com/v2/natural/nutrients';
  private SearchUrl = 'https://trackapi.nutritionix.com/v2/search/item';
  private ExerciseUrl = 'https://trackapi.nutritionix.com/v2/natural/exercise';
  private InstantUrl = 'https://trackapi.nutritionix.com/v2/search/instant';

  /**
   * Gets food nutrition facts for NLP query
   * @param query - food query string (i.e for breakfast i ate 2 eggs, bacon, and french toast)
   * @param lineDelimited - query contains line (\r\n) delimited list
   * @param useRawFoods - include raw foods
   */
  public async getNutritionDataByNlp(
    query: string,
    lineDelimited: boolean = false,
    useRawFoods: boolean = false
  ): Promise<NutritionInfo> {
    const config: AxiosRequestConfig = {
      headers: {
        [this.AppIdHeader]: this.AppIdValue,
        [this.AppKeyKey]: this.AppKeyValue,
        [this.AppUserHeader]: this.AppUserValue,
      },
    };
    const data = {
      query: query,
      line_delimited: lineDelimited,
      use_raw_foods: useRawFoods,
    };
    const response = await axios
      .post<NutritionInfo>(this.NutrientsUrl, data, config)
      .catch((e) => {
        console.log(e);
      });
    return response ? response.data : null;
  }

  /**
   * Gets food nutrition facts for UPC code or By NIX item id
   * @param upc - upc code
   * @param nix_item_id - nix item id
   */
  public async getNutritionDataByUpc(
    upc: string,
    nix_item_id?: string
  ): Promise<NutritionInfo> {
    const config: AxiosRequestConfig = {
      headers: {
        [this.AppIdHeader]: this.AppIdValue,
        [this.AppKeyKey]: this.AppKeyValue,
        [this.AppUserHeader]: this.AppUserValue,
      },
      params: { upc, nix_item_id },
    };
    const response = await axios
      .get<NutritionInfo>(this.SearchUrl, config)
      .catch((e) => {
        console.log(e);
      });
    return response ? response.data : null;
  }

  /**
   * Gets food nutrition facts for UPC code
   * @param upc - upc code
   */
  public async getNutritionInstant(query: string): Promise<NutritionInstant> {
    const config: AxiosRequestConfig = {
      headers: {
        [this.AppIdHeader]: this.AppIdValue,
        [this.AppKeyKey]: this.AppKeyValue,
        [this.AppUserHeader]: this.AppUserValue,
      },
      params: { query },
    };
    const response = await axios
      .get<NutritionInstant>(this.InstantUrl, config)
      .catch((e) => {
        console.log(e);
      });
    return response ? response.data : null;
  }

  /**
   * Gets exercise facts for NLP query
   * @param query - exercise query string (ran 30 min)
   * @param gender - gender
   * @param weightKg - weight kg
   * @param heightCm - height cm
   * @param age - age
   */
  public async getExerciseDataByNlp(
    query: string,
    gender: GenderType,
    weightKg: number,
    heightCm: number,
    age: number
  ): Promise<ExerciseInfo> {
    const config: AxiosRequestConfig = {
      headers: {
        [this.AppIdHeader]: this.AppIdValue,
        [this.AppKeyKey]: this.AppKeyValue,
        [this.AppUserHeader]: this.AppUserValue,
      },
    };
    const data = {
      query,
      age,
      weight_kg: weightKg,
      height_cm: heightCm,
      gender: gender.toLowerCase(),
    };
    const response = await axios
      .post<ExerciseInfo>(this.ExerciseUrl, data, config)
      .catch((e) => {
        console.log(e);
      });
    return response ? response.data : null;
  }
}
