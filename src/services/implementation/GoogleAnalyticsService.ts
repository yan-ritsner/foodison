import ReactGA from 'react-ga';
import { IGoogleAnalyticsService } from '../interfaces';
ReactGA.initialize('UA-180906861-1');

/** Google analytics service implementation */
export class GoogleAnalyticsService implements IGoogleAnalyticsService {
  /** Sends page view to ga */
  public pageView(path: string) {
    ReactGA.pageview(path);
  }

  /** Sends modal view to ga */
  public modalView(name: string) {
    ReactGA.modalview(name);
  }

  /** Sends event to ga */
  public event(
    category: string,
    action: string,
    label?: string,
    value?: number
  ) {
    ReactGA.event({ category, action, label, value });
  }
}
