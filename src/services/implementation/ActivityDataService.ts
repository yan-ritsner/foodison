import { Health } from '@ionic-native/health';
import { Plugins } from '@capacitor/core';
import { isSameDay, startOfDay, subDays } from 'date-fns';
import { xo, ObservableArray } from '../../observable';
import { IActivityDataService } from '../interfaces';
import {
  ActivityData,
  ActivityDailyData,
  ActivityStatsData,
  SettingKeys,
} from '../../data';

/** Activity data service implementation */
export class ActivityDataService implements IActivityDataService {
  /** Key in the storage */
  private static get storageKey(): string {
    return SettingKeys.activityData;
  }
  /** Max id in the storage */
  private static storageId: number = 0;

  /** Data ids array */
  public dataIds: number[];
  /** Data observable array */
  public data: ObservableArray<ActivityData>;
  /** Data map by id */
  public map: Map<number, ActivityData>;

  /** [Lazy Load] chunk size*/
  private chunkSize: number = 20;

  /**
   * @constructor
   */
  public constructor() {
    this.dataIds = [];
    this.data = new ObservableArray();
    this.map = new Map();
  }

  /** Returns activity data splitted into days */
  @xo.computed
  public get dailyData(): ActivityDailyData[] {
    const dailyData: ActivityDailyData[] = [];
    let currentDay: ActivityDailyData = null;
    this.data
      .sort((d1, d2) => d2.timestamp.valueOf() - d1.timestamp.valueOf())
      .forEach((data, index) => {
        const prevData = this.data.get(index - 1);
        if (
          prevData == null ||
          !isSameDay(prevData.timestamp, data.timestamp)
        ) {
          currentDay = {
            timestamp: startOfDay(data.timestamp),
            calories: 0,
            data: [],
          };
          dailyData.push(currentDay);
        }
        currentDay.data.push(data);
        currentDay.calories += data.calories;
      });
    return dailyData;
  }

  /** Returns activity data splitted into days as map */
  @xo.computed
  public get dailyDataMap(): Map<number, ActivityDailyData> {
    const dailyDataMap: Map<number, ActivityDailyData> = new Map();
    this.dailyData.forEach((data) => {
      dailyDataMap.set(data.timestamp.valueOf(), data);
    });
    return dailyDataMap;
  }

  /** Activity statistics data  */
  @xo.computed
  public get statsData(): ActivityStatsData[] {
    const stats: ActivityStatsData[] = [];
    const statsMap: Map<string, ActivityStatsData> = new Map();
    this.data.forEach((data) => {
      let statsData = statsMap.get(data.name);
      if (!statsData) {
        statsData = new ActivityStatsData();
        statsData.count = 1;
        statsData.activity = data.name;
        statsData.duration = data.duration;
        statsData.calories = data.calories;
        statsMap.set(data.name, statsData);
        stats.push(statsData);
      } else {
        statsData.count += 1;
        statsData.duration += data.duration;
        statsData.calories += data.calories;
      }
    });
    return stats.sort((s1, s2) => s2.count - s1.count);
  }

  /** Gets all data from storage */
  public async get(): Promise<void> {
    this.dataIds = await this.getDataIds();
    const data = await this.getData();

    this.data.assign(data);
    data.forEach((d) => this.map.set(d.id, d));
  }

  /** Gets initial data from storage */
  public async getInitial(): Promise<void> {
    this.dataIds = await this.getDataIds();
    const data = await this.getData(0, this.chunkSize);

    this.data.assign(data);
    data.forEach((d) => this.map.set(d.id, d));
  }

  /** [Lazy Load] More data */
  public hasMore(): boolean {
    return this.data.length < this.dataIds.length;
  }

  /** Smallest date timestamp */
  public startDate(): Date {
    return this.data.length > 0
      ? this.data.get(this.data.length - 1).timestamp
      : new Date();
  }

  /** Gets more data from storage */
  public async getMore(): Promise<void> {
    if (!this.hasMore()) return;
    const data = await this.getData(this.data.length, this.chunkSize);

    this.data.push(...data);
    data.forEach((d) => this.map.set(d.id, d));
  }

  /** Gets data by id */
  public getById(id: number): ActivityData | null {
    const data = this.map.get(id);
    return data ? data : null;
  }

  /** Add new data to the storage */
  public async add(data: ActivityData): Promise<void> {
    data.id = ActivityDataService.storageId;
    ActivityDataService.storageId += 1;

    this.dataIds.unshift(data.id);
    this.data.unshift(data);
    this.map.set(data.id, data);

    await this.saveData(data);
    await this.saveDataIds();
  }

  /** Update existing data in the storage */
  public async update(data: ActivityData): Promise<void> {
    const index = this.data.findIndex((d) => d.id === data.id);
    if (index < 0) return;

    this.data.set(index, data);
    this.map.set(data.id, data);

    await this.saveData(data);
  }

  /** Deletes existing data from the storage */
  public async delete(data: ActivityData): Promise<void> {
    const index = this.data.findIndex((d) => d.id === data.id);
    if (index < 0) return;

    this.dataIds.splice(index, 1);
    this.data.splice(index, 1);
    this.map.delete(data.id);

    await this.deleteData(data);
    await this.saveDataIds();
  }

  /** Synchronize with health repository */
  public async sync() {
    const { Storage } = Plugins;
    try {
      // Check that health repo is available and authorize
      const isAvailable = await Health.isAvailable();
      if (!isAvailable) return;
      await Health.requestAuthorization(['activity']);
      const isAuthorized = await Health.isAuthorized(['activity']);
      if (!isAuthorized) return;

      // Get sync data time interval
      let endDate = new Date();
      let startDateValue = await Storage.get({
        key: `${ActivityDataService.storageKey}_sync_date`,
      });
      let startDate = startDateValue.value
        ? new Date(Number(startDateValue.value))
        : subDays(endDate, 7);

      // Get more existing data if needed
      while (this.startDate() > startDate && this.hasMore()) {
        await this.getMore();
      }

      // Get activity data
      const data = await Health.queryAggregated({
        startDate: startDate,
        endDate: endDate,
        dataType: 'activity',
        bucket: 'day',
      });

      // Add activity data
      data.forEach((d) => {
        const value = (d.value as unknown) as object;
        const activities = Object.keys(value).filter((a) => {
          return (
            a !== 'still' &&
            a !== 'unknown' &&
            a !== 'in_vehicle' &&
            a !== 'elevator' &&
            a !== 'escalator' &&
            a.indexOf('sleep') < 0
          );
        });
        activities.forEach((a) => {
          const activity = value[a];
          const activityName = a
            .replace('_', ' ')
            .replace(/\w\S*/g, (w) => w.replace(/^\w/, (c) => c.toUpperCase()));
          const activityData = new ActivityData({
            name: activityName,
            timestamp: d.startDate,
            duration: Math.round(activity.duration / 60000),
            calories: Math.round(activity.calories),
            synced: true,
          });
          const activityDay = startOfDay(activityData.timestamp).valueOf();
          const dailyData = this.dailyDataMap.get(activityDay);
          if (!dailyData) {
            this.add(activityData);
          } else {
            const data = dailyData.data.find((a) => a.synced);
            if (data) {
              activityData.id = data.id;
              this.update(activityData);
            }
          }
        });
      });

      // Store latest sync date
      await Storage.set({
        key: `${ActivityDataService.storageKey}_sync_date`,
        value: endDate.valueOf().toString(),
      });
    } catch (e) {
      console.log(e);
    }
  }

  /** Asynchronously gets activity data ids from the storage */
  protected async getDataIds() {
    const { Storage } = Plugins;
    // get data ids array
    const dataIdsValue = await Storage.get({
      key: ActivityDataService.storageKey,
    });
    const dataIdsString = dataIdsValue ? dataIdsValue.value : null;
    const dataIds = (dataIdsString
      ? JSON.parse(dataIdsString)
      : []) as number[];
    if (dataIds && dataIds.length > 0) {
      ActivityDataService.storageId = Math.max(...dataIds) + 1;
    }
    return dataIds;
  }

  /** Asynchronously gets activity data from the storage */
  protected async getData(start?: number, count?: number) {
    const { Storage } = Plugins;
    const startIndex = start != null ? start : 0;
    const endIndex = startIndex + (count != null ? count : this.dataIds.length);
    const dataIds = this.dataIds.slice(startIndex, endIndex);
    // get all activity data by id
    const data = await Promise.all(
      dataIds.map(async (id) => {
        const dataValue = await Storage.get({
          key: `${ActivityDataService.storageKey}#${id}`,
        });
        const dataString = dataValue ? dataValue.value : null;
        const dataJson = dataString ? JSON.parse(dataString) : null;
        const dataObj = new ActivityData(dataJson);
        return dataObj;
      })
    );
    return data;
  }

  /** Asynchronously saves activity data to the storage */
  protected async saveData(data: ActivityData) {
    const { Storage } = Plugins;
    const dataValue = data.data();
    const dataString = JSON.stringify(dataValue);
    await Storage.set({
      key: `${ActivityDataService.storageKey}#${data.id}`,
      value: dataString,
    });
  }

  /** Asynchronously deleted activity info from the storage */
  protected async deleteData(data: ActivityData) {
    const { Storage } = Plugins;
    await Storage.remove({
      key: `${ActivityDataService.storageKey}#${data.id}`,
    });
  }

  /** Asynchronously saves all data ids to the storage */
  protected async saveDataIds() {
    const { Storage } = Plugins;
    const dataIds = this.data.map((data) => data.id);
    const dataIdsString = JSON.stringify(dataIds);
    await Storage.set({
      key: ActivityDataService.storageKey,
      value: dataIdsString,
    });
  }
}
