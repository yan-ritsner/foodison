/** Google analytics service interface */
export interface IGoogleAnalyticsService {
  /** Sends page view to ga */
  pageView(path: string): void;
  /** Sends modal view to ga */
  modalView(name: string): void;
  /** Sends event to ga */
  event(category: string, action: string, label?: string, value?: number): void;
}
