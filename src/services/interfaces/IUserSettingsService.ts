/** User settings service interface */
export interface IUserSettingsService {
  /**
   * Sets user setting
   * @param key - settings key
   * @param value - setting value
   */
  set(key: string, value: any): Promise<void>;

  /**
   * Gets user setting
   * @param key - setting key
   */
  get<T = any>(key: string): Promise<T>;

  /**
   * Removes user setting
   * @param key - setting key
   */
  remove(key: string): Promise<void>;
}
