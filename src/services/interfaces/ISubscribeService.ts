import { OfferData, SubscribeData } from '../../data';

/** Subscription service interface */
export interface ISubscribeService {
  /**
   * Inits subscription service for current user
   * @param userId - user identifier
   */
  init(userId: string): void;

  /**
   * Returns offerings data
   */
  getOfferings(): Promise<OfferData[] | SubscribeData>;

  /**
   * Purchases package
   * @param offer - offer data
   */
  purchasePackage(offer: OfferData): Promise<SubscribeData | null>;

  /**
   * Gets existing subscription
   */
  getSubscription(): Promise<SubscribeData | null>;

  /**
   * Subscribes for active subscription updates
   * @param callback - callback to be called on subscription update
   */
  onSubscriptionUpdate(callback: (data: SubscribeData | null) => void);
}
