import { ObservableArray } from '../../observable';
import { FoodData, FoodDailyData } from '../../data';

/** Food data service interface */
export interface IFoodDataService {
  /** Data observable array */
  data: ObservableArray<FoodData>;
  /** Data map by id */
  map: Map<number, FoodData>;
  /** Data splitted by day */
  dailyData: FoodDailyData[];
  /** Date splitted by day mapped by date */
  dailyDataMap: Map<number, FoodDailyData>;

  /** Gets all food data from storage */
  get(): Promise<void>;
  /** [Lazy Load] Get initial data from storage */
  getInitial(): Promise<void>;
  /** [Lazy Load] Has more data to load */
  hasMore(): boolean;
  /** [Lazy Load] Smallest date timestamp */
  startDate(): Date;
  /** [Lazy Load] Gets more activity data from storage  */
  getMore(): Promise<void>;
  /** Gets food data by id */
  getById(id: number): FoodData | null;
  /** Add new food data to the storage */
  add(data: FoodData): Promise<void>;
  /** Update existing food data in the storage */
  update(data: FoodData): Promise<void>;
  /** Deletes existing food data from the storage */
  delete(data: FoodData): Promise<void>;
}
