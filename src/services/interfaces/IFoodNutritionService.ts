import { NutritionInfo, ExerciseInfo, NutritionInstant } from '../../data';
import { GenderType } from '../../enums';

/** Food nutrition service interface */
export interface IFoodNutritionService {
  /**
   * Gets food nutrition facts for NLP query
   * @param query - food query string (for breakfast i ate 2 eggs, bacon, and french toast)
   * @param lineDelimited - query contains line (\r\n) delimited list
   * @param useRawFoods - include raw foods
   */
  getNutritionDataByNlp(
    query: string,
    lineDelimited?: boolean,
    useRawFoods?: boolean
  ): Promise<NutritionInfo>;

  /**
   * Gets food nutrition facts for UPC code or By NIX item id
   * @param upc - upc code
   * @param nix_item_id - nix item id
   */
  getNutritionDataByUpc(
    upc?: string,
    nix_item_id?: string
  ): Promise<NutritionInfo>;

  /**
   * Gets food nutrition by instant search
   * @param query - instant search query
   */
  getNutritionInstant(query: string): Promise<NutritionInstant>;

  /**
   * Gets exercise facts for NLP query
   * @param query - exercise query string (ran 30 min)
   * @param gender - gender
   * @param weightKg - weight kg
   * @param heightCm - height cm
   * @param age - age
   */
  getExerciseDataByNlp(
    query: string,
    gender: GenderType,
    weightKg: number,
    heightCm: number,
    age: number
  ): Promise<ExerciseInfo>;
}
