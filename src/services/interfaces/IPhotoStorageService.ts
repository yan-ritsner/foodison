import { Photo } from '../../data';
import { ObservableArray } from '../../observable';

/** Photo storage service interface */
export interface IPhotoStorageService {
  /** Data ids observable array */
  data: ObservableArray<number>;
  /** Data ids set */
  set: Set<number>;

  /** Gets all photo ids */
  get(): Promise<number[]>;
  /** Gets photo by id */
  getById(id: number): Promise<Photo | null>;
  /** Adds new photo to the storage */
  add(): Promise<Photo | null>;
  /** Deletes existing photo from the storage */
  delete(photo: Photo): Promise<void>;
}
