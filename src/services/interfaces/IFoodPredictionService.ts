import { FoodConcept } from '../../data';

/** Food prediction service interface */
export interface IFoodPredictionService {
  /** Predicts foods and its ingredients by base 64 encoded image */
  predict(imageBase64: string): Promise<FoodConcept[]>;
}
