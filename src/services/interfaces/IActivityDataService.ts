import { ObservableArray } from '../../observable';
import { ActivityData, ActivityDailyData, ActivityStatsData } from '../../data';

/** Activity data service interface */
export interface IActivityDataService {
  /** Data observable array */
  data: ObservableArray<ActivityData>;
  /** Data map by id */
  map: Map<number, ActivityData>;
  /** Data splitted by day */
  dailyData: ActivityDailyData[];
  /** Date splitted by day mapped by date */
  dailyDataMap: Map<number, ActivityDailyData>;
  /** Statistics data  */
  statsData: ActivityStatsData[];

  /** Gets all activity data from storage */
  get(): Promise<void>;
  /** [Lazy Load] Get initial data from storage */
  getInitial(): Promise<void>;
  /** [Lazy Load] Has more data to load */
  hasMore(): boolean;
  /** [Lazy Load] Smallest date timestamp */
  startDate(): Date;
  /** [Lazy Load] Gets more activity data from storage  */
  getMore(): Promise<void>;
  /** Gets activity data by id */
  getById(id: number): ActivityData | null;
  /** Add new activity data to the storage */
  add(data: ActivityData): Promise<void>;
  /** Update existing activity data in the storage */
  update(data: ActivityData): Promise<void>;
  /** Deletes existing activity data from the storage */
  delete(data: ActivityData): Promise<void>;
  /** Synchronize with health repository */
  sync(): Promise<void>;
}
