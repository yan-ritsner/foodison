import * as tf from '@tensorflow/tfjs';
import { Prediction } from '../../data';

/** Photo classifier service interface */
export interface IPhotoClassifierService {
  /**
   * Loads tf model asynchronously
   * @param url - model url
   */
  loadModel(url: string): Promise<void>;

  /**
   * Get pre loaded tf model or loads and returns new model
   * @param url - model url
   */
  getModel(url: string): Promise<tf.LayersModel>;

  /**
   * Classifies image using specified tf model and classes map
   * @param model - tf model
   * @param classes - classes map
   * @param image - html image element
   */
  classifyImage(
    model: tf.LayersModel,
    classes: any,
    image: HTMLImageElement
  ): Promise<Prediction[]>;
}
