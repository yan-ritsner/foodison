/** Data input service interface */
export interface IDataInputService {
  /** Scans and return barcode */
  scanBarCode(): Promise<string>;
  /** Recognizes speech and calls callbacks passing matched results or error */
  recognizeSpeech(
    onMatch: (matches: string[]) => void,
    onError: (error: any) => void
  ): Promise<void>;
}
