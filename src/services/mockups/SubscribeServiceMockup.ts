import { OfferData, SubscribeData } from '../../data';
import { OfferPeriod } from '../../enums';
import { ISubscribeService } from '../interfaces';

let isSubscribed = true;

/**
 * Subscription Service Mockup
 */
export class SubscribeServiceMockup implements ISubscribeService {
  public init(userId: string): void {
    //nop
  }

  public async getOfferings(): Promise<OfferData[] | SubscribeData> {
    return new Promise((resolve) =>
      setTimeout(() => {
        resolve([
          new OfferData(1, OfferPeriod.Week, 1.49, '$ 1.49'),
          new OfferData(1, OfferPeriod.Month, 4.99, '$ 4.99'),
          new OfferData(1, OfferPeriod.Year, 29.99, '$ 29.99'),
        ]);
      }, 200)
    );
  }

  public async purchasePackage(
    offer: OfferData
  ): Promise<SubscribeData | null> {
    return new Promise((resolve) =>
      setTimeout(() => {
        isSubscribed = true;
        resolve(new SubscribeData(true, true, '31/12/2021'));
      }, 200)
    );
  }

  public async getSubscription(): Promise<SubscribeData | null> {
    return new Promise((resolve) =>
      setTimeout(() => {
        if (!isSubscribed) resolve(null);
        else resolve(new SubscribeData(true, true, '31/12/2021'));
      }, 200)
    );
  }

  public onSubscriptionUpdate(callback: (data: SubscribeData) => void) {
    setInterval(() => {
      if (!isSubscribed) callback(null);
      else callback(new SubscribeData(true, true, '31/12/2021'));
    }, 5000);
  }
}
