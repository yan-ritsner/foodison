import { DataSource } from './../../enums/';
import { FoodData } from '../../data/';
import { FoodDataService } from './../implementation/';
import { subDays, startOfDay, addHours } from 'date-fns';

/** Food data service mockup */
export class FoodDataServiceMockup extends FoodDataService {
  protected static mockupData: FoodData[];

  protected async getDataIds() {
    if (FoodDataServiceMockup.mockupData == null) {
      FoodDataServiceMockup.mockupData = this.getMockupData();
    }
    return FoodDataServiceMockup.mockupData.map((data) => data.id);
  }

  protected async getData(start?: number, count?: number) {
    const startIndex = start != null ? start : 0;
    const endIndex = startIndex + (count != null ? count : this.dataIds.length);
    if (FoodDataServiceMockup.mockupData == null) {
      FoodDataServiceMockup.mockupData = this.getMockupData();
    }
    return FoodDataServiceMockup.mockupData.slice(startIndex, endIndex);
  }

  private getMockupData() {
    let today = startOfDay(new Date());
    let lastDay = subDays(today, 60);
    let id = 0;

    const data: FoodData[] = [];

    while (today >= lastDay) {
      const times = 3 + Math.round(Math.random() * 2);
      let hours = 10;

      for (let index = 0; index < times; index++) {
        hours += Math.round(Math.random() * 3);
        const timestamp = addHours(new Date(today), hours);
        const factor = (5 + Math.round(Math.random() * 5)) / 10;
        const calories = Math.round(700 * factor);

        const item = new FoodData({
          id: id,
          source: DataSource.Photo,
          photoId: null,
          timestamp: timestamp,
          predictions: [],
          nutrition: {
            foods: [
              {
                food_name: 'Hamburger',
                serving_qty: 1,
                serving_unit: 'Sandwich',
                serving_weight_grams: 400,
                nf_calories: calories,
                photo: {
                  thumb: null,
                  highres: null,
                },
              },
            ],
          },
          foodSelected: [true],
          foodPortion: [1],
        });
        item.calories = item.getTotalCalories();

        data.push(item);
        id += 1;
      }

      today = subDays(today, 1);
    }
    return data;
  }
}
