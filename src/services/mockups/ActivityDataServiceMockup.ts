import { ActivityData } from './../../data/ActivityData';
import { ActivityDataService } from './../implementation/ActivityDataService';
import { subDays, startOfDay, addHours } from 'date-fns';

export class ActivityDataServiceMockup extends ActivityDataService {
  public static mockupData: ActivityData[];

  protected async getDataIds() {
    if (ActivityDataServiceMockup.mockupData == null) {
      ActivityDataServiceMockup.mockupData = this.getMockupData();
    }
    return ActivityDataServiceMockup.mockupData.map((data) => data.id);
  }

  protected async getData(start?: number, count?: number) {
    const startIndex = start != null ? start : 0;
    const endIndex = startIndex + (count != null ? count : this.dataIds.length);
    if (ActivityDataServiceMockup.mockupData == null) {
      ActivityDataServiceMockup.mockupData = this.getMockupData();
    }
    return ActivityDataServiceMockup.mockupData.slice(startIndex, endIndex);
  }

  private getMockupData() {
    let today = startOfDay(new Date());
    let lastDay = subDays(today, 60);
    let id = 0;

    const data: ActivityData[] = [];

    while (today >= lastDay) {
      const times = 1 + Math.round(Math.random() * 1);
      let hours = 10;

      for (let index = 0; index < times; index++) {
        hours += Math.round(Math.random() * 3);
        const timestamp = addHours(new Date(today), hours);
        const duration = 6 * Math.round(Math.random() * 10);
        const calories = (400 * duration) / 60;

        const item = new ActivityData({
          id: id,
          timestamp: timestamp,
          name: 'Running, general',
          duration: duration,
          calories: calories,
        });

        data.push(item);

        id += 1;
      }

      today = subDays(today, 1);
    }
    return data;
  }
}
