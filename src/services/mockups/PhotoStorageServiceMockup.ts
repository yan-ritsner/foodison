import { Plugins, FilesystemDirectory } from '@capacitor/core';
import { PhotoStorageService } from './../implementation/';
import { Photo } from '../../data';

/** Photo storage service mockup */
export class PhotoStorageServiceMockup extends PhotoStorageService {
  protected async getPhoto(): Promise<Photo | null> {
    const { Filesystem } = Plugins;
    const path = '/assets/hamburger.jpg';
    const timestamp = new Date();
    const fileName = timestamp.getTime() + '.jpeg';

    const base64Data = await this.base64FromPath(path, true);

    const savedFile = await Filesystem.writeFile({
      path: fileName,
      data: base64Data,
      directory: FilesystemDirectory.Data,
    });

    const photo = new Photo();
    photo.id = 0;
    photo.timestamp = timestamp;
    photo.base64 = base64Data;
    photo.filePath = savedFile.uri;
    photo.webviewPath = path;

    return photo;
  }
}
