import { xo } from "../observable";

/** Activity date */
export class ActivityData {
  /** Activity unique id */
  id: number;
  /** Activity display name */
  @xo.observable
  name: string;
  /** Activity timestamp */
  @xo.observable
  timestamp: Date;
  /** Activity duration in minutes */
  @xo.observable
  duration: number;
  /** Calories burned during activity */
  @xo.observable
  calories: number;
  /** Activity synchronized from health repo */
  @xo.observable
  synced: boolean;

  /**
   * @constructor
   * @param data - optional raw data
   */
  constructor(data?: any) {
    if (!data) return;
    this.id = data.id;
    this.name = data.name;
    this.timestamp = new Date(data.timestamp);
    this.duration = data.duration;
    this.calories = data.calories;
    this.synced = data.synced;
  }

  /** Returns photo raw data */
  data() {
    return {
      id: this.id,
      name: this.name,
      timestamp: this.timestamp,
      duration: this.duration,
      calories: this.calories,
      synced: this.synced,
    };
  }
}
