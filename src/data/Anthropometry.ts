import { GenderType, UnitsType, ActivityType, UnitsFactor } from '../enums';
import { differenceInYears } from 'date-fns';

/** Anthropometry settings */
export class Anthropometry {
  /** Gender */
  gender: GenderType;
  /** Units */
  units: UnitsType;
  /** Activity */
  activity: ActivityType;
  /** Date of birth */
  dob: Date;
  /** Height */
  height: number;
  /** Weight */
  weight: number;
  /** BMR value */
  bmr: number;

  /**
   * @constructor
   * @param data - optional raw data
   */
  constructor(data?: any) {
    if (data) {
      Object.assign(this, data);
      this.dob = new Date(data.dob);
      this.calculateBmr();
    }
  }

  /** Calculates bmr value */
  public calculateBmr() {
    const heightFactor =
      this.units === UnitsType.English ? 1 : UnitsFactor.HeightCmToIn;
    const weightFactor =
      this.units === UnitsType.English ? 1 : UnitsFactor.WeightKgToLb;

    let activityFactor = 1;
    switch (this.activity) {
      case ActivityType.None:
        activityFactor = 1;
        break;
      case ActivityType.Sedentary:
        activityFactor = 1.2;
        break;
      case ActivityType.LightActivity:
        activityFactor = 1.375;
        break;
      case ActivityType.ModerateActivity:
        activityFactor = 1.55;
        break;
      case ActivityType.VeryActive:
        activityFactor = 1.725;
        break;
      case ActivityType.ExtraActive:
        activityFactor = 1.9;
        break;
    }

    const height = this.height * heightFactor;
    const weight = this.weight * weightFactor;
    const age = this.dob == null ? 0 : differenceInYears(new Date(), this.dob);
    let bmr = 0;

    switch (this.gender) {
      case GenderType.Male:
        bmr = 66 + 6.2 * weight + 12.7 * height - 6.76 * age;
        break;
      case GenderType.Female:
        bmr = 655.1 + 4.35 * weight + 4.7 * height + 4.7 * age;
        break;
    }

    this.bmr = Math.round(bmr * activityFactor);
  }

  /** Get weight in specified units */
  public getWeight(units: UnitsType): number {
    let weight = this.weight;
    if (units !== this.units) {
      switch (this.units) {
        case UnitsType.Metric:
          weight = weight * UnitsFactor.WeightKgToLb;
          break;
        case UnitsType.English:
          weight = weight / UnitsFactor.WeightKgToLb;
      }
    }
    return weight;
  }

  /** Get height in specified units */
  public getHeight(units: UnitsType): number {
    let height = this.height;
    if (units !== this.units) {
      switch (this.units) {
        case UnitsType.Metric:
          height = height * UnitsFactor.HeightCmToIn;
          break;
        case UnitsType.English:
          height = height / UnitsFactor.HeightCmToIn;
      }
    }
    return height;
  }

  /** Get age */
  public getAge() {
    return this.dob == null ? 0 : differenceInYears(new Date(), this.dob);
  }
}
