/** App subscription data */
export class SubscribeData {
  /**  Subscription */
  isActive: boolean;
  /** Subscription will auto renew  */
  willRenew: boolean;
  /** Expiration date */
  expirationDate: string;
  /** Error ocured */
  error?: any;

  /**
   * @constructor
   */
  constructor(
    isActive: boolean,
    willRenew: boolean,
    expirationDate: string,
    error?: any
  ) {
    this.isActive = isActive;
    this.willRenew = willRenew;
    this.expirationDate = expirationDate;
    this.error = error;
  }
}
