/** Search result */
export interface SearchResult {
  id: string;
  group: string;
  name: string;
  image: string;
}
