/** Food prediction concept */
export interface FoodConcept {
  index: number;
  id: string;
  name: string;
  app_id: string;
  value: number;
}
