import { OfferPeriod } from '../enums';

/** Offer data */
export class OfferData {
  /** Offer duration */
  duration: number;
  /** Offer period type */
  period: OfferPeriod;
  /** Offer price */
  price: number;
  /** Offer price string */
  priceString: string;
  /** Offer price details */
  priceDetails: string;
  /** Offer badge */
  badge: string;
  /** Offer metadata */
  data: any;

  /** Offer name */
  get displayName() {
    return `${this.duration} ${this.periodName}`;
  }

  /** Offer period name */
  get periodName() {
    return `${this.period}${this.duration > 1 ? 'S' : ''}`;
  }

  /**
   * @constructor
   */
  constructor(
    duration: number,
    period: OfferPeriod,
    price: number,
    priceString: string,
    data: any = null,
    priceDetails: string = '',
    badge: any = ''
  ) {
    this.duration = duration;
    this.period = period;
    this.price = price;
    this.priceString = priceString;
    this.data = data;
    this.priceDetails = priceDetails;
    this.badge = badge;
  }
}
