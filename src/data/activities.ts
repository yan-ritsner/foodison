import { UnitsType } from '../enums';
import { ActivitiesInfo } from './ActivityInfo';

/**
 * Activities data from: https://www.nutristrategy.com/caloriesburned.htm
 * Alternative: https://www.health.harvard.edu/diet-and-weight-loss/calories-burned-in-30-minutes-of-leisure-and-routine-activities
 **/
export const activities: ActivitiesInfo = {
  duration: 60,
  weightUnits: UnitsType.English,
  weightCategories: [130, 155, 180, 205],
  categories: [
    {
      category: 'Cycling',
      activities: [
        {
          name: 'Cycling, mountain bike, bmx',
          calories: [502, 598, 695, 791],
        },
        {
          name: 'Cycling, <10 mph, leisure bicycling',
          calories: [236, 281, 327, 372],
        },
        {
          name: 'Cycling, >20 mph, racing',
          calories: [944, 1126, 1308, 1489],
        },
        {
          name: 'Cycling, 10-11.9 mph, light',
          calories: [354, 422, 490, 558],
        },
        {
          name: 'Cycling, 12-13.9 mph, moderate',
          calories: [472, 563, 654, 745],
        },
        {
          name: 'Cycling, 14-15.9 mph, vigorous',
          calories: [590, 704, 817, 931],
        },
        {
          name: 'Cycling, 16-19 mph, very fast, racing',
          calories: [708, 844, 981, 1117],
        },
        {
          name: 'Unicycling',
          calories: [295, 352, 409, 465],
        },
        {
          name: 'Stationary cycling, very light',
          calories: [177, 211, 245, 279],
        },
        {
          name: 'Stationary cycling, light',
          calories: [325, 387, 449, 512],
        },
        {
          name: 'Stationary cycling, moderate',
          calories: [413, 493, 572, 651],
        },
        {
          name: 'Stationary cycling, vigorous',
          calories: [620, 739, 858, 977],
        },
        {
          name: 'Stationary cycling, very vigorous',
          calories: [738, 880, 1022, 1163],
        },
      ],
    },
    {
      category: 'Running',
      activities: [
        {
          name: 'Running, 5 mph (12 minute mile)',
          calories: [472, 563, 654, 745],
        },
        {
          name: 'Running, 5.2 mph (11.5 minute mile)',
          calories: [531, 633, 735, 838],
        },
        {
          name: 'Running, 6 mph (10 min mile)',
          calories: [590, 704, 817, 931],
        },
        {
          name: 'Running, 6.7 mph (9 min mile)',
          calories: [649, 774, 899, 1024],
        },
        {
          name: 'Running, 7 mph (8.5 min mile)',
          calories: [679, 809, 940, 1070],
        },
        {
          name: 'Running, 7.5mph (8 min mile)',
          calories: [738, 880, 1022, 1163],
        },
        {
          name: 'Running, 8 mph (7.5 min mile)',
          calories: [797, 950, 1103, 1256],
        },
        {
          name: 'Running, 8.6 mph (7 min mile)',
          calories: [826, 985, 1144, 1303],
        },
        {
          name: 'Running, 9 mph (6.5 min mile)',
          calories: [885, 1056, 1226, 1396],
        },
        {
          name: 'Running, 10 mph (6 min mile)',
          calories: [944, 1126, 1308, 1489],
        },
        {
          name: 'Running, 10.9 mph (5.5 min mile)',
          calories: [1062, 1267, 1471, 1675],
        },
        {
          name: 'Running, cross country',
          calories: [531, 633, 735, 838],
        },
        {
          name: 'Running, general',
          calories: [472, 563, 654, 745],
        },
        {
          name: 'Running, on a track, team practice',
          calories: [590, 704, 817, 931],
        },
        {
          name: 'Running, stairs, up',
          calories: [885, 1056, 1226, 1396],
        },
        {
          name: 'Track and field (shot, discus)',
          calories: [236, 281, 327, 372],
        },
        {
          name: 'Track and field (high jump, pole vault)',
          calories: [354, 422, 490, 558],
        },
        {
          name: 'Track and field (hurdles)',
          calories: [590, 704, 817, 931],
        },
      ],
    },
    {
      category: 'Walking',
      activities: [
        {
          name: 'Walking, under 2.0 mph, very slow',
          calories: [118, 141, 163, 186],
        },
        {
          name: 'Walking 2.0 mph, slow',
          calories: [148, 176, 204, 233],
        },
        {
          name: 'Walking 2.5 mph',
          calories: [177, 211, 245, 279],
        },
        {
          name: 'Walking 3.0 mph, moderate',
          calories: [195, 232, 270, 307],
        },
        {
          name: 'Walking 3.5 mph, brisk pace',
          calories: [224, 267, 311, 354],
        },
        {
          name: 'Walking 3.5 mph, uphill',
          calories: [354, 422, 490, 558],
        },
        {
          name: 'Walking 4.0 mph, very brisk',
          calories: [295, 352, 409, 465],
        },
        {
          name: 'Walking 4.5 mph',
          calories: [372, 443, 515, 586],
        },
        {
          name: 'Walking 5.0 mph',
          calories: [472, 563, 654, 745],
        },
        {
          name: 'Orienteering',
          calories: [531, 633, 735, 838],
        },
        {
          name: 'Rock climbing, ascending rock',
          calories: [649, 774, 899, 1024],
        },
        {
          name: 'Rock climbing, rappelling',
          calories: [472, 563, 654, 745],
        },
        {
          name: 'Backpacking, Hiking with pack',
          calories: [413, 493, 572, 651],
        },
        {
          name: 'Carrying infant, level ground',
          calories: [207, 246, 286, 326],
        },
        {
          name: 'Carrying infant, upstairs',
          calories: [295, 352, 409, 465],
        },
        {
          name: 'Carrying 16 to 24 lbs, upstairs',
          calories: [354, 422, 490, 558],
        },
        {
          name: 'Carrying 25 to 49 lbs, upstairs',
          calories: [472, 563, 654, 745],
        },
        {
          name: 'Standing, playing with children, light',
          calories: [165, 197, 229, 261],
        },
        {
          name: 'Walk/run, playing with children, moderate',
          calories: [236, 281, 327, 372],
        },
        {
          name: 'Walk/run, playing with children, vigorous',
          calories: [295, 352, 409, 465],
        },
        {
          name: 'Carrying small children',
          calories: [177, 211, 245, 279],
        },
        {
          name: 'Loading, unloading car',
          calories: [177, 211, 245, 279],
        },
        {
          name: 'Climbing hills, carrying up to 9 lbs',
          calories: [413, 493, 572, 651],
        },
        {
          name: 'Climbing hills, carrying 10 to 20 lb',
          calories: [443, 528, 613, 698],
        },
        {
          name: 'Climbing hills, carrying 21 to 42 lb',
          calories: [472, 563, 654, 745],
        },
        {
          name: 'Climbing hills, carrying over 42 lb',
          calories: [531, 633, 735, 838],
        },
        {
          name: 'Walking downstairs',
          calories: [177, 211, 245, 279],
        },
        {
          name: 'Hiking, cross country',
          calories: [354, 422, 490, 558],
        },
        {
          name: 'Bird watching',
          calories: [148, 176, 204, 233],
        },
        {
          name: 'Marching, rapidly, military',
          calories: [384, 457, 531, 605],
        },
        {
          name: 'Pushing stroller or walking with children',
          calories: [148, 176, 204, 233],
        },
        {
          name: 'Pushing a wheelchair',
          calories: [236, 281, 327, 372],
        },
        {
          name: 'Race walking',
          calories: [384, 457, 531, 605],
        },
        {
          name: 'Rock climbing, mountain climbing',
          calories: [472, 563, 654, 745],
        },
        {
          name: 'Walking using crutches',
          calories: [295, 352, 409, 465],
        },
        {
          name: 'Walking the dog',
          calories: [177, 211, 245, 279],
        },
        {
          name: 'Walk / run, playing with animals',
          calories: [236, 281, 327, 372],
        },
        {
          name: 'Walking, pushing a wheelchair',
          calories: [236, 281, 327, 372],
        },
      ],
    },
    {
      category: 'Swimming',
      activities: [
        {
          name: 'Swimming laps, freestyle, fast',
          calories: [590, 704, 817, 931],
        },
        {
          name: 'Swimming laps, freestyle, slow',
          calories: [413, 493, 572, 651],
        },
        {
          name: 'Swimming backstroke',
          calories: [413, 493, 572, 651],
        },
        {
          name: 'Swimming breaststroke',
          calories: [590, 704, 817, 931],
        },
        {
          name: 'Swimming butterfly',
          calories: [649, 774, 899, 1024],
        },
        {
          name: 'Swimming leisurely, not laps',
          calories: [354, 422, 490, 558],
        },
        {
          name: 'Swimming sidestroke',
          calories: [472, 563, 654, 745],
        },
        {
          name: 'Swimming synchronized',
          calories: [472, 563, 654, 745],
        },
        {
          name: 'Swimming, treading water, fast, vigorous',
          calories: [590, 704, 817, 931],
        },
        {
          name: 'Swimming, treading water, moderate',
          calories: [236, 281, 327, 372],
        },
        {
          name: 'Water aerobics, water calisthenics',
          calories: [236, 281, 327, 372],
        },
        {
          name: 'Water polo',
          calories: [590, 704, 817, 931],
        },
        {
          name: 'Water volleyball',
          calories: [177, 211, 245, 279],
        },
        {
          name: 'Water jogging',
          calories: [472, 563, 654, 745],
        },
        {
          name: 'Diving, springboard or platform',
          calories: [177, 211, 245, 279],
        },
      ],
    },
    {
      category: 'Workout, Gym',
      activities: [
        {
          name: 'Calisthenics, vigorous, pushups, situps…',
          calories: [472, 563, 654, 745],
        },
        {
          name: 'Calisthenics, light',
          calories: [207, 246, 286, 326],
        },
        {
          name: 'Circuit training, minimal rest',
          calories: [472, 563, 654, 745],
        },
        {
          name: 'Weight lifting, body building, vigorous',
          calories: [354, 422, 490, 558],
        },
        {
          name: 'Weight lifting, light workout',
          calories: [177, 211, 245, 279],
        },
        {
          name: 'Health club exercise',
          calories: [325, 387, 449, 512],
        },
        {
          name: 'Stair machine',
          calories: [531, 633, 735, 838],
        },
        {
          name: 'Rowing machine, light',
          calories: [207, 246, 286, 326],
        },
        {
          name: 'Rowing machine, moderate',
          calories: [413, 493, 572, 651],
        },
        {
          name: 'Rowing machine, vigorous',
          calories: [502, 598, 695, 791],
        },
        {
          name: 'Rowing machine, very vigorous',
          calories: [708, 844, 981, 1117],
        },
        {
          name: 'Ski machine',
          calories: [413, 493, 572, 651],
        },
      ],
    },
    {
      category: 'Exercise Class',
      activities: [
        {
          name: 'Aerobics, low impact',
          calories: [295, 352, 409, 465],
        },
        {
          name: 'Aerobics, high impact',
          calories: [413, 493, 572, 651],
        },
        {
          name: 'Aerobics, step aerobics',
          calories: [502, 598, 695, 791],
        },
        {
          name: 'Aerobics, general',
          calories: [384, 457, 531, 605],
        },
        {
          name: 'Jazzercise',
          calories: [354, 422, 490, 558],
        },
        {
          name: 'Stretching, hatha yoga',
          calories: [236, 281, 327, 372],
        },
        {
          name: 'Mild stretching',
          calories: [148, 176, 204, 233],
        },
        {
          name: 'Instructing aerobic class',
          calories: [354, 422, 490, 558],
        },
        {
          name: 'Water aerobics',
          calories: [236, 281, 327, 372],
        },
        {
          name: 'Ballet, twist, jazz, tap',
          calories: [266, 317, 368, 419],
        },
        {
          name: 'Ballroom dancing, slow',
          calories: [177, 211, 245, 279],
        },
        {
          name: 'Ballroom dancing, fast',
          calories: [325, 387, 449, 512],
        },
        {
          name: 'Teach aerobic classes (& participate)',
          calories: [384, 457, 531, 605],
        },
      ],
    },
    {
      category: 'Water Sports',
      activities: [
        {
          name: 'Boating, power, speed boat',
          calories: [148, 176, 204, 233],
        },
        {
          name: 'Canoeing, camping trip',
          calories: [236, 281, 327, 372],
        },
        {
          name: 'Canoeing, rowing, light',
          calories: [177, 211, 245, 279],
        },
        {
          name: 'Canoeing, rowing, moderate',
          calories: [413, 493, 572, 651],
        },
        {
          name: 'Canoeing, rowing, vigorous',
          calories: [708, 844, 981, 1117],
        },
        {
          name: 'Crew, sculling, rowing, competition',
          calories: [708, 844, 981, 1117],
        },
        {
          name: 'Kayaking',
          calories: [295, 352, 409, 465],
        },
        {
          name: 'Paddle boat',
          calories: [236, 281, 327, 372],
        },
        {
          name: 'Windsurfing, sailing',
          calories: [177, 211, 245, 279],
        },
        {
          name: 'Sailing, competition',
          calories: [295, 352, 409, 465],
        },
        {
          name: 'Sailing, yachting, ocean sailing',
          calories: [177, 211, 245, 279],
        },
        {
          name: 'Skiing, water skiing',
          calories: [354, 422, 490, 558],
        },
        {
          name: 'Skin diving, fast',
          calories: [944, 1126, 1308, 1489],
        },
        {
          name: 'Skin diving, moderate',
          calories: [738, 880, 1022, 1163],
        },
        {
          name: 'Skin diving, scuba diving',
          calories: [413, 493, 572, 651],
        },
        {
          name: 'Snorkeling',
          calories: [295, 352, 409, 465],
        },
        {
          name: 'Surfing, body surfing or board surfing',
          calories: [177, 211, 245, 279],
        },
        {
          name: 'Whitewater rafting, kayaking, canoeing',
          calories: [295, 352, 409, 465],
        },
      ],
    },
    {
      category: 'Winter Sports',
      activities: [
        { name: 'Ice skating, < 9 mph', calories: [325, 387, 449, 512] },
        {
          name: 'Ice skating, average speed',
          calories: [413, 493, 572, 651],
        },
        { name: 'Ice skating, rapidly', calories: [531, 633, 735, 838] },
        {
          name: 'Speed skating, ice, competitive',
          calories: [885, 1056, 1226, 1396],
        },
        {
          name: 'Cross country snow skiing, slow',
          calories: [413, 493, 572, 651],
        },
        {
          name: 'Cross country skiing, moderate',
          calories: [472, 563, 654, 745],
        },
        {
          name: 'Cross country skiing, vigorous',
          calories: [531, 633, 735, 838],
        },
        {
          name: 'Cross country skiing, racing',
          calories: [826, 985, 1144, 1303],
        },
        {
          name: 'Cross country skiing, uphill',
          calories: [974, 1161, 1348, 1536],
        },
        {
          name: 'Snow skiing, downhill skiing, light',
          calories: [295, 352, 409, 465],
        },
        {
          name: 'Downhill snow skiing, moderate',
          calories: [354, 422, 490, 558],
        },
        {
          name: 'Downhill snow skiing, racing',
          calories: [472, 563, 654, 745],
        },
        {
          name: 'Sledding, tobagganing, luge',
          calories: [413, 493, 572, 651],
        },
        {
          name: 'Snow shoeing',
          calories: [472, 563, 654, 745],
        },
        {
          name: 'Snowmobiling',
          calories: [207, 246, 286, 326],
        },
        {
          name: 'Hockey, ice hockey',
          calories: [472, 563, 654, 745],
        },
        {
          name: 'Ski mobiling',
          calories: [413, 493, 572, 651],
        },
      ],
    },
    {
      category: 'Summer Sports',
      activities: [
        {
          name: 'Archery',
          calories: [207, 246, 286, 326],
        },
        {
          name: 'Badminton',
          calories: [266, 317, 368, 419],
        },
        {
          name: 'Basketball game, competitive',
          calories: [472, 563, 654, 745],
        },
        {
          name: 'Playing basketball, non game',
          calories: [354, 422, 490, 558],
        },
        {
          name: 'Basketball, officiating',
          calories: [413, 493, 572, 651],
        },
        {
          name: 'Basketball, shooting baskets',
          calories: [266, 317, 368, 419],
        },
        {
          name: 'Basketball, wheelchair',
          calories: [384, 457, 531, 605],
        },
        {
          name: 'Billiards',
          calories: [148, 176, 204, 233],
        },
        {
          name: 'Bowling',
          calories: [177, 211, 245, 279],
        },
        {
          name: 'Boxing, in ring',
          calories: [708, 844, 981, 1117],
        },
        {
          name: 'Boxing, punching bag',
          calories: [354, 422, 490, 558],
        },
        {
          name: 'Boxing, sparring',
          calories: [531, 633, 735, 838],
        },
        {
          name: 'Cricket (batting, bowling)',
          calories: [295, 352, 409, 465],
        },
        {
          name: 'Croquet',
          calories: [148, 176, 204, 233],
        },
        {
          name: 'Curling',
          calories: [236, 281, 327, 372],
        },
        {
          name: 'Darts (wall or lawn)',
          calories: [148, 176, 204, 233],
        },
        {
          name: 'Fencing',
          calories: [354, 422, 490, 558],
        },
        {
          name: 'Football, competitive',
          calories: [531, 633, 735, 838],
        },
        {
          name: 'Football, touch, flag, general',
          calories: [472, 563, 654, 745],
        },
        {
          name: 'Football or baseball, playing catch',
          calories: [148, 176, 204, 233],
        },
        {
          name: 'Frisbee playing, general',
          calories: [177, 211, 245, 279],
        },
        {
          name: 'Frisbee, ultimate frisbee',
          calories: [472, 563, 654, 745],
        },
        {
          name: 'Golf, general',
          calories: [266, 317, 368, 419],
        },
        {
          name: 'Golf, walking and carrying clubs',
          calories: [266, 317, 368, 419],
        },
        {
          name: 'Golf, driving range',
          calories: [177, 211, 245, 279],
        },
        {
          name: 'Golf, miniature golf',
          calories: [177, 211, 245, 279],
        },
        {
          name: 'Golf, walking and pulling clubs',
          calories: [254, 303, 351, 400],
        },
        {
          name: 'Golf, using power cart',
          calories: [207, 246, 286, 326],
        },
        {
          name: 'Gymnastics',
          calories: [236, 281, 327, 372],
        },
        {
          name: 'Hacky sack',
          calories: [236, 281, 327, 372],
        },
        {
          name: 'Handball',
          calories: [708, 844, 981, 1117],
        },
        {
          name: 'Handball, team',
          calories: [472, 563, 654, 745],
        },
        {
          name: 'Hockey, field hockey',
          calories: [472, 563, 654, 745],
        },
        {
          name: 'Riding a horse, general',
          calories: [236, 281, 327, 372],
        },
        {
          name: 'Horesback riding, saddling horse',
          calories: [207, 246, 286, 326],
        },
        {
          name: 'Horseback riding, grooming horse',
          calories: [207, 246, 286, 326],
        },
        {
          name: 'Horseback riding, trotting',
          calories: [384, 457, 531, 605],
        },
        {
          name: 'Horseback riding, walking',
          calories: [148, 176, 204, 233],
        },
        {
          name: 'Horse racing, galloping',
          calories: [472, 563, 654, 745],
        },
        {
          name: 'Horse grooming, moderate',
          calories: [354, 422, 490, 558],
        },
        {
          name: 'Horseshoe pitching',
          calories: [177, 211, 245, 279],
        },
        {
          name: 'Jai alai',
          calories: [708, 844, 981, 1117],
        },
        {
          name: 'Martial arts, judo, karate, jujitsu',
          calories: [590, 704, 817, 931],
        },
        {
          name: 'Martial arts, kick boxing',
          calories: [590, 704, 817, 931],
        },
        {
          name: 'Martial arts, tae kwan do',
          calories: [590, 704, 817, 931],
        },
        {
          name: 'Krav maga training',
          calories: [590, 704, 817, 931],
        },
        {
          name: 'Juggling',
          calories: [236, 281, 327, 372],
        },
        {
          name: 'Kickball',
          calories: [413, 493, 572, 651],
        },
        {
          name: 'Lacrosse',
          calories: [472, 563, 654, 745],
        },
        {
          name: 'Orienteering',
          calories: [531, 633, 735, 838],
        },
        {
          name: 'Playing paddleball',
          calories: [354, 422, 490, 558],
        },
        {
          name: 'Paddleball, competitive',
          calories: [590, 704, 817, 931],
        },
        {
          name: 'Polo',
          calories: [472, 563, 654, 745],
        },
        {
          name: 'Racquetball, competitive',
          calories: [590, 704, 817, 931],
        },
        {
          name: 'Playing racquetball',
          calories: [413, 493, 572, 651],
        },
        {
          name: 'Rock climbing, ascending rock',
          calories: [649, 774, 899, 1024],
        },
        {
          name: 'Rock climbing, rappelling',
          calories: [472, 563, 654, 745],
        },
        {
          name: 'Jumping rope, fast',
          calories: [708, 844, 981, 1117],
        },
        {
          name: 'Jumping rope, moderate',
          calories: [590, 704, 817, 931],
        },
        {
          name: 'Jumping rope, slow',
          calories: [472, 563, 654, 745],
        },
        {
          name: 'Rugby',
          calories: [590, 704, 817, 931],
        },
        {
          name: 'Shuffleboard, lawn bowling',
          calories: [177, 211, 245, 279],
        },
        {
          name: 'Skateboarding',
          calories: [295, 352, 409, 465],
        },
        {
          name: 'Roller skating',
          calories: [413, 493, 572, 651],
        },
        {
          name: 'Roller blading, in-line skating',
          calories: [708, 844, 981, 1117],
        },
        {
          name: 'Sky diving',
          calories: [177, 211, 245, 279],
        },
        {
          name: 'Soccer, competitive',
          calories: [590, 704, 817, 931],
        },
        {
          name: 'Playing soccer',
          calories: [413, 493, 572, 651],
        },
        {
          name: 'Softball or baseball',
          calories: [295, 352, 409, 465],
        },
        {
          name: 'Softball, officiating',
          calories: [236, 281, 327, 372],
        },
        {
          name: 'Softball, pitching',
          calories: [354, 422, 490, 558],
        },
        {
          name: 'Squash',
          calories: [708, 844, 981, 1117],
        },
        {
          name: 'Table tennis, ping pong',
          calories: [236, 281, 327, 372],
        },
        {
          name: 'Tai chi',
          calories: [236, 281, 327, 372],
        },
        {
          name: 'Playing tennis',
          calories: [413, 493, 572, 651],
        },
        {
          name: 'Tennis, doubles',
          calories: [354, 422, 490, 558],
        },
        {
          name: 'Tennis, singles',
          calories: [472, 563, 654, 745],
        },
        {
          name: 'Trampoline',
          calories: [207, 246, 286, 326],
        },
        {
          name: 'Volleyball, competitive',
          calories: [472, 563, 654, 745],
        },
        {
          name: 'Playing volleyball',
          calories: [177, 211, 245, 279],
        },
        {
          name: 'Volleyball, beach',
          calories: [472, 563, 654, 745],
        },
        {
          name: 'Wrestling',
          calories: [354, 422, 490, 558],
        },
        {
          name: 'Wallyball',
          calories: [413, 493, 572, 651],
        },
      ],
    },
    {
      category: 'Activities',
      activities: [
        {
          name: 'Coaching: football, basketball, soccer…',
          calories: [236, 281, 327, 372],
        },
        {
          name: 'Flying airplane (pilot)',
          calories: [118, 141, 163, 186],
        },
        {
          name: 'Pushing plane in and out of hanger',
          calories: [354, 422, 490, 558],
        },
        {
          name: 'Riding motorcyle',
          calories: [148, 176, 204, 233],
        },
        {
          name: 'General housework, light',
          calories: [148, 176, 204, 233],
        },
        {
          name: 'General housework, moderate',
          calories: [207, 246, 286, 326],
        },
        {
          name: 'General housework, vigorous',
          calories: [236, 281, 327, 372],
        },
        {
          name: 'Cleaning gutters',
          calories: [295, 352, 409, 465],
        },
        {
          name: 'Painting',
          calories: [266, 317, 368, 419],
        },
        {
          name: 'Sit, playing with animals, light',
          calories: [148, 176, 204, 233],
        },
        {
          name: 'Walk / run, playing with animals',
          calories: [236, 281, 327, 372],
        },
        {
          name: 'Bathing dog',
          calories: [207, 246, 286, 326],
        },
        {
          name: 'Mowing lawn, walk, power mower',
          calories: [325, 387, 449, 512],
        },
        {
          name: 'Mowing lawn, riding mower',
          calories: [148, 176, 204, 233],
        },
        {
          name: 'Walking, snow blower',
          calories: [207, 246, 286, 326],
        },
        {
          name: 'Riding, snow blower',
          calories: [177, 211, 245, 279],
        },
        {
          name: 'Shoveling snow by hand',
          calories: [354, 422, 490, 558],
        },
        {
          name: 'Raking lawn',
          calories: [254, 303, 351, 400],
        },
        {
          name: 'Gardening, general',
          calories: [236, 281, 327, 372],
        },
        {
          name: 'Bagging grass, leaves',
          calories: [236, 281, 327, 372],
        },
        {
          name: 'Watering lawn or garden',
          calories: [89, 106, 123, 140],
        },
        {
          name: 'Weeding, cultivating garden',
          calories: [266, 317, 368, 419],
        },
        {
          name: 'Music, playing a cello',
          calories: [118, 141, 163, 186],
        },
        {
          name: 'Music, playing drums',
          calories: [236, 281, 327, 372],
        },
        {
          name: 'Music, playing piano',
          calories: [148, 176, 204, 233],
        },
        {
          name: 'Music, playing trombone',
          calories: [207, 246, 286, 326],
        },
        {
          name: 'Music, playing trumpet',
          calories: [148, 176, 204, 233],
        },
        {
          name: 'Music, playing violin',
          calories: [148, 176, 204, 233],
        },
        {
          name: 'Music, playing guitar',
          calories: [177, 211, 245, 279],
        },
        {
          name: 'Marching band, playing instrument',
          calories: [236, 281, 327, 372],
        },
        {
          name: 'Bakery, light effort',
          calories: [148, 176, 204, 233],
        },
        {
          name: 'Carpentry, general',
          calories: [207, 246, 286, 326],
        },
        {
          name: 'Carrying heavy loads',
          calories: [472, 563, 654, 745],
        },
        {
          name: 'Carrying moderate loads upstairs',
          calories: [472, 563, 654, 745],
        },
        {
          name: 'General cleaning',
          calories: [207, 246, 286, 326],
        },
        {
          name: 'Cleaning, dusting',
          calories: [148, 176, 204, 233],
        },
        {
          name: 'Electrical work, plumbing',
          calories: [207, 246, 286, 326],
        },
        {
          name: 'Coal mining, general',
          calories: [354, 422, 490, 558],
        },
        {
          name: 'Construction, exterior, remodeling',
          calories: [325, 387, 449, 512],
        },
        {
          name: 'Taking out trash',
          calories: [177, 211, 245, 279],
        },
        {
          name: 'Farming, baling hay, cleaning barn',
          calories: [472, 563, 654, 745],
        },
        {
          name: 'Farming, chasing cattle on horseback',
          calories: [236, 281, 327, 372],
        },
        {
          name: 'Farming, driving tractor',
          calories: [148, 176, 204, 233],
        },
        {
          name: 'Farming, feeding small animals',
          calories: [236, 281, 327, 372],
        },
        {
          name: 'Farming, feeding horses or cattle',
          calories: [266, 317, 368, 419],
        },
        {
          name: 'Farming, grooming animals',
          calories: [354, 422, 490, 558],
        },
        {
          name: 'Fire fighter, climbing ladder, full gear',
          calories: [649, 774, 899, 1024],
        },
        {
          name: 'Fire fighter, hauling hoses on ground',
          calories: [472, 563, 654, 745],
        },
        {
          name: 'Forestry, ax chopping, fast',
          calories: [1003, 1196, 1389, 1582],
        },
        {
          name: 'Forestry, ax chopping, slow',
          calories: [295, 352, 409, 465],
        },
        {
          name: 'Forestry, carrying logs',
          calories: [649, 774, 899, 1024],
        },
        {
          name: 'Forestry, sawing by hand',
          calories: [413, 493, 572, 651],
        },
        {
          name: 'Forestry, trimming trees',
          calories: [531, 633, 735, 838],
        },
        {
          name: 'Fishing, general',
          calories: [177, 211, 245, 279],
        },
        {
          name: 'Fishing from riverbank, walking',
          calories: [236, 281, 327, 372],
        },
        {
          name: 'Fishing from boat, sitting',
          calories: [148, 176, 204, 233],
        },
        {
          name: 'Fishing from riverbank, standing',
          calories: [207, 246, 286, 326],
        },
        {
          name: 'Fishing in stream, in waders',
          calories: [354, 422, 490, 558],
        },
        {
          name: 'Fishing, ice fishing',
          calories: [118, 141, 163, 186],
        },
        {
          name: 'Hunting, general',
          calories: [295, 352, 409, 465],
        },
        {
          name: 'Hunting, large game',
          calories: [354, 422, 490, 558],
        },
        {
          name: 'Hunting, small game',
          calories: [295, 352, 409, 465],
        },
        {
          name: 'Pistol shooting, trap shooting',
          calories: [148, 176, 204, 233],
        },
        {
          name: 'Machine tooling, sheet metal',
          calories: [148, 176, 204, 233],
        },
        {
          name: 'Machine tooling, tapping, drilling',
          calories: [236, 281, 327, 372],
        },
        {
          name: 'Masonry, concrete',
          calories: [413, 493, 572, 651],
        },
        {
          name: 'Masseur, masseuse, standing',
          calories: [236, 281, 327, 372],
        },
        {
          name: 'Moving heavy objects, moving van',
          calories: [443, 528, 613, 698],
        },
        {
          name: 'Skindiving or scuba diving',
          calories: [708, 844, 981, 1117],
        },
        {
          name: 'Police, directing traffic, standing',
          calories: [148, 176, 204, 233],
        },
        {
          name: 'Shoveling, digging ditches',
          calories: [502, 598, 695, 791],
        },
        {
          name: 'Sitting, light office work',
          calories: [89, 106, 123, 140],
        },
        {
          name: 'Nursing, patient care',
          calories: [177, 211, 245, 279],
        },
        {
          name: 'Walking, pushing a wheelchair',
          calories: [236, 281, 327, 372],
        },
        {
          name: 'Standing, bartending, store clerk',
          calories: [136, 162, 188, 214],
        },
        {
          name: 'Steel mill, working in general',
          calories: [472, 563, 654, 745],
        },
        {
          name: 'Tailoring, general',
          calories: [148, 176, 204, 233],
        },
        {
          name: 'Truck driving, loading, unloading truck',
          calories: [384, 457, 531, 605],
        },
        {
          name: 'Typing, computer data entry',
          calories: [89, 106, 123, 140],
        },
        {
          name: 'Teach physical education,exercise class',
          calories: [236, 281, 327, 372],
        },
        {
          name: 'Teach exercise classes (& participate)',
          calories: [384, 457, 531, 605],
        },
      ],
    },
  ],
};
