import { UnitsType } from '../enums';

/** Activities data */
export interface ActivitiesInfo {
  /** Activity duration in minutes */
  duration: number;
  /** Weight measurement units */
  weightUnits: UnitsType;
  /** Weight categories*/
  weightCategories: number[];
  /** Activity categories containing data */
  categories: ActivityCategory[];
}

/** Activity category */
export interface ActivityCategory {
  /** Activity Category name */
  category: string;
  /** Activities array */
  activities: ActivityInfo[];
}

/** Activity info  */
export interface ActivityInfo {
  /** Name of activity */
  name: string;
  /** Calories burned */
  calories: number[];
}
