/** Instant nutrition data */
export interface NutritionInstant {
  common: NutritionCommon[];
  branded: NutritionBranded[];
}

/** Instant nutrition data for common food */
export interface NutritionCommon {
  food_name: string;
  serving_unit: string;
  tag_name: string;
  serving_qty: number;
  common_type: string;
  tag_id: string;
  photo: {
    thumb: string;
    highres: string;
    is_user_uploaded: boolean;
  };
  locale: string;
}

/** Instant nutrition data for branded food */
export interface NutritionBranded {
  food_name: string;
  serving_unit: string;
  nix_brand_id: string;
  brand_name_item_name: string;
  serving_qty: number;
  nf_calories: number;
  photo: {
    thumb: string;
    highres: string;
    is_user_uploaded: boolean;
  };
  brand_name: string;
  region: number;
  brand_type: number;
  nix_item_id: string;
  locale: string;
}
