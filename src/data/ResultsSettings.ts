import { WeightGoal, ReportingPeriod } from '../enums';

/** Goals and results data */
export interface ResultsSettings {
  /** Selected weight goal */
  weightGoal: WeightGoal;
  /** Selected weight */
  weight: number;
  /** Reporting period */
  reportingPeriod: ReportingPeriod;
}
