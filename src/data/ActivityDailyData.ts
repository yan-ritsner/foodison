import { ActivityData } from './ActivityData';

/** Activity data grouped by day */
export interface ActivityDailyData {
  /** Day time stamp */
  timestamp: Date;
  /** Total daily calories */
  calories: number;
  /** Daily activity data array */
  data: ActivityData[];
}
