/** Photo data */
export class Photo {
  /** Photo unique id */
  id: number;
  /** Photo timestamp */
  timestamp: Date;
  /** Photo file path */
  filePath: string;
  /** Web view path */
  webviewPath: string;
  /** Base 64 encoded image */
  base64: string;

  /**
   * @constructor
   * @param data - optional raw data
   */
  constructor(data?: any) {
    if (!data) return;
    this.id = data.id;
    this.timestamp = new Date(data.timestamp);
    this.filePath = data.filePath;
    this.base64 = data.base64;
    this.webviewPath = data.webviewPath;
  }

  /** Returns raw data */
  data(isHybridPlatform: boolean = false) {
    return {
      id: this.id,
      timestamp: this.timestamp,
      filePath: this.filePath,
      base64: isHybridPlatform ? this.base64 : undefined,
      webviewPath: this.webviewPath,
    };
  }
}
