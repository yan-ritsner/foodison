import { xo } from '../observable';
import { DataSource } from '../enums';
import { NutritionData, NutritionInfo } from './NutritionInfo';
import { Photo } from './Photo';

/** Food data */
export class FoodData {
  /** Food data unique id */
  id: number;
  /** Food data source */
  source: DataSource;
  /** Food photo id */
  photoId: number;
  /** Food data timestamp */
  timestamp: Date;
  /** Food name */
  name: string;
  /** Food calories */
  calories: number;

  /** Nutrition food data */
  @xo.observable
  nutrition: NutritionInfo;
  /** Selected foods */
  @xo.observable
  foodSelected: boolean[];
  /** Selected foods portion  */
  @xo.observable
  foodPortion: number[];
  /** Foods expand state */
  @xo.observable
  foodExpanded: boolean[];

  /** Food loaded photo  */
  @xo.observable
  photo: Photo;
  /** Loading photo */
  @xo.observable
  loadingPhoto: boolean;
  /** Photo was loaded */
  @xo.observable
  loadedPhoto: boolean;

  /** Data is new - not yet saved to storage */
  @xo.observable
  newData: boolean;

  /**
   * @constructor
   * @param data - optional raw data
   */
  constructor(data?: any) {
    if (!data) return;
    this.id = data.id;
    this.source = data.source;
    this.photoId = data.photoId;
    this.timestamp = new Date(data.timestamp);
    this.name = data.name;
    this.calories = data.calories;
    this.nutrition = data.nutrition;
    this.foodSelected = data.foodSelected;
    this.foodPortion = data.foodPortion;
  }

  /** Returns raw data */
  data() {
    return {
      id: this.id,
      source: this.source,
      photoId: this.photoId,
      timestamp: this.timestamp,
      name: this.name,
      calories: this.calories,
      nutrition: this.nutrition,
      foodSelected: this.foodSelected,
      foodPortion: this.foodPortion,
    };
  }

  /** Initializes new data */
  @xo.action
  initData(source: DataSource, nutrition: NutritionInfo, photo?: Photo) {
    this.source = source;
    this.timestamp = new Date();
    this.setNutrition(nutrition);
    if (photo) this.setPhoto(photo);
    this.setNewData(true);
  }

  /** Accepts new data */
  @xo.action
  acceptsData() {
    this.name = this.getFullName();
    this.calories = this.getTotalCalories();
    this.setNewData(false);
  }

  /** Sets nutrition info */
  @xo.action
  setNutrition(nutrition: NutritionInfo) {
    this.nutrition = nutrition;
    this.nutrition.foods.forEach((food, index) => (food.index = index));
    this.foodSelected = this.nutrition.foods.map(() => false);
    this.foodPortion = this.nutrition.foods.map(() => 1);
  }

  /** Sets loaded photo */
  @xo.action
  setPhoto(photo: Photo) {
    this.photoId = photo.id;
    this.photo = photo;
    this.loadingPhoto = false;
    this.loadedPhoto = true;
  }

  /** Sets loading photo */
  @xo.action
  setLoadingPhoto() {
    this.loadingPhoto = true;
  }

  /** Sets loaded photo */
  @xo.action
  setLoadedPhoto() {
    this.loadedPhoto = true;
  }

  /** Sets loaded photo */
  @xo.action
  setNewData(newData: boolean) {
    this.newData = newData;
  }

  /** Sets food selected state */
  @xo.action
  setFoodSelected(index: number, selected: boolean) {
    if (this.foodSelected) {
      this.foodSelected[index] = selected;
      xo.notify(this, 'foodSelected');
    } else {
      const foodSelected = [];
      foodSelected[index] = selected;
      this.foodSelected = foodSelected;
    }
  }

  /** Sets food portion */
  @xo.action
  setFoodPortion(index: number, portion: number) {
    const p = Math.round(portion * 10) / 10;
    if (this.foodPortion) {
      this.foodPortion[index] = p;
      xo.notify(this, 'foodPortion');
    } else {
      const foodPortion = [];
      foodPortion[index] = p;
      this.foodPortion = foodPortion;
    }
  }

  /** Sets food expanded state */
  @xo.action
  setFoodExpanded(index: number, expanded: boolean) {
    if (this.foodExpanded) {
      this.foodExpanded[index] = expanded;
      xo.notify(this, 'foodExpanded');
    } else {
      const foodExpanded = [];
      foodExpanded[index] = expanded;
      this.foodExpanded = foodExpanded;
    }
  }

  /** Increment food portion */
  @xo.action
  incFoodPortion(index: number) {
    let portion = this.getFoodPortion(index);
    portion += 1;
    this.setFoodPortion(index, portion);
  }

  /** Decrement food portion */
  @xo.action
  decFoodPortion(index: number) {
    let portion = this.getFoodPortion(index);
    if (portion === 0) return;
    portion -= 1;
    this.setFoodPortion(index, portion);
  }

  /** Get food portion share */
  setFoodPortionShare(index: number, share: number) {
    const portion = this.getFoodPortion(index);
    const integer = Math.ceil(portion) - 1;
    this.setFoodPortion(index, integer + share);
    console.log(integer + share);
  }

  /** Returns true if there are food concept is selected */
  hasFoodsSelected() {
    return this.foodSelected && this.foodSelected.some((c) => c);
  }

  /** Returns food nutrition data */
  getFoodData(index: number): NutritionData {
    if (!this.nutrition) return null;
    if (!this.nutrition.foods) return null;
    return this.nutrition.foods[index];
  }

  /** Returns food name  */
  getFoodName(index: number): string {
    const food = this.getFoodData(index);
    return food ? food.food_name : '';
  }

  /** Returns food portion */
  getFoodPortion(index: number): number {
    return this.foodPortion ? this.foodPortion[index] : 1;
  }

  /** Get food portion share */
  getFoodPortionShare(index: number): number {
    const portion = this.getFoodPortion(index);
    const share = portion - Math.floor(portion);
    return share === 0 ? 1 : Math.round(share * 10) / 10;
  }

  /** Returns food selected state */
  getFoodSelected(index: number): boolean {
    return this.foodSelected ? this.foodSelected[index] : false;
  }

  /** Returns food expanded state */
  getFoodExpanded(index: number): boolean {
    return this.foodExpanded ? this.foodExpanded[index] : false;
  }

  /** Returns food serving unit*/
  getFoodServingUnit(index: number): string {
    const food = this.getFoodData(index);
    return food ? food.serving_unit : '';
  }

  /** Returns food serving qty */
  getFoodServingQty(index: number): number {
    const food = this.getFoodData(index);
    return food ? food.serving_qty : 0;
  }

  /** Returns food serving weight (in grams)*/
  getFoodServingWeight(index: number): number {
    const food = this.getFoodData(index);
    return food ? food.serving_weight_grams : 0;
  }

  /** Returns food concept calories*/
  getFoodCalories(index: number) {
    const food = this.getFoodData(index);
    return food ? Math.round(food.nf_calories) : 0;
  }

  /** Returns food portion weight */
  getFoodPortionWeight(index: number): number {
    const portion = this.getFoodPortion(index);
    const servingWeight = this.getFoodServingWeight(index);
    if (servingWeight == null) return 0;
    return Math.round(portion * servingWeight);
  }

  /** Returns food portion calories */
  getFoodPortionCalories(index: number): number {
    const portion = this.getFoodPortion(index);
    const calories = this.getFoodCalories(index);
    if (calories == null) return 0;
    return Math.round(portion * calories);
  }

  /** Returns all foods */
  getFoods(): NutritionData[] {
    const foods: NutritionData[] = [];
    if (!this.nutrition) return foods;
    if (!this.nutrition.foods) return foods;
    return this.nutrition.foods;
  }

  /** Returns foods sorted by calories */
  getFoodsByCalories(): NutritionData[] {
    const foods: NutritionData[] = [];
    if (!this.nutrition) return foods;
    if (!this.nutrition.foods) return foods;
    return this.nutrition.foods.slice().sort((c1, c2) => {
      return c2.nf_calories - c1.nf_calories;
    });
  }

  /** Returns selected foods nutrition data */
  getSelectedFoods(): NutritionData[] {
    const foods: NutritionData[] = [];
    if (!this.nutrition) return foods;
    if (!this.nutrition.foods) return foods;
    this.nutrition.foods.forEach((f, i) => {
      if (this.getFoodSelected(i)) foods.push(f);
    });
    return foods;
  }

  /** Returns selected foods total calories */
  getSelectedFoodsCalories(): number {
    let calories = 0;
    this.nutrition.foods.forEach((_, i) => {
      if (!this.getFoodSelected(i)) return;
      calories += this.getFoodCalories(i);
    });
    return calories;
  }

  /** Returns full food name (comma separated list) */
  getFullName(): string {
    const foods = this.getSelectedFoods();
    return foods.map((f) => f.food_name).join(', ');
  }

  /** Returns selected foods total calories taking into account portion */
  getTotalCalories(): number {
    let calories = 0;
    this.nutrition.foods.forEach((f, i) => {
      if (!this.getFoodSelected(i)) return;
      calories += this.getFoodPortionCalories(i);
    });
    return calories;
  }

  /** Returns photo url */
  getPhotoUrl(thumb: boolean = true): string {
    if (this.photo != null) {
      return this.photo.webviewPath ?? this.photo.base64;
    } else if (
      this.nutrition &&
      this.nutrition.foods &&
      this.nutrition.foods[0]
    ) {
      const food = this.nutrition.foods[0];
      return thumb || !food.photo.highres
        ? food.photo.thumb
        : food.photo.highres;
    } else {
      return '';
    }
  }
}
