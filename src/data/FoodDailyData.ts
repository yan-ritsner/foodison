import { FoodData } from './FoodData';

/** Food data grouped by day */
export interface FoodDailyData {
  /** Day time stamp */
  timestamp: Date;
  /** Total daily calories */
  calories: number;
  /** Daily activity data array */
  data: FoodData[];
}
