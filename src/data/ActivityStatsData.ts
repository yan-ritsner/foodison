/** Activity statistics data */
export class ActivityStatsData {
  /** Activity name */
  activity: string;
  /** Number of times activity has been practiced */
  count: number;
  /** Total Activity duration in minutes */
  duration: number;
  /** Total Calories burned during activity */
  calories: number;

  /** Average activity duration */
  get avgDuration() {
    let duration = this.count === 0 ? 0 : this.duration / this.count;
    duration = Math.round(duration / 10) * 10;
    return duration;
  }

  /** Average calories consumed */
  get avgCalories() {
    let calories = this.count === 0 ? 0 : this.calories / this.count;
    calories = Math.round(calories / 10) * 10;
    return calories;
  }
}
