/** Food nutrients info */
export interface NutritionInfo {
  /** Food nutrients data array */
  foods: NutritionData[];
}

/** Food nutrients data */
export interface NutritionData {
  index: number;
  food_name: string;
  brand_name: string;
  serving_qty: number;
  serving_unit: string;
  serving_weight_grams: number;
  nf_calories: number;
  nf_total_fat: number;
  nf_saturated_fat: number;
  nf_cholesterol: number;
  nf_sodium: number;
  nf_total_carbohydrate: number;
  nf_dietary_fiber: number;
  nf_sugars: number;
  nf_protein: number;
  nf_potassium: number;
  nf_p: number;
  photo: { thumb: string; highres: string };
}
