/** Photo prediction data */
export interface Prediction {
  /** Photo class name */
  className: string;
  /** Predication probability */
  probability: number;
}
