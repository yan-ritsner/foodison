import { System } from './../system';

/** Settings keys */
export class SettingKeys {
  /** Anthropometry settings key */
  public static get anthropometry() {
    return this.getSettingName('anthropometry_v3');
  }
  /** Sync activity settings key */
  public static get syncActivity() {
    return this.getSettingName('sync_activity');
  }
  /** Sync activity requested settings key */
  public static get syncRequested() {
    return this.getSettingName('sync_requested');
  }
  /** Theme settings key */
  public static get theme() {
    return this.getSettingName('theme_v1');
  }
  /** Activity data settings key */
  public static get activityData() {
    return this.getSettingName('activity_data_v12');
  }
  /** Food data settings key */
  public static get foodData() {
    return this.getSettingName('food_data_v9');
  }
  /** Photo storage settings key */
  public static get photoStorage() {
    return this.getSettingName('photo_gallery_v7');
  }

  /** Results settings key */
  public static get resultSettings() {
    return this.getSettingName('result_settings');
  }

  /** Card view settings key */
  public static get cardView() {
    return this.getSettingName('card_view');
  }

  /** Get full settings name including uid */
  public static getSettingName(name: string) {
    const uid = System.user.info?.uid;
    return uid ? `${name}_${uid}` : name;
  }
}
