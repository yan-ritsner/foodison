import { ExerciseData } from './ExerciseData';

/** Exercise info returned from API */
export interface ExerciseInfo {
  /** Exercises data array */
  exercises: ExerciseData[];
}
