/** Exercise data returned from API */
export interface ExerciseData {
  tag_id: number;
  user_input: string;
  duration_min: number;
  met: number;
  nf_calories: number;
  photo: { thumb: string; highres: string };
  compendium_code: number;
  name: string;
}
