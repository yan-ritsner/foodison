import * as React from 'react';
import {
  IonContent,
  IonList,
  IonRadioGroup,
  IonListHeader,
  IonLabel,
  IonItem,
  IonRadio,
  IonLoading,
  IonCard,
} from '@ionic/react';
import { xo } from '../../observable';
import { cleanUpModes } from '../../enums';
import { ViewProps } from '../../props';
import { CleanupViewModel } from './CleanupViewModel';
import { CleanupAcceptButton } from './CleanupAcceptButton';

import './Cleanup.css';

/** Cleanup View component props */
export interface CleanupViewProps extends ViewProps<CleanupViewModel> {}

/** Cleanup View component */
@xo.observer
export class CleanupView extends React.Component<CleanupViewProps> {
  render() {
    const viewModel = this.props.viewModel;
    return (
      <IonContent class="app-background">
        <IonCard>
          <IonList lines="none">
            <IonRadioGroup
              value={viewModel.mode}
              onIonChange={(e) => viewModel.setMode(e.detail.value)}
            >
              <IonListHeader>
                <IonLabel>Clean up old data</IonLabel>
              </IonListHeader>

              {cleanUpModes.map((mode) => (
                <IonItem key={mode}>
                  <IonLabel>{mode}</IonLabel>
                  <IonRadio slot="start" value={mode} />
                </IonItem>
              ))}
            </IonRadioGroup>
          </IonList>
        </IonCard>

        <CleanupAcceptButton viewModel={viewModel}></CleanupAcceptButton>

        <IonLoading
          isOpen={viewModel.loading}
          message={viewModel.loadingText}
        />
      </IonContent>
    );
  }
}
