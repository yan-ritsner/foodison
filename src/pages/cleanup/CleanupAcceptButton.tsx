import * as React from 'react';
import { IonFab, IonFabButton, IonIcon } from '@ionic/react';
import { xo } from '../../observable';
import { trashBinOutline } from 'ionicons/icons';
import { ViewProps } from '../../props';
import { CleanupViewModel } from './CleanupViewModel';

/** Cleanup Accept button component props */
export interface CleanupAcceptButtonProps extends ViewProps<CleanupViewModel> {}

/** Cleanup Accept button component */
@xo.observer
export class CleanupAcceptButton extends React.Component<
  CleanupAcceptButtonProps
> {
  render() {
    const viewModel = this.props.viewModel;
    return (
      <IonFab vertical="bottom" horizontal="center" slot="fixed">
        <IonFabButton
          disabled={viewModel.mode == null}
          onClick={() => viewModel.cleanUp()}
        >
          <IonIcon icon={trashBinOutline}></IonIcon>
        </IonFabButton>
      </IonFab>
    );
  }
}
