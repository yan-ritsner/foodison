import { startOfToday, subMonths, subDays } from 'date-fns';
import { xo } from '../../observable';
import { System } from '../../system';
import { PageViewModel } from '../page';
import { CleanUpMode, ServiceType } from '../../enums';
import {
  IActivityDataService,
  IPhotoStorageService,
  IFoodDataService,
} from '../../services/';

/** Cleanup view model */
export class CleanupViewModel extends PageViewModel {
  /** Clean up mode */
  @xo.observable
  public mode: CleanUpMode;

  /** Loading indicator */
  @xo.observable
  public loading: boolean = false;
  /** Loading indicator text */
  @xo.observable
  public loadingText: string | null = null;

  /** Sets clean up mode */
  @xo.action
  public setMode(mode: CleanUpMode) {
    this.mode = mode;
  }

  /** Sets loading indicator and text */
  @xo.action
  public setLoading(loading: boolean, loadingText: string | null = null) {
    this.loading = loading;
    this.loadingText = loadingText;
  }

  /** Sets clean up mode */
  @xo.action
  public cleanUp() {
    let photosOnly: boolean = false;
    let date: Date | null = null;
    this.setLoading(true, 'Cleaning up...');

    switch (this.mode) {
      case CleanUpMode.DeleteAllData:
        photosOnly = false;
        break;
      case CleanUpMode.DeleteAllPhotos:
        photosOnly = true;
        break;
      case CleanUpMode.DeleteDataMonthOld:
        photosOnly = false;
        date = subMonths(startOfToday(), 1);
        break;
      case CleanUpMode.DeletePhotoMonthOld:
        photosOnly = true;
        date = subMonths(startOfToday(), 1);
        break;
      case CleanUpMode.DeleteDataWeekOld:
        photosOnly = false;
        date = subDays(startOfToday(), 7);

        break;
      case CleanUpMode.DeletePhotoWeekOld:
        photosOnly = true;
        date = subDays(startOfToday(), 7);

        break;
    }

    if (photosOnly) {
      this.cleanUpFoodData(date, true);
    } else {
      this.cleanUpFoodData(date, false);
      this.cleanUpActivityData(date);
    }

    this.setLoading(false);

    if (this.history) this.history.goBack();
  }

  /** Cleans up food data */
  private async cleanUpFoodData(date: Date | null, photosOnly: boolean) {
    const foodService = this.getService<IFoodDataService>(ServiceType.FoodData);
    const photoService = System.getService<IPhotoStorageService>(
      ServiceType.PhotoStorage
    );

    await foodService.getInitial();
    while (foodService.hasMore()) {
      await foodService.getMore();
    }
    await photoService.get();

    const data = foodService.data.slice();
    data.forEach(async (food) => {
      if (date == null || food.timestamp.valueOf() < date.valueOf()) {
        const photo = await photoService.getById(food.photoId);
        if (photo) await photoService.delete(photo);
        if (!photosOnly) await foodService.delete(food);
      }
    });
  }

  /** Cleans up activity data */
  private async cleanUpActivityData(date: Date | null) {
    const activityService = System.getService<IActivityDataService>(
      ServiceType.ActivityData
    );

    await activityService.getInitial();
    while (activityService.hasMore()) {
      await activityService.getMore();
    }

    const data = activityService.data.slice();
    data.forEach(async (activity) => {
      if (date == null || activity.timestamp.valueOf() < date.valueOf()) {
        await activityService.delete(activity);
      }
    });
  }
}
