import React from 'react';
import StyledFirebaseAuth from 'react-firebaseui/StyledFirebaseAuth';
import * as firebase from 'firebase/app';
import 'firebase/auth';

import {
  IonButton,
  IonCard,
  IonCardContent,
  IonContent,
  IonHeader,
  IonLoading,
  IonPage,
  IonText,
  IonTitle,
  IonToolbar,
} from '@ionic/react';

import { xo } from '../../observable';
import { System } from '../../system';
import { PageCopyright, PageLogo } from '../page';

import './Login.css';

// Configure Firebase.
const config = {
  apiKey: 'AIzaSyDVIsQWUm6hNbUdKn__msWJpPyB9Fas44s',
  authDomain: 'foodison-30e95.firebaseapp.com',
  // ...
};
firebase.initializeApp(config);

// Configure FirebaseUI.
const uiConfig: firebaseui.auth.Config = {
  // Popup sign-in flow rather than redirect flow.
  signInFlow: 'popup',
  // Redirect to / after sign in is successful.
  signInSuccessUrl: '/',
  // We will display Email as auth provider.
  signInOptions: [
    {
      provider: firebase.auth.EmailAuthProvider.PROVIDER_ID,
      signInMethod:
        firebase.auth.EmailAuthProvider.EMAIL_PASSWORD_SIGN_IN_METHOD,
    },
  ],
  callbacks: {
    // Avoid redirects after sign-in.
    signInSuccessWithAuthResult: () => false,
  },
  credentialHelper: 'none',
};

/** Login page */
@xo.observer
export class Login extends React.Component {
  /** Auth observer unsubscribe callback */
  private unregisterAuthObserver: firebase.Unsubscribe;

  public render() {
    let content: JSX.Element;
    if (System.user.isAuthenticated) {
      if (System.user.isVerified) {
        content = this.renderVerified();
      } else {
        content = this.renderVerifying();
      }
    } else {
      content = this.renderAuth();
    }
    return this.renderContent(content);
  }

  /** Render login content */
  private renderContent(content: JSX.Element) {
    return (
      <IonPage>
        <IonHeader>
          <IonToolbar color={System.theme.headerFooterColor}>
            <PageLogo marginLeft={'10px'}></PageLogo>
            <IonTitle slot="end">
              <IonText>Login</IonText>
            </IonTitle>
          </IonToolbar>
        </IonHeader>
        <IonContent class="app-background">
          {content && System.user.isInitialized && (
            <IonCard className={'fade-in'}>
              <IonCardContent style={{ paddingBottom: '25px' }}>
                <div>{content}</div>
                <PageCopyright></PageCopyright>
              </IonCardContent>
            </IonCard>
          )}
          <IonLoading
            isOpen={!System.user.isInitialized}
            message={'Loading...'}
          />
        </IonContent>
      </IonPage>
    );
  }

  /** Render email being verified indicator */
  private renderVerifying() {
    return (
      <div>
        <h1>Confirm you email address.</h1>
        <br></br>
        <div>1. Please check you inbox for confirmation email.</div>
        <div>2. Click the link in the email to confirm you email address.</div>
        <div>3. Press sign in button, after verification.</div>
        <br></br>
        <div
          style={{
            textAlign: 'right',
            marginRight: '20px',
            marginBottom: '30px',
          }}
        >
          <IonButton fill={'clear'} onClick={() => this.signOut()}>
            Sign Out
          </IonButton>
          <IonButton onClick={() => this.refreshAuth()}>Sign In</IonButton>
        </div>
      </div>
    );
  }

  /** Render email verified indicator */
  private renderVerified() {
    return (
      <div>
        <h1>Email Verified.</h1>
      </div>
    );
  }

  /** Render firebase auth ui */
  private renderAuth() {
    return (
      <StyledFirebaseAuth uiConfig={uiConfig} firebaseAuth={firebase.auth()} />
    );
  }

  // Listen to the Firebase Auth state and set the local state.
  public componentDidMount() {
    this.unregisterAuthObserver = firebase
      .auth()
      .onAuthStateChanged((user) => this.onAuthStateChanged(user));
  }

  // Make sure we un-register Firebase observers when the component un-mounts.
  public componentWillUnmount() {
    this.unregisterAuthObserver();
  }

  /** Auth state changed event handler */
  @xo.action
  private onAuthStateChanged(user: firebase.User) {
    if (System.user.signIn(user)) {
      const props = this.props as any;
      if (props.history.location.pathname === props.location.state.from) {
        props.history.push(System.defaultPage.route);
      } else {
        props.history.push(props.location.state.from);
      }
    }
  }

  /** Refreshed user auth info */
  private async refreshAuth() {
    const user = firebase.auth().currentUser;
    if (user == null) return;
    await firebase.auth().currentUser.reload();
    if (user.emailVerified) this.onAuthStateChanged(user);
  }

  /** Signs out current user */
  private signOut() {
    firebase.auth().signOut();
  }
}
