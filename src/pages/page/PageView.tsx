import React, { createRef } from 'react';
import {
  createAnimation,
  IonContent,
  IonSplitPane,
  IonHeader,
  IonPage,
} from '@ionic/react';
import { ViewProps } from '../../props';
import { PageViewModel } from './PageViewModel';
import { PageMenu } from './PageMenu';
import { PageToolbar } from './PageToolbar';

/** Page view component props */
export interface PageViewProps extends ViewProps<PageViewModel> {}

/** Page view component props */
export class PageView extends React.Component<PageViewProps> {
  /** Content element ref */
  ref: React.RefObject<HTMLIonContentElement>;

  /** @constructor */
  constructor(props: PageViewProps) {
    super(props);
    this.ref = createRef();
  }

  render() {
    const viewModel = this.props.viewModel;

    const view: JSX.Element =
      typeof viewModel.info.view === 'function'
        ? viewModel.info.view(viewModel)
        : viewModel.info.view;

    return (
      <IonContent ref={this.ref}>
        <IonSplitPane contentId="main">
          {/*-- side menu  --*/}
          <PageMenu viewModel={viewModel}></PageMenu>
          {/*-- main content --*/}
          <IonPage id="main" ref={viewModel.ref}>
            {/*-- page toolbar --*/}
            <IonHeader>
              <PageToolbar viewModel={viewModel}></PageToolbar>
            </IonHeader>
            {/*-- page content --*/}
            <IonContent>{view}</IonContent>
          </IonPage>
        </IonSplitPane>
      </IonContent>
    );
  }

  componentDidMount() {
    const animation = createAnimation('')
      .addElement(this.ref.current)
      .duration(500)
      .fromTo('opacity', '0', '1');
    animation.play();
  }
}
