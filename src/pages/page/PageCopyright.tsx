import React from 'react';

/** Page copyright component props */
export interface PageCopyrightProps {}

/** Page copyright component */
export class PageCopyright extends React.Component<PageCopyrightProps> {
  render() {
    return (
      <div
        style={{
          position: 'absolute',
          left: '10px',
          bottom: '5px',
          right: '10px',
          fontSize: 12,
          textAlign: 'center',
        }}
      >
        Copyright © 2021 Foodison. All Rights Reserved.
      </div>
    );
  }
}
