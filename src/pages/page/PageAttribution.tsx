import React from 'react';
import { xo } from '../../observable';
import { System } from '../../system';

export interface PageAttributionProps {
  style?: React.CSSProperties;
}

/** Page attribution logo */
@xo.observer
export class PageAttribution extends React.Component<PageAttributionProps> {
  render() {
    const src = System.theme.darkTheme
      ? '/assets/attrib-white.png'
      : '/assets/attrib-black.png';
    const style = this.props.style;
    return <img alt="" height={40} src={src} style={style}></img>;
  }
}
