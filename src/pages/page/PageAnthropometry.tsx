import React from 'react';
import { xo } from '../../observable';
import { IonItem, IonIcon, IonAlert, IonCard } from '@ionic/react';
import { alertCircleOutline } from 'ionicons/icons';
import { System } from '../../system';

/** Page Anthropometry component */
@xo.observer
export class PageAnthropometry extends React.Component<any> {
  render() {
    if (System.user.anthropometry != null) return '';
    const url = `/anthropometry`;
    const okHandler = () => {
      if (System.activePage && System.activePage.history) {
        System.activePage.history.push(url);
      }
    };
    const cancelAlert = () => {
      System.user.showAnthropometryAlert = false;
    };
    return (
      <IonCard>
        <IonItem lines="none" routerLink={url}>
          <IonIcon
            color={'primary'}
            icon={alertCircleOutline}
            style={{ marginRight: 5 }}
          ></IonIcon>
          Set up anthropometry settings
        </IonItem>
        <IonAlert
          isOpen={System.user.showAnthropometryAlert}
          onDidDismiss={() => cancelAlert()}
          message={
            'In order to count burned calories from activities, ' +
            'please set up your <strong> gender, age, weight and height</strong>. '
          }
          buttons={[
            {
              text: 'Maybe later',
              role: 'cancel',
              handler: () => cancelAlert(),
            },
            {
              text: 'Set up now',
              handler: () => okHandler(),
            },
          ]}
        />
      </IonCard>
    );
  }
}
