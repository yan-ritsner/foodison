import React from 'react';
import { PageInfo } from './PageInfo';
import { PageViewModel } from './PageViewModel';
import { System } from '../../system';
import { PageView } from './PageView';

/** Page container component props */
export interface PageContainerProps {
  /** Page info */
  info: PageInfo;
  /** Current location */
  location: string;
  /** Url search params */
  search: URLSearchParams;
  /** Page params */
  params: any;
  /** Browser history */
  history: any;
}

/** Page container component */
export class PageContainer extends React.Component<PageContainerProps> {
  render() {
    const info = this.props.info;
    const location = this.props.location;
    const history = this.props.history;
    const params = this.props.params;
    const search = this.props.search;

    if (location !== info.route) {
      return '';
    }

    if (System.activePage?.info !== info) {
      // create view model
      const viewModel = info.viewModel
        ? info.viewModel(params)
        : new PageViewModel();
      viewModel.info = info;
      viewModel.history = history;
      viewModel.params = params;
      viewModel.search = search;
      // dispose current active view model
      if (System.activePage != null) {
        System.activePage.dispose();
      }
      // assign new active page
      System.activePage = viewModel;
      System.googleAnalytics.pageView(info.route);
    }

    // return page
    return <PageView viewModel={System.activePage}></PageView>;
  }
}
