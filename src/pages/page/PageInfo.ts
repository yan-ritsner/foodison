import { PageViewModel } from './PageViewModel';

/** Page descriptor */
export interface PageInfo<T extends PageViewModel = PageViewModel> {
  /** Page route */
  route: string;
  /** Page display name */
  name: string;
  /** Page icon */
  icon: string;

  /** Page is default index page */
  default?: boolean;
  /** Page is hidden from the menu */
  tab?: boolean;

  /** Page content view model */
  viewModel?: (params?: any) => T;
  /** Page content view */
  view?: React.ReactNode | ((viewModel: T) => React.ReactNode);
  /** Page custom toolbar items */
  toolbar?: React.ReactNode | ((viewModel: T) => React.ReactNode);
  /** Page custom menu items */
  menu?: React.ReactNode | ((viewModel: T) => React.ReactNode);
}
