import React from 'react';
import {
  IonContent,
  IonMenu,
  IonHeader,
  IonToolbar,
  IonTitle,
  IonList,
  IonListHeader,
  IonItem,
  IonLabel,
  IonIcon,
  IonAvatar,
  IonToggle,
} from '@ionic/react';
import { xo } from '../../observable';
import { System } from '../../system';
import { ViewProps } from '../../props';
import { PageViewModel } from './PageViewModel';
import {
  bodyOutline,
  colorPaletteOutline,
  logOutOutline,
  mailOutline,
  moonOutline,
  personCircleOutline,
  trashOutline,
} from 'ionicons/icons';

/** Page menu component props */
export interface PageMenuProps extends ViewProps<PageViewModel> {}

/** Page menu component props */
@xo.observer
export class PageMenu extends React.Component<PageMenuProps> {
  render() {
    const viewModel = this.props.viewModel;

    const menu: JSX.Element =
      typeof viewModel.info.menu === 'function'
        ? viewModel.info.menu(viewModel)
        : viewModel.info.menu;

    return (
      <IonMenu contentId="main" style={{ maxWidth: '300px' }}>
        <IonHeader>
          <IonToolbar
            color={System.theme.headerFooterColor}
            style={{ minHeight: '50px' }}
          >
            <IonTitle>Menu</IonTitle>
          </IonToolbar>
        </IonHeader>
        <IonContent>
          <IonList lines="none">
            <IonListHeader>User</IonListHeader>
            <IonItem>
              {System.user.info.photoURL && (
                <IonAvatar slot="start">
                  <img src={System.user.info.photoURL} alt="" />
                </IonAvatar>
              )}
              {!System.user.info.photoURL && (
                <IonIcon slot="start" icon={personCircleOutline}></IonIcon>
              )}
              <IonLabel>{System.user.info.displayName}</IonLabel>
            </IonItem>
            <IonItem button onClick={() => System.user.signOut()}>
              <IonIcon slot="start" icon={logOutOutline}></IonIcon>
              <IonLabel>Sign Out</IonLabel>
            </IonItem>

            <IonListHeader>Settings</IonListHeader>
            <IonItem routerLink="/anthropometry">
              <IonIcon slot="start" icon={bodyOutline}></IonIcon>
              <IonLabel>Anthropometry</IonLabel>
            </IonItem>
            <IonItem>
              <IonIcon slot="start" icon={personCircleOutline}></IonIcon>
              <IonLabel>Sync Activity</IonLabel>
              <IonToggle
                checked={System.user.syncActivity}
                onIonChange={(e) =>
                  System.user.toggleSyncActivity(e.detail.checked)
                }
              />
            </IonItem>

            <IonListHeader>Interface</IonListHeader>
            <IonItem>
              <IonIcon slot="start" icon={moonOutline}></IonIcon>
              <IonLabel>Dark Mode</IonLabel>
              <IonToggle
                checked={System.theme.darkTheme}
                onIonChange={(e) => System.theme.toggleTheme(e.detail.checked)}
              />
            </IonItem>
            <IonItem>
              <IonIcon slot="start" icon={colorPaletteOutline}></IonIcon>
              <IonLabel>Dark Header &amp; Footer</IonLabel>
              <IonToggle
                checked={System.theme.darkFooterHeader}
                onIonChange={(e) =>
                  System.theme.toggleFooterHeader(e.detail.checked)
                }
              />
            </IonItem>
            <IonItem>
              <IonIcon slot="start" icon={colorPaletteOutline}></IonIcon>
              <IonLabel>Blue Background</IonLabel>
              <IonToggle
                checked={System.theme.blueBackground}
                onIonChange={(e) =>
                  System.theme.toggleBackground(e.detail.checked)
                }
              />
            </IonItem>

            <IonListHeader>Clean up</IonListHeader>
            <IonItem routerLink="/cleanup">
              <IonIcon slot="start" icon={trashOutline}></IonIcon>
              <IonLabel>Clean up old data</IonLabel>
            </IonItem>

            <IonListHeader>Support</IonListHeader>
            <IonItem href={'mailto:support@foodison.net'}>
              <IonIcon slot="start" icon={mailOutline}></IonIcon>
              <IonLabel>support@foodison.net</IonLabel>
            </IonItem>
          </IonList>
          {menu}
        </IonContent>
      </IonMenu>
    );
  }
}
