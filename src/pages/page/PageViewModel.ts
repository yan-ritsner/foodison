import { createRef } from 'react';
import { PageInfo } from './PageInfo';
import { IDisposable } from '../../disposable';
import { System } from '../../system';

export class PageViewModel implements IDisposable {
  /** Services being consumed by page */
  private services: Map<number, any> = new Map();
  /** Page info */
  public info: PageInfo;
  /** Browser history */
  public history: any;
  /** Route params */
  public params: any;
  /** Route search params */
  public search: URLSearchParams;
  /** Page ref */
  public ref: React.RefObject<HTMLElement>;

  /** @constructor */
  constructor() {
    this.ref = createRef<HTMLElement>();
  }

  /**
   * Returns service by type
   * @param type - service type
   */
  public getService<T>(type: number): T {
    // check if service was consumed before then use it
    if (this.services.has(type)) return this.services.get(type);
    // get service from the system and store it locally
    const service = System.getService<T>(type);
    this.services.set(type, service);
    return service;
  }

  /** Disposed view model */
  public dispose(): void {
    // services clean up - dispose disposable ones
    this.services.forEach((service) => {
      const disposable = service as IDisposable;
      if (disposable.dispose) {
        disposable.dispose();
      }
    });
    this.services.clear();
  }
}
