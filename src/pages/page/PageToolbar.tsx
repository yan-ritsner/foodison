import React from 'react';
import { IonToolbar, IonTitle, IonButtons, IonMenuButton } from '@ionic/react';
import { System } from '../../system';
import { xo } from '../../observable';
import { ViewProps } from '../../props';
import { PageViewModel } from './PageViewModel';
import { PageLogo } from './PageLogo';

/** Page toolbar component props */
export interface PageToolbarProps extends ViewProps<PageViewModel> {}

/** Page toolbar component props */
@xo.observer
export class PageToolbar extends React.Component<PageToolbarProps> {
  render() {
    const viewModel = this.props.viewModel;

    const toolbar: JSX.Element =
      typeof viewModel.info.toolbar === 'function'
        ? viewModel.info.toolbar(viewModel)
        : viewModel.info.toolbar;

    return (
      <IonToolbar color={System.theme.headerFooterColor}>
        <IonButtons slot="start">
          <IonMenuButton className="main-menu"></IonMenuButton>
        </IonButtons>
        <PageLogo></PageLogo>
        <IonTitle slot="end">{viewModel.info.name}</IonTitle>
        {toolbar}
      </IonToolbar>
    );
  }
}
