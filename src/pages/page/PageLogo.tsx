import React from 'react';

/** Page logo component props */
export interface PageLogoProps {
  marginLeft?: string | number;
}

/** Page logo component */
export class PageLogo extends React.Component<PageLogoProps> {
  render() {
    var marginLeft = this.props.marginLeft;
    return (
      <img
        src={'/assets/logo.png'}
        height="40px"
        width="115px"
        alt=""
        style={{ maxWidth: 'none', marginLeft: marginLeft }}
      ></img>
    );
  }
}
