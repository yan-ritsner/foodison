import React from 'react';
import {
  IonModal,
  IonContent,
  IonHeader,
  IonToolbar,
  IonTitle,
  IonButtons,
  IonButton,
  IonIcon,
} from '@ionic/react';
import { closeCircleOutline } from 'ionicons/icons';
import { xo } from '../../observable';
import { System } from '../../system';
import { ViewProps } from '../../props';
import { PageLogo } from '../page';
import { ActivityViewModel } from './ActivityViewModel';
import { ActivityDetails } from './ActivityDetails';
import { ActivityAcceptButton } from './ActivityAcceptButton';

/** Activity Modal component props */
export interface ActivityModalProps extends ViewProps<ActivityViewModel> {}

/** Activity Modal component */
@xo.observer
export class ActivityModal extends React.Component<ActivityModalProps> {
  render() {
    const viewModel = this.props.viewModel;
    return (
      <IonModal
        isOpen={viewModel.showModal}
        swipeToClose={!viewModel.loading}
        presentingElement={viewModel.ref.current ?? undefined}
        onDidDismiss={() => viewModel.hideModal()}
      >
        <IonHeader translucent>
          <IonToolbar color={System.theme.headerFooterColor}>
            <PageLogo marginLeft={'10px'}></PageLogo>
            <IonTitle slot="end">Activity</IonTitle>
            <IonButtons slot="end">
              <IonButton
                onClick={() => viewModel.hideModal()}
                disabled={viewModel.loading}
              >
                <IonIcon
                  slot="icon-only"
                  icon={closeCircleOutline}
                  color="primary"
                ></IonIcon>
              </IonButton>
            </IonButtons>
          </IonToolbar>
        </IonHeader>
        <IonContent fullscreen class="app-background">
          <ActivityDetails viewModel={viewModel}></ActivityDetails>
          <ActivityAcceptButton viewModel={viewModel}></ActivityAcceptButton>
        </IonContent>
      </IonModal>
    );
  }
}
