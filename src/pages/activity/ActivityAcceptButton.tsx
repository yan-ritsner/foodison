import React from 'react';
import { IonFab, IonFabButton, IonIcon } from '@ionic/react';
import { checkmark } from 'ionicons/icons';
import { xo } from '../../observable';
import { ViewProps } from '../../props';
import { ActivityViewModel } from './ActivityViewModel';

/** Activity Accept Button component props */
export interface ActivityAcceptButtonProps
  extends ViewProps<ActivityViewModel> {}

/** Activity Accept Button component */
@xo.observer
export class ActivityAcceptButton extends React.Component<
  ActivityAcceptButtonProps
> {
  render() {
    const viewModel = this.props.viewModel;
    return (
      <IonFab vertical="bottom" horizontal="center" slot="fixed">
        <IonFabButton
          onClick={() => viewModel.saveActivity()}
          disabled={viewModel.loading}
        >
          <IonIcon icon={checkmark}></IonIcon>
        </IonFabButton>
      </IonFab>
    );
  }
}
