import React from 'react';
import {
  IonList,
  IonItem,
  IonGrid,
  IonRow,
  IonCol,
  IonLabel,
  IonSelect,
  IonSelectOption,
  IonListHeader,
  IonDatetime,
} from '@ionic/react';
import { xo } from '../../observable';
import { ViewProps } from '../../props';
import { ActivityViewModel } from './ActivityViewModel';

/** Activity Statistics component props */
export interface ActivityStatsProps extends ViewProps<ActivityViewModel> {}

/** Activity Statistics component */
@xo.observer
export class ActivityStats extends React.Component<ActivityStatsProps> {
  render() {
    const viewModel = this.props.viewModel;
    return (
      <IonList lines="none">
        <IonListHeader>Recent Activity</IonListHeader>
        {viewModel.statsData.map((data) => (
          <IonItem
            key={data.activity}
            onClick={() => viewModel.selectStats(data)}
          >
            <IonGrid>
              <IonRow>
                <IonCol>{data.activity}</IonCol>
                <IonCol className="ion-text-end">
                  {`${data.avgDuration} Min.`}
                </IonCol>
                <IonCol className="ion-text-end">
                  {`${data.avgCalories} Cal.`}
                </IonCol>
              </IonRow>
            </IonGrid>
          </IonItem>
        ))}
        {viewModel.selectedStats && (
          <>
            <IonListHeader>Selected Activity</IonListHeader>
            <IonItem>{viewModel.selectedStats.activity}</IonItem>
            <IonItem>
              <IonLabel position="floating">Time</IonLabel>
              <IonDatetime
                displayFormat="MMM DD, YYYY HH:mm"
                placeholder="Select Time"
                value={viewModel.timestamp?.toString()}
                onIonChange={(e) => {
                  if (e.detail.value) {
                    viewModel.setTimestamp(new Date(e.detail.value));
                  }
                }}
              ></IonDatetime>
            </IonItem>
            <IonItem>
              <IonLabel position="floating">Duration (min.)</IonLabel>
              <IonSelect
                value={viewModel.duration}
                okText="Ok"
                cancelText="Cancel"
                interface="action-sheet"
                onIonChange={(e) =>
                  viewModel.setDuration(Number(e.detail.value))
                }
              >
                {viewModel.durationsExtra.map((duration) => (
                  <IonSelectOption key={duration} value={duration}>
                    {duration}
                  </IonSelectOption>
                ))}
              </IonSelect>
            </IonItem>
          </>
        )}
      </IonList>
    );
  }
}
