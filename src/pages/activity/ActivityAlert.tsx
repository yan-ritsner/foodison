import * as React from 'react';
import { IonAlert, AlertButton } from '@ionic/react';
import { xo } from '../../observable';
import { ViewProps } from '../../props';
import { ActivityViewModel } from './ActivityViewModel';

/** Activity Alert component props */
export interface ActivityAlertProps extends ViewProps<ActivityViewModel> {}

/** Activity Alert component */
@xo.observer
export class ActivityAlert extends React.Component<ActivityAlertProps> {
  render() {
    const viewModel = this.props.viewModel;
    const buttons: AlertButton[] = [];
    if (viewModel.alertCancel) {
      buttons.push({
        text: viewModel.alertCancelText,
        handler: () => this.cancelHandler(),
      });
    }
    buttons.push({
      text: viewModel.alertOkText,
      handler: () => this.okHandler(),
    });

    return (
      <IonAlert
        isOpen={viewModel.alert}
        onDidDismiss={() => this.cancelHandler()}
        message={viewModel.alertText}
        buttons={buttons}
      />
    );
  }

  /** Ok button handler */
  okHandler() {
    const viewModel = this.props.viewModel;
    viewModel.setAlert(false);
    if (viewModel.onAlertOk) {
      viewModel.onAlertOk();
      viewModel.onAlertOk = null;
    }
    viewModel.alertCancel = false;
  }

  /** Cancel button handler */
  cancelHandler() {
    const viewModel = this.props.viewModel;
    viewModel.setAlert(false);
    if (viewModel.onAlertCancel) {
      viewModel.onAlertCancel();
      viewModel.onAlertCancel = null;
    }
    viewModel.alertCancel = false;
  }
}
