import React from 'react';
import { IonFab, IonFabButton, IonIcon, IonFabList } from '@ionic/react';
import { add, micOutline, searchOutline, repeatOutline } from 'ionicons/icons';
import { xo } from '../../observable';
import { ViewProps } from '../../props';
import { ActivityViewModel } from './ActivityViewModel';
import { DataSource } from '../../enums';

/** Activity Add Button component props */
export interface ActivityAddButtonProps extends ViewProps<ActivityViewModel> {}

/** Activity Add Button component */
@xo.observer
export class ActivityAddButton extends React.Component<ActivityAddButtonProps> {
  render() {
    const viewModel = this.props.viewModel;
    return (
      <IonFab
        vertical="bottom"
        horizontal="center"
        slot="fixed"
        activated={viewModel.addButtonActivated}
      >
        <IonFabButton disabled={viewModel.loading}>
          <IonIcon icon={add}></IonIcon>
        </IonFabButton>
        <IonFabList side="top">
          <IonFabButton
            color="primary"
            className="add-by-sync"
            onClick={() => viewModel.syncActivity(true)}
          >
            <IonIcon icon={repeatOutline}></IonIcon>
          </IonFabButton>
        </IonFabList>
        <IonFabList side="start">
          <IonFabButton
            color="primary"
            className="add-by-input"
            onClick={() => viewModel.addData(DataSource.Input)}
          >
            <IonIcon icon={searchOutline}></IonIcon>
          </IonFabButton>
        </IonFabList>
        <IonFabList side="end">
          <IonFabButton
            color="primary"
            className="add-by-voice"
            onClick={() => viewModel.addData(DataSource.Voice)}
          >
            <IonIcon icon={micOutline}></IonIcon>
          </IonFabButton>
        </IonFabList>
      </IonFab>
    );
  }
}
