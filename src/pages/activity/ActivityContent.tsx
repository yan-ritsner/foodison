import * as React from 'react';
import {
  IonList,
  IonItem,
  IonGrid,
  IonRow,
  IonCol,
  IonListHeader,
  IonInfiniteScroll,
  IonInfiniteScrollContent,
  IonCard,
  IonIcon,
} from '@ionic/react';
import { format } from 'date-fns';
import { ViewProps } from '../../props';
import { xo } from '../../observable';
import { ActivityViewModel } from './ActivityViewModel';
import { informationCircleOutline } from 'ionicons/icons';

/** Activity Content component props */
export interface ActivityContentProps extends ViewProps<ActivityViewModel> {}

/** Activity Content component props */
@xo.observer
export class ActivityContent extends React.Component<ActivityContentProps> {
  render() {
    const viewModel = this.props.viewModel;
    const rows: JSX.Element[] = [];

    viewModel.dailyData.forEach((dailyData) => {
      /** Add data header */
      rows.push(
        <IonListHeader key={`header-${dailyData.timestamp.valueOf()}`}>
          <IonGrid>
            <IonRow style={{ borderBottom: '1px dotted #999' }}>
              <IonCol>{format(dailyData.timestamp, 'dd/MM/yyyy')}</IonCol>
              <IonCol className="ion-text-end">{`${dailyData.calories} Cal.`}</IonCol>
            </IonRow>
          </IonGrid>
        </IonListHeader>
      );
      /** Add data */
      dailyData.data.forEach((data) => {
        rows.push(
          <IonItem
            key={`data-${data.id}`}
            onClick={() => viewModel.selectData(data)}
          >
            <IonGrid>
              <IonRow>
                <IonCol size="6">{data.name}</IonCol>
                <IonCol size="3" className="ion-text-end">
                  {`${data.duration} Min.`}
                </IonCol>
                <IonCol size="3" className="ion-text-end">
                  {`${data.calories} Cal.`}
                </IonCol>
              </IonRow>
            </IonGrid>
          </IonItem>
        );
      });
    });

    if (rows.length === 0) {
      rows.push(
        <IonItem lines="none" key="info">
          <IonIcon
            color={'primary'}
            icon={informationCircleOutline}
            style={{ marginRight: 5 }}
          ></IonIcon>
          Add activity and get calories burned
        </IonItem>
      );
    }

    return (
      <IonCard>
        <IonList lines="none">{rows}</IonList>
        <IonInfiniteScroll onIonInfinite={(e) => viewModel.loadMoreData(e)}>
          <IonInfiniteScrollContent
            loadingSpinner="circular"
            loadingText="Loading more data..."
          ></IonInfiniteScrollContent>
        </IonInfiniteScroll>
      </IonCard>
    );
  }
}
