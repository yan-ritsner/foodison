import * as React from 'react';
import { IonContent, IonLoading } from '@ionic/react';
import { xo } from '../../observable';
import { ViewProps } from '../../props';
import { PageAnthropometry } from '../page';
import { ActivityViewModel } from './ActivityViewModel';
import { ActivityContent } from './ActivityContent';
import { ActivityModal } from './ActivityModal';
import { ActivityAddButton } from './ActivityAddButton';
import { ActivityActions } from './ActivityActions';
import { ActivityAlert } from './ActivityAlert';

import './Activity.css';

/** Activity View component props */
export interface ActivityViewProps extends ViewProps<ActivityViewModel> {}

/** Activity View component props */
@xo.observer
export class ActivityView extends React.Component<ActivityViewProps> {
  render() {
    const viewModel = this.props.viewModel;

    return (
      <IonContent class="app-background">
        <PageAnthropometry></PageAnthropometry>
        <ActivityContent viewModel={viewModel}></ActivityContent>
        <ActivityModal viewModel={viewModel}></ActivityModal>
        <ActivityAddButton viewModel={viewModel}></ActivityAddButton>
        <ActivityActions viewModel={viewModel} />
        <IonLoading
          isOpen={viewModel.loading}
          message={viewModel.loadingText}
        />
        <ActivityAlert viewModel={viewModel}></ActivityAlert>
      </IonContent>
    );
  }
}
