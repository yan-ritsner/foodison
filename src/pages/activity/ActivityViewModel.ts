import Driver from 'driver.js';
import { isPlatform } from '@ionic/react';
import { xo } from '../../observable';
import { System } from '../../system';
import { PageViewModel } from '../page';
import {
  IActivityDataService,
  IDataInputService,
  IFoodNutritionService,
} from '../../services';
import { ServiceType, DataSource, UnitsType } from '../../enums';
import {
  ActivityData,
  ActivityDailyData,
  ActivityStatsData,
  ExerciseData,
  activities,
} from '../../data';

/**
 * Activity view model
 */
export class ActivityViewModel extends PageViewModel {
  /** Selected data */
  @xo.observable
  public selectedData: ActivityData | null;
  /** Selected statistics data */
  @xo.observable
  public selectedStats: ActivityStatsData | null;
  /** Selected exercise data */
  @xo.observable
  public selectedExercise: ExerciseData | null;

  /** Show actions */
  @xo.observable
  public showActions: boolean = false;
  /** Show actions */
  @xo.observable
  public showModal: boolean = false;

  /** Activity time */
  @xo.observable
  public timestamp: Date;
  /** Activity category */
  @xo.observable
  public category: string;
  /** Activity name */
  @xo.observable
  public activity: string;
  /** Activity duration */
  @xo.observable
  public duration: number;

  /** Alert is visible  */
  @xo.observable
  public alert: boolean;
  /** Alert text */
  @xo.observable
  public alertText: string;
  /** Alert cancellation available */
  public alertCancel: boolean;
  /** Alert ok text */
  public alertOkText: string;
  /** Alert cancel text */
  public alertCancelText: string;
  /** Alert ok callback */
  public onAlertOk: () => void;
  /** Alert cancel callback */
  public onAlertCancel: () => void;

  /** Loading indicator */
  @xo.observable
  public loading: boolean = false;
  /** Loading indicator text */
  @xo.observable
  public loadingText: string | null = null;

  /** Add button activated */
  @xo.observable
  public addButtonActivated: boolean;

  /** Show info */
  @xo.observable
  public showInfo: boolean = false;

  /** Info driver */
  private infoDriver: Driver;

  /** Activity categories*/
  public get categories(): string[] {
    return activities.categories.map((c) => c.category);
  }

  /** Returns true if platform is ios */
  public get isIos() {
    return isPlatform('ios');
  }

  /** Returns true if platform is android */
  public get isAndroid() {
    return isPlatform('android');
  }

  /** Health repo name */
  public get repoName() {
    if (this.isIos) {
      return 'Apple HealthKit';
    } else if (this.isAndroid) {
      return 'Google Fit';
    }
    return 'Fitness and Health Repository';
  }

  /** Activity categories*/
  @xo.computed
  public get activities(): string[] {
    if (this.category == null) return [];
    const category = activities.categories.find(
      (c) => c.category === this.category
    );
    if (category == null) return [];
    return category.activities.map((a) => a.name);
  }

  /** Activity durations */
  public get durations(): number[] {
    return [5, 10, 15, 30, 60, 120, 180];
  }

  /** Activity durations */
  @xo.computed
  public get durationsExtra(): number[] {
    if (!this.selectedStats && !this.selectedExercise) {
      return this.durations;
    }

    const extra = this.selectedStats
      ? this.selectedStats.avgDuration
      : this.selectedExercise.duration_min;

    let durations = [...this.durations];
    if (!durations.find((d) => d === extra)) {
      durations.push(extra);
    }
    durations = durations.sort((a1, a2) => a1 - a2);
    return durations;
  }

  /** Selected activity calories based on a anthropometry settings */
  @xo.computed
  public get calories(): number {
    return this.getCalories();
  }

  /** Show recent activities section */
  @xo.computed
  public get showRecentActivity(): boolean {
    return (
      this.statsData.length > 0 && !this.category && !this.selectedExercise
    );
  }

  /** Show new activity section */
  @xo.computed
  public get showNewActivity(): boolean {
    return !this.selectedStats && !this.selectedExercise;
  }

  /** Show exercise activities section */
  @xo.computed
  public get showExerciseActivity(): boolean {
    return this.selectedExercise != null;
  }

  /** Activity data service */
  public get activityDataService(): IActivityDataService {
    return this.getService<IActivityDataService>(ServiceType.ActivityData);
  }

  /** Data input service */
  public get dataInputService(): IDataInputService {
    return this.getService<IDataInputService>(ServiceType.DataInput);
  }

  /** Food nutrition service */
  public get nutritionService(): IFoodNutritionService {
    return this.getService<IFoodNutritionService>(ServiceType.FoodNutrition);
  }

  /** Stored activity data array */
  public get data(): ActivityData[] {
    return this.activityDataService.data.data;
  }

  /** Stored activity data splitted into days */
  public get dailyData(): ActivityDailyData[] {
    return this.activityDataService.dailyData;
  }

  /** Stored activity statistics data */
  public get statsData(): ActivityStatsData[] {
    return this.activityDataService.statsData;
  }

  /**
   * @constructor
   */
  constructor() {
    super();
    this.addButtonActivated = false;
    this.setLoading(true, 'Loading...');
    setTimeout(() => this.init(), 100);
  }

  /** Init component */
  public async init() {
    this.initInfoDriver();
    await this.activityDataService.getInitial();
    await this.syncActivity();
    this.setLoading(false);
  }

  /** Inits info driver */
  private initInfoDriver() {
    this.infoDriver = new Driver({
      opacity: 0,
      onReset: () => this.hideInfo(),
    });
    this.infoDriver.defineSteps([
      {
        element: '.add-by-sync',
        popover: {
          className: 'info-top-popover',
          title: `Sync activity from ${this.repoName}`,
          description:
            `We will sync activity data from your phone or smartwatch <br>` +
            `using your ${this.repoName} account.`,
          position: 'top',
        },
      },
      {
        element: '.add-by-input',
        popover: {
          title: 'Add activity manually',
          description:
            'Choose manually category, type and duration of you activity, <br>' +
            'or select from the list of your recent activities.',
          position: 'top',
        },
      },
      {
        element: '.add-by-voice',
        popover: {
          className: 'info-right-popover',
          title: 'Add activity by voice',
          description:
            'Tell us the name and duration of your activity and we will show you matching results.<br>' +
            'For example say: Ran 3 miles or 30 min yoga',
          position: 'top',
        },
      },
    ]);
  }

  /** Start info driver*/
  private startInfoDriver() {
    this.infoDriver.start();
  }

  /** Synchronizes activity from health repo */
  public async syncActivity(forceSync: boolean = false) {
    if (System.user.syncActivity || forceSync) {
      if (forceSync) this.setLoading(true, 'Synchronizing');
      await this.activityDataService.sync();
      if (forceSync) this.setLoading(false);
    }
    this.requestAutoSync();
  }

  /** Request auto sync */
  private requestAutoSync() {
    if (!System.user.syncActivity && !System.user.syncRequested) {
      const repo = this.repoName;
      const text =
        `Would you like to automatically get your activity data from ${repo}.<br><br>` +
        'You can enable/disable this feature in the main menu using "Sync Activity" toggle.';
      this.setAlert(
        true,
        text,
        true,
        () => {
          System.user.toggleSyncActivity();
        },
        () => {},
        'Enable',
        'Maybe later'
      );
      System.user.setSyncRequested();
    }
  }

  /** Loads more data */
  public async loadMoreData(e: any) {
    if (!this.activityDataService.hasMore()) {
      e.target.complete();
      e.target.disabled = true;
    } else {
      await this.activityDataService.getMore();
      e.target.complete();
    }
  }

  /** Adds new activity data */
  public addData(dataSource: DataSource) {
    this.timestamp = new Date();
    switch (dataSource) {
      case DataSource.Input:
        this.addDataByInput();
        break;
      case DataSource.Voice:
        this.addDataByVoice();
    }
    System.googleAnalytics.event('Activity', `AddBy${DataSource[dataSource]}`);
  }

  /** Add new data by input */
  public addDataByInput() {
    this.viewModal();
  }

  /** Add new data by voice */
  public async addDataByVoice() {
    const anthropometry = System.user.anthropometry;
    if (anthropometry == null) return;

    await this.dataInputService.recognizeSpeech(
      async (matches) => {
        this.setLoading(true, 'Processing voice...');
        const query = matches[0];

        // get exercise data
        const exerciseInfo = await this.nutritionService.getExerciseDataByNlp(
          query,
          anthropometry.gender,
          anthropometry.getWeight(UnitsType.Metric),
          anthropometry.getHeight(UnitsType.Metric),
          anthropometry.getAge()
        );

        const exercise = exerciseInfo?.exercises[0];

        if (exercise) {
          this.selectExercise(exercise);
          this.viewModal();
        }

        // hide loading indicator
        this.setLoading(false);
      },
      (error) => {
        console.log(error);
        // hide loading indicator
        this.setLoading(false);
      }
    );
  }

  /** Deletes selected activity data */
  public async deleteData() {
    if (this.selectedData == null) return;
    await this.activityDataService.delete(this.selectedData);
    this.unselectData();
  }

  /** Get activity calories*/
  private getCalories() {
    if (this.selectedStats) {
      return Math.round(
        (this.selectedStats.avgCalories * this.duration) /
          this.selectedStats.avgDuration
      );
    } else if (this.selectedExercise) {
      return Math.round(
        (this.selectedExercise.nf_calories * this.duration) /
          this.selectedExercise.duration_min
      );
    } else {
      const calories = this.getActivityCalories();
      return Math.round((calories * this.duration) / activities.duration);
    }
  }

  /** Gets selected activity calories based on anthropometry */
  private getActivityCalories() {
    if (System.user.anthropometry == null) return 0;
    const activity = this.getActivity();
    if (!activity) return 0;
    const weight = System.user.anthropometry.getWeight(activities.weightUnits);
    const closestWeight = activities.weightCategories.reduce((prev, curr) =>
      Math.abs(curr - weight) < Math.abs(prev - weight) ? curr : prev
    );
    const weightIndex = activities.weightCategories.indexOf(closestWeight);
    const calories = activity.calories[weightIndex];
    return calories;
  }

  /** Gets selected activity */
  private getActivity() {
    const category = activities.categories.find((c) => {
      return c.category === this.category;
    });
    const activity = category?.activities.find((a) => {
      return a.name === this.activity;
    });
    return activity;
  }

  /** Saves activity */
  public async saveActivity() {
    const errors = this.validateActivity();

    if (errors.length > 0) {
      this.setAlert(true, errors.join('<br>'));
      return;
    }

    this.setLoading(true, 'Adding activity...');

    const data = new ActivityData();
    data.name = this.activity;
    data.timestamp = this.timestamp;
    data.duration = this.duration;
    data.calories = this.getCalories();

    await this.activityDataService.add(data);

    this.setLoading(false);

    this.hideModal();
  }

  /** Validates activity */
  public validateActivity() {
    const errors: string[] = [];
    if (
      this.selectedStats == null &&
      this.selectedExercise == null &&
      this.activity == null
    ) {
      errors.push('No activity selected');
    } else if (this.selectedStats == null && this.selectedExercise == null) {
      if (this.category == null) {
        errors.push('Category is not specified');
      }
      if (this.activity == null) {
        errors.push('Activity is not specified');
      }
    }
    if (this.duration == null) {
      errors.push('Duration is not specified');
    }
    return errors;
  }

  /** Clear activity fields */
  public cleanActivity() {
    this.category = null;
    this.activity = null;
    this.timestamp = null;
    this.duration = null;
    this.selectedStats = null;
    this.selectedExercise = null;
  }

  /** Selects data */
  @xo.action
  public selectData(data: ActivityData, viewActions: boolean = true): void {
    this.selectedData = data;
    if (viewActions) this.viewActions();
  }

  /** Unselects data */
  @xo.action
  public unselectData(): void {
    this.selectedData = null;
    this.hideActions();
  }

  /** Selects statistics data */
  @xo.action
  public selectStats(data: ActivityStatsData): void {
    this.selectedStats = data;
    this.activity = data.activity;
    this.duration = data.avgDuration;
  }

  /** Unselects statistics data */
  @xo.action
  public unselectStats(): void {
    this.selectedStats = null;
  }

  /** Selects statistics data */
  @xo.action
  public selectExercise(data: ExerciseData): void {
    this.selectedExercise = data;
    this.activity = data.name.charAt(0).toUpperCase() + data.name.slice(1);
    this.duration = data.duration_min;
  }

  /** Unselects statistics data */
  @xo.action
  public unselectExercise(): void {
    this.selectedExercise = null;
  }

  /** View data actions */
  @xo.action
  public viewActions() {
    this.showActions = true;
  }

  /** Hide data actions */
  @xo.action
  public hideActions() {
    this.showActions = false;
  }

  /** View modal */
  @xo.action
  public viewModal() {
    this.showModal = true;
  }

  /** Hide modal */
  @xo.action
  public hideModal() {
    this.showModal = false;
    this.cleanActivity();
  }

  /** Sets timestamp */
  @xo.action
  public setTimestamp(timestamp: Date) {
    this.timestamp = timestamp;
  }

  /** Sets category */
  @xo.action
  public setCategory(category: string) {
    this.category = category;
  }

  /** Sets activity */
  @xo.action
  public setActivity(activity: string) {
    this.activity = activity;
  }

  /** Sets duration */
  @xo.action
  public setDuration(duration: number) {
    this.duration = duration;
  }

  /** Shows/hides alert */
  @xo.action
  public setAlert(
    show: boolean,
    text: string = '',
    cancel: boolean = false,
    onOk?: () => void,
    onCancel?: () => void,
    okText: string = 'Ok',
    cancelText: string = 'Cancel'
  ) {
    this.alert = show;
    this.alertText = text;
    this.alertCancel = cancel;
    if (onOk) this.onAlertOk = onOk;
    if (onCancel) this.onAlertCancel = onCancel;
    this.alertOkText = okText;
    this.alertCancelText = cancelText;
  }

  /** Sets loading indicator and text */
  @xo.action
  public setLoading(loading: boolean, loadingText: string | null = null) {
    this.loading = loading;
    this.loadingText = loadingText;
  }

  /** Shows info */
  @xo.action
  public viewInfo() {
    this.showInfo = true;
    this.addButtonActivated = true;
    setTimeout(() => {
      this.startInfoDriver();
    }, 200);
  }

  /** Hides info */
  @xo.action
  public hideInfo() {
    this.showInfo = false;
    this.addButtonActivated = false;
  }
}
