import React from 'react';
import { IonActionSheet } from '@ionic/react';
import { trash, close } from 'ionicons/icons';
import { xo } from '../../observable';
import { ViewProps } from '../../props';
import { ActivityViewModel } from './ActivityViewModel';

/** Activity actions component props */
export interface ActivityActionsProps extends ViewProps<ActivityViewModel> {}

/** Activity actions component */
@xo.observer
export class ActivityActions extends React.Component<ActivityActionsProps> {
  render() {
    const viewModel = this.props.viewModel;
    const isOpen = viewModel.showActions;
    return (
      <IonActionSheet
        isOpen={isOpen}
        onDidDismiss={() => viewModel.hideActions()}
        buttons={[
          {
            text: 'Delete',
            icon: trash,
            handler: () => {
              viewModel.deleteData();
            },
          },
          {
            text: 'Cancel',
            icon: close,
            handler: () => {
              viewModel.unselectData();
            },
          },
        ]}
      />
    );
  }
}
