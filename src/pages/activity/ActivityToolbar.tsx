import React from 'react';
import { IonButtons, IonButton, IonIcon } from '@ionic/react';
import { informationCircleOutline } from 'ionicons/icons';
import { xo } from '../../observable';
import { ViewProps } from '../../props';
import { ActivityViewModel } from './ActivityViewModel';

/** Activity toolbar component props */
export interface ActivityToolbarProps extends ViewProps<ActivityViewModel> {}

/** Activity toolbar component props */
@xo.observer
export class ActivityToolbar extends React.Component<ActivityToolbarProps> {
  render() {
    const viewModel = this.props.viewModel;

    return (
      <IonButtons slot="end">
        <IonButton onClick={() => viewModel.viewInfo()} className="page-info">
          <IonIcon
            slot="icon-only"
            icon={informationCircleOutline}
            color="primary"
          />
        </IonButton>
      </IonButtons>
    );
  }
}
