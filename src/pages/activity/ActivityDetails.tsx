import React from 'react';
import {
  IonList,
  IonItem,
  IonLabel,
  IonDatetime,
  IonSelect,
  IonSelectOption,
  IonText,
  IonItemDivider,
  IonCard,
} from '@ionic/react';
import { xo } from '../../observable';
import { ViewProps } from '../../props';
import { ActivityViewModel } from './ActivityViewModel';

/** Activity Details component props */
export interface ActivityDetailsProps extends ViewProps<ActivityViewModel> {}

/** Activity Details component */
@xo.observer
export class ActivityDetails extends React.Component<ActivityDetailsProps> {
  render() {
    const viewModel = this.props.viewModel;
    return (
      <IonCard>
        <IonList lines="none">
          {viewModel.showRecentActivity && (
            <>
              <IonItemDivider>
                <IonLabel>1. Repeat Recent Activity</IonLabel>
              </IonItemDivider>
              <IonItem>
                <IonLabel position="floating">Activity</IonLabel>
                <IonSelect
                  value={viewModel.selectedStats}
                  okText="Ok"
                  cancelText="Cancel"
                  interface="action-sheet"
                  onIonChange={(e) => viewModel.selectStats(e.detail.value)}
                >
                  {viewModel.statsData.map((stats) => (
                    <IonSelectOption key={stats.activity} value={stats}>
                      {stats.activity}
                    </IonSelectOption>
                  ))}
                </IonSelect>
              </IonItem>
            </>
          )}
          {viewModel.showNewActivity && (
            <>
              <IonItemDivider>
                <IonLabel>1. Select New Activity</IonLabel>
              </IonItemDivider>
              <IonItem>
                <IonLabel position="floating">Category</IonLabel>
                <IonSelect
                  value={viewModel.category}
                  okText="Ok"
                  cancelText="Cancel"
                  interface="action-sheet"
                  onIonChange={(e) => viewModel.setCategory(e.detail.value)}
                >
                  {viewModel.categories.map((category) => (
                    <IonSelectOption key={category} value={category}>
                      {category}
                    </IonSelectOption>
                  ))}
                </IonSelect>
              </IonItem>
              <IonItem>
                <IonLabel position="floating">Activity</IonLabel>
                <IonSelect
                  value={viewModel.activity}
                  okText="Ok"
                  cancelText="Cancel"
                  interface="action-sheet"
                  onIonChange={(e) => viewModel.setActivity(e.detail.value)}
                >
                  {viewModel.activities.map((activity) => (
                    <IonSelectOption key={activity} value={activity}>
                      {activity}
                    </IonSelectOption>
                  ))}
                </IonSelect>
              </IonItem>
            </>
          )}
          {viewModel.showExerciseActivity && (
            <>
              <IonItemDivider>
                <IonLabel>1. Exercise Activity</IonLabel>
              </IonItemDivider>
              <IonItem>
                <IonText>{viewModel.activity}</IonText>
              </IonItem>
            </>
          )}
          <IonItemDivider>
            <IonLabel>2. Time and Duration</IonLabel>
          </IonItemDivider>
          <IonItem>
            <IonLabel position="floating">Time</IonLabel>
            <IonDatetime
              displayFormat="MMM DD, YYYY HH:mm"
              placeholder="Select Time"
              value={viewModel.timestamp?.toString()}
              onIonChange={(e) => {
                if (e.detail.value) {
                  viewModel.setTimestamp(new Date(e.detail.value));
                }
              }}
            ></IonDatetime>
          </IonItem>
          <IonItem>
            <IonLabel position="floating">Duration (min.)</IonLabel>
            <IonSelect
              value={viewModel.duration}
              okText="Ok"
              cancelText="Cancel"
              interface="action-sheet"
              onIonChange={(e) => viewModel.setDuration(Number(e.detail.value))}
            >
              {viewModel.durationsExtra.map((duration) => (
                <IonSelectOption key={duration} value={duration}>
                  {duration}
                </IonSelectOption>
              ))}
            </IonSelect>
          </IonItem>
          <IonItemDivider>
            <IonLabel>3. Calories</IonLabel>
          </IonItemDivider>
          <IonItem>
            <IonText>
              {viewModel.calories
                ? `Calories burned: ${viewModel.calories}`
                : ''}
            </IonText>
          </IonItem>
        </IonList>
      </IonCard>
    );
  }
}
