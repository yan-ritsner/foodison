import {
  startOfDay,
  startOfMonth,
  isToday,
  addDays,
  endOfMonth,
} from 'date-fns';
import { xo } from '../../observable';
import { System } from '../../system';
import { PageViewModel } from '../page';
import { ServiceType } from '../../enums';
import {
  IFoodDataService,
  IActivityDataService,
} from '../../services/interfaces';

/**
 * Dashboard view model
 */
export class DashboardViewModel extends PageViewModel {
  /** Loading indicator */
  @xo.observable
  public loading: boolean = false;
  /** Loading indicator text */
  @xo.observable
  public loadingText: string | null = null;

  /** Start date */
  @xo.observable
  public startDate: Date;
  /** Selected date */
  @xo.observable
  public selectedDate: Date;

  /** Food data service */
  public get foodDataService(): IFoodDataService {
    return this.getService<IFoodDataService>(ServiceType.FoodData);
  }
  /** Activity data service */
  public get activityDataService(): IActivityDataService {
    return this.getService<IActivityDataService>(ServiceType.ActivityData);
  }

  /**
   * @constructor
   */
  public constructor() {
    super();
    this.setLoading(true, 'Loading...');
    setTimeout(() => this.init(), 500);
  }

  /** Init component */
  public async init() {
    await this.getInitialData();
    await this.getMoreData();
    this.setLoading(false);
  }

  /** Get more data */
  private async getInitialData() {
    await this.foodDataService.getInitial();
    await this.activityDataService.getInitial();
  }

  /** Get more data */
  private async getMoreData() {
    await this.getMoreConsumedData();
    await this.getMoreBurnedData();
  }

  /** Get more consumed data */
  private async getMoreConsumedData() {
    const startDate = this.startDate ?? startOfMonth(new Date());
    const service = this.foodDataService;
    while (service.hasMore() && service.startDate() >= startDate) {
      await service.getMore();
    }
  }

  /** Get more burned data */
  private async getMoreBurnedData() {
    const startDate = this.startDate ?? startOfMonth(new Date());
    const service = this.activityDataService;
    while (service.hasMore() && service.startDate() >= startDate) {
      await service.getMore();
    }
    this.setLoading(false);
  }

  /** Selected date is today */
  @xo.computed
  public get isToday(): boolean {
    if (this.selectedDate == null) return true;
    return isToday(this.selectedDate);
  }

  /** Maximum number of calories consumed or burned */
  @xo.computed
  public get maxCalories(): number {
    const bmr = System.user.anthropometry ? System.user.anthropometry.bmr : 0;
    const consumed = this.foodDataService.dailyData;
    const maxConsumed = Math.max(...consumed.map((d) => d.calories));
    const burned = this.activityDataService.dailyData;
    const maxBurned = Math.max(...burned.map((d) => d.calories + bmr));
    return Math.max(maxConsumed, maxBurned);
  }

  /** Amount of calories consumed in selected day */
  @xo.computed
  public get consumedCalories(): number {
    const date = this.selectedDate ?? startOfDay(new Date());
    const dailyDataMap = this.foodDataService.dailyDataMap;
    const data = dailyDataMap.get(date.valueOf());
    if (data == null) return 0;
    return data.calories;
  }

  /** Amount of calories burned in selected day */
  @xo.computed
  public get burnedCalories() {
    const date = this.selectedDate ?? startOfDay(new Date());
    const dailyDataMap = this.activityDataService.dailyDataMap;
    const data = dailyDataMap.get(date.valueOf());
    if (data == null) return 0;
    const bmr = System.user.anthropometry ? System.user.anthropometry.bmr : 0;
    return bmr + data.calories;
  }

  /** Percent of calories consumed */
  @xo.computed
  public get consumedPercent() {
    if (this.maxCalories === 0) return 0;
    return this.consumedCalories / this.maxCalories;
  }

  /** Percent of calories burned */
  @xo.computed
  public get burnedPercent() {
    if (this.maxCalories === 0) return 0;
    return this.burnedCalories / this.maxCalories;
  }

  /** Chart data */
  @xo.computed
  public get chartData(): any[][] {
    const bmr = System.user.anthropometry ? System.user.anthropometry.bmr : 0;

    const startDate = this.startDate ?? startOfMonth(new Date());
    const maxDate = endOfMonth(startDate);
    let currDate = startDate;

    const consumedData = this.foodDataService.dailyDataMap;
    const burnedData = this.activityDataService.dailyDataMap;
    const data: any[][] = [];

    while (currDate < maxDate) {
      const consumed = consumedData.get(currDate.valueOf());
      const burned = burnedData.get(currDate.valueOf());

      if (burned || consumed) {
        const burnedCalories = (burned ? burned.calories : 0) + bmr;
        const consumedCalories = consumed ? consumed.calories : 0;
        data.push([currDate, burnedCalories, consumedCalories]);
      }

      currDate = addDays(currDate, 1);
    }
    return data;
  }

  /** Calendar date change */
  @xo.action
  public onDateChange(date: Date | Date[]) {
    if (date instanceof Date) this.selectedDate = date;
  }

  /** Calendar start date change */
  @xo.action
  public onStartDateChange(date: Date) {
    this.startDate = date;
    this.setLoading(true, 'Loading...');
    setTimeout(() => this.getMoreData(), 100);
  }

  /** Returns calendar day class name depending of consumed/burned rate */
  public getDateClass(date: Date, view: string) {
    if (view !== 'month') return '';

    const bmr = System.user.anthropometry ? System.user.anthropometry.bmr : 0;

    const consumedData = this.foodDataService.dailyDataMap;
    const burnedData = this.activityDataService.dailyDataMap;

    const consumed = consumedData.get(date.valueOf());
    const burned = burnedData.get(date.valueOf());

    if (!consumed || !burned) return '';

    const burnedCalories = (burned ? burned.calories : 0) + bmr;
    const consumedCalories = consumed ? consumed.calories : 0;
    const diffCalories = burnedCalories - consumedCalories;

    if (diffCalories > 300) {
      return 'rate-green-1';
    } else if (diffCalories > 150) {
      return 'rate-green-2';
    } else if (diffCalories > 0) {
      return 'rate-green-3';
    } else if (diffCalories < -300) {
      return 'rate-red-3';
    } else if (diffCalories < -150) {
      return 'rate-red-2';
    } else {
      return 'rate-red-1';
    }
  }

  /** Sets loading indicator and text */
  @xo.action
  public setLoading(loading: boolean, loadingText: string | null = null) {
    this.loading = loading;
    this.loadingText = loadingText;
  }
}
