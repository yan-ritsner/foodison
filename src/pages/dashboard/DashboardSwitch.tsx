import * as React from 'react';
import { IonSegment, IonSegmentButton, IonLabel } from '@ionic/react';
import { xo } from '../../observable';
import { ViewProps } from '../../props';
import { DashboardViewModel } from './DashboardViewModel';

/** Dashboard View component props */
export interface DashboardSwitchProps extends ViewProps<DashboardViewModel> {}

/** Dashboard Switch component */
@xo.observer
export class DashboardSwitch extends React.Component<DashboardSwitchProps> {
  render() {
    return (
      <IonSegment
        value={'Month'}
        onIonChange={(e) => console.log('Segment selected', e.detail.value)}
      >
        <IonSegmentButton value="Day">
          <IonLabel>Day</IonLabel>
        </IonSegmentButton>
        <IonSegmentButton value="Month">
          <IonLabel>Month</IonLabel>
        </IonSegmentButton>
      </IonSegment>
    );
  }
}
