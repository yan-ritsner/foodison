import * as React from 'react';
import { format } from 'date-fns';

import {
  IonLabel,
  IonProgressBar,
  IonGrid,
  IonRow,
  IonCol,
} from '@ionic/react';
import { xo } from '../../observable';
import { ViewProps } from '../../props';
import { DashboardViewModel } from './DashboardViewModel';

/** Dashboard Day overview component props */
export interface DashboardDayProps extends ViewProps<DashboardViewModel> {}

/** Dashboard Day overview component */
@xo.observer
export class DashboardDay extends React.Component<DashboardDayProps> {
  render() {
    const viewModel = this.props.viewModel;
    const date = viewModel.isToday
      ? 'Today'
      : `On ${format(viewModel.selectedDate, 'dd/MM/yyyy')}`;
    return (
      <IonGrid>
        <IonRow>
          <IonCol>
            <IonLabel>{`Burned ${date}`}</IonLabel>
          </IonCol>
          <IonCol size={'3'} className="ion-text-end">
            <IonLabel>{`${viewModel.burnedCalories} Cal.`}</IonLabel>
          </IonCol>
        </IonRow>
        <IonRow>
          <IonCol>
            <IonProgressBar
              color="primary"
              value={viewModel.burnedPercent}
              style={{ height: '10px', borderRadius: '2px' }}
            ></IonProgressBar>
          </IonCol>
        </IonRow>
        <IonRow>
          <IonCol>
            <IonLabel>{`Consumed ${date}`}</IonLabel>
          </IonCol>
          <IonCol size={'3'} className="ion-text-end">
            <IonLabel>{`${viewModel.consumedCalories} Cal.`}</IonLabel>
          </IonCol>
        </IonRow>
        <IonRow>
          <IonCol>
            <IonProgressBar
              color="danger"
              value={viewModel.consumedPercent}
              style={{ height: '10px', borderRadius: '2px' }}
            ></IonProgressBar>
          </IonCol>
        </IonRow>
      </IonGrid>
    );
  }
}
