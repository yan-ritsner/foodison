import * as React from 'react';
import { Chart } from 'react-google-charts';
import { System } from '../../system';
import { xo } from '../../observable';
import { ViewProps } from '../../props';
import { DashboardViewModel } from './DashboardViewModel';

/** Dashboard Chart component props */
export interface DashboardChartProps extends ViewProps<DashboardViewModel> {}

/** Dashboard Chart component */
@xo.observer
export class DashboardChart extends React.Component<DashboardChartProps> {
  /**
   * @constructor
   */
  constructor(props: DashboardChartProps) {
    super(props);
    this.onWindowResize = this.onWindowResize.bind(this);
  }

  render() {
    const viewModel = this.props.viewModel;
    let chartTextStyle = {};
    if (System.theme.darkTheme) {
      chartTextStyle = { color: '#F0F0F0' };
    }
    const chartData = viewModel.chartData;
    if (chartData.length === 0) return '';

    return (
      <Chart
        width={'100%'}
        height={'250px'}
        chartType="LineChart"
        loader={<div>Loading Chart</div>}
        data={[['x', 'burned', 'consumed'], ...chartData]}
        options={{
          backgroundColor: 'none',
          hAxis: {
            title: 'Date',
            gridlines: {
              color: 'none',
            },
            textStyle: chartTextStyle,
            titleTextStyle: chartTextStyle,
            format: 'd MMM',
          },
          vAxis: {
            title: 'Calories',
            gridlines: {
              color: 'none',
            },
            textStyle: chartTextStyle,
            titleTextStyle: chartTextStyle,
          },
          curveType: 'function',
          legend: {
            position: 'top',
            alignment: 'center',
            textStyle: chartTextStyle,
          },
          series: {
            0: { color: '#66bb6a' },
            1: { color: '#eb445a' },
          },
        }}
      />
    );
  }

  componentDidMount() {
    setTimeout(() => {
      this.forceUpdate();
    }, 100);
    window.addEventListener('resize', this.onWindowResize);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.onWindowResize);
  }

  onWindowResize() {
    setTimeout(() => {
      this.forceUpdate();
    }, 100);
  }
}
