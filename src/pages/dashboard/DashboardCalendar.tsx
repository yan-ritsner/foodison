import * as React from 'react';
import Calendar from 'react-calendar';
import { xo } from '../../observable';
import { ViewProps } from '../../props';
import { DashboardViewModel } from './DashboardViewModel';

/** Dashboard View component props */
export interface DashboardCalendarProps extends ViewProps<DashboardViewModel> {}

/** Dashboard View component */
@xo.observer
export class DashboardCalendar extends React.Component<DashboardCalendarProps> {
  render() {
    const viewModel = this.props.viewModel;
    if (viewModel.loading) {
      // binding
    }
    return (
      <Calendar
        onChange={(e) => {
          viewModel.onDateChange(e);
        }}
        onActiveStartDateChange={(e) => {
          viewModel.onStartDateChange(e.activeStartDate);
        }}
        tileClassName={(e) => {
          return viewModel.getDateClass(e.date, e.view);
        }}
      ></Calendar>
    );
  }
}
