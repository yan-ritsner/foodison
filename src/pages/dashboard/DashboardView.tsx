import * as React from 'react';
import { IonCard, IonContent, IonLoading } from '@ionic/react';
import { xo } from '../../observable';
import { ViewProps } from '../../props';
import { PageAnthropometry } from '../page';
import { DashboardViewModel } from './DashboardViewModel';
import { DashboardChart } from './DashboardChart';
import { DashboardDay } from './DashboardDay';
import { DashboardCalendar } from './DashboardCalendar';

import './Dashboard.css';

/** Dashboard View component props */
export interface DashboardViewProps extends ViewProps<DashboardViewModel> {}

/** Dashboard View component */
@xo.observer
export class DashboardView extends React.Component<DashboardViewProps> {
  render() {
    const viewModel = this.props.viewModel;
    return (
      <IonContent class="app-background">
        <PageAnthropometry></PageAnthropometry>

        <IonCard>
          <DashboardCalendar viewModel={viewModel}></DashboardCalendar>
        </IonCard>

        <IonCard>
          <DashboardDay viewModel={viewModel}></DashboardDay>
        </IonCard>

        <IonCard>
          <DashboardChart viewModel={viewModel}></DashboardChart>
        </IonCard>

        <IonLoading
          isOpen={viewModel.loading}
          message={viewModel.loadingText}
        />
      </IonContent>
    );
  }
}
