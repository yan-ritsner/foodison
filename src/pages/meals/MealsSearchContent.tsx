import React from 'react';
import {
  IonItem,
  IonItemDivider,
  IonLabel,
  IonList,
  IonCard,
  IonText,
} from '@ionic/react';
import { xo } from '../../observable';
import { ViewProps } from '../../props';
import { PageAttribution } from '../page';
import { MealsViewModel } from './MealsViewModel';

/** Meals Search Content component props */
export interface MealsSearchContentProps extends ViewProps<MealsViewModel> {}

/** Meals Search Content component */
@xo.observer
export class MealsSearchContent extends React.Component<MealsSearchContentProps> {
  render() {
    const viewModel = this.props.viewModel;
    const groups = viewModel.searchResults
      ? Array.from(viewModel.searchResults.keys())
      : [];
    return (
      <IonCard style={{ marginTop: 60 }}>
        <PageAttribution style={{ float: 'right' }}></PageAttribution>
        {groups.map((g) => (
          <IonList key={g} style={{ width: '100%' }}>
            <IonItemDivider>
              <IonLabel>{g}</IonLabel>
            </IonItemDivider>
            {viewModel.searchResults.get(g).map((result) => (
              <IonItem
                key={result.id}
                button={true}
                onClick={() => viewModel.selectSearchResult(result)}
              >
                <img
                  alt=""
                  height={30}
                  width={30}
                  src={result.image}
                  style={{ marginRight: 10, borderRadius: 15 }}
                ></img>
                <IonText>{result.name}</IonText>
              </IonItem>
            ))}
          </IonList>
        ))}
      </IonCard>
    );
  }
}
