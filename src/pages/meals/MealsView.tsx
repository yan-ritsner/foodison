import React from 'react';
import { IonContent, IonLoading, IonToast } from '@ionic/react';
import { xo } from '../../observable';
import { ViewProps } from '../../props';
import { MealsViewModel } from './MealsViewModel';
import { MealsActions } from './MealsActions';
import { MealsContent } from './MealsContent';
import { MealsDetails } from './MealsDetails';
import { MealsSearch } from './MealsSearch';
import { MealsAddButton } from './MealsAddButton';
import { MealsSelection } from './MealsSelection';

import './Meals.css';

/** Meals View component props */
export interface MealsViewProps extends ViewProps<MealsViewModel> {}

/** Meals View component */
@xo.observer
export class MealsView extends React.Component<MealsViewProps> {
  render() {
    const viewModel = this.props.viewModel;

    return (
      <IonContent class="app-background">
        <MealsContent viewModel={viewModel}></MealsContent>
        <MealsDetails viewModel={viewModel}></MealsDetails>
        <MealsSearch viewModel={viewModel}></MealsSearch>
        <MealsSelection viewModel={viewModel}></MealsSelection>
        <MealsAddButton viewModel={viewModel}></MealsAddButton>
        <MealsActions viewModel={viewModel} />
        <IonLoading
          isOpen={viewModel.loading}
          message={viewModel.loadingText}
        />
        <IonToast
          isOpen={viewModel.showError}
          onDidDismiss={() => viewModel.hideError()}
          message={viewModel.errorText}
          duration={2000}
          position={'middle'}
        />
      </IonContent>
    );
  }
}
