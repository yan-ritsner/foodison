import React from 'react';
import { IonActionSheet } from '@ionic/react';
import { trash, close, eye } from 'ionicons/icons';
import { xo } from '../../observable';
import { ViewProps } from '../../props';
import { MealsViewModel } from './MealsViewModel';

/** Meals actions component props */
export interface MealsActionsProps extends ViewProps<MealsViewModel> {}

/** Meals actions component */
@xo.observer
export class MealsActions extends React.Component<MealsActionsProps> {
  render() {
    const viewModel = this.props.viewModel;
    const isOpen = viewModel.showActions;
    return (
      <IonActionSheet
        isOpen={isOpen}
        onDidDismiss={() => viewModel.hideActions()}
        buttons={[
          {
            text: 'View',
            role: 'apply',
            icon: eye,
            handler: () => {
              viewModel.viewData();
            },
          },
          {
            text: 'Delete',
            icon: trash,
            handler: () => {
              viewModel.deleteData();
            },
          },
          {
            text: 'Cancel',
            icon: close,
            handler: () => {
              viewModel.unselectData();
            },
          },
        ]}
      />
    );
  }
}
