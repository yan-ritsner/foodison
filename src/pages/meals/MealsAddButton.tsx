import React from 'react';
import { IonFab, IonFabButton, IonIcon, IonFabList } from '@ionic/react';
import {
  add,
  cameraOutline,
  barcodeOutline,
  micOutline,
  searchOutline,
} from 'ionicons/icons';
import { xo } from '../../observable';
import { DataSource } from '../../enums';
import { ViewProps } from '../../props';
import { MealsViewModel } from './MealsViewModel';

/** Meals Add Button component props */
export interface MealsAddButtonProps extends ViewProps<MealsViewModel> {}

/** Meals Add Button component */
@xo.observer
export class MealsAddButton extends React.Component<MealsAddButtonProps> {
  render() {
    const viewModel = this.props.viewModel;
    return (
      <IonFab
        vertical="bottom"
        horizontal="center"
        slot="fixed"
        activated={viewModel.addButtonActivated}
      >
        <IonFabButton disabled={viewModel.loading}>
          <IonIcon icon={add}></IonIcon>
        </IonFabButton>
        <IonFabList side="top">
          <IonFabButton
            color="primary"
            className="add-by-photo"
            onClick={() => viewModel.addData(DataSource.Photo)}
          >
            <IonIcon icon={cameraOutline}></IonIcon>
          </IonFabButton>
          <IonFabButton
            color="primary"
            className="add-by-barcode"
            onClick={() => viewModel.addData(DataSource.Barcode)}
          >
            <IonIcon icon={barcodeOutline}></IonIcon>
          </IonFabButton>
        </IonFabList>
        <IonFabList side="start">
          <IonFabButton
            color="primary"
            className="add-by-input"
            onClick={() => viewModel.addData(DataSource.Input)}
          >
            <IonIcon icon={searchOutline}></IonIcon>
          </IonFabButton>
        </IonFabList>
        <IonFabList side="end">
          <IonFabButton
            color="primary"
            className="add-by-voice"
            onClick={() => viewModel.addData(DataSource.Voice)}
          >
            <IonIcon icon={micOutline}></IonIcon>
          </IonFabButton>
        </IonFabList>
      </IonFab>
    );
  }
}
