import React from 'react';
import {
  IonRow,
  IonCol,
  IonGrid,
  IonLabel,
  IonText,
  IonItem,
  IonInput,
  IonList,
  IonButton,
  IonIcon,
  IonRange,
} from '@ionic/react';
import { xo } from '../../observable';
import { ViewProps } from '../../props';
import { FoodData } from '../../data';
import { MealsViewModel } from './MealsViewModel';
import {
  addCircleOutline,
  removeCircleOutline,
  chevronDownCircleOutline,
  chevronForwardCircleOutline,
} from 'ionicons/icons';
import { MealsNutrition } from './MealsNutrition';

/** Meals Properties component props */
export interface MealsPropertiesProps extends ViewProps<MealsViewModel> {
  data: FoodData;
  food?: number;
  border: boolean;
}

/** Meals Properties component */
@xo.observer
export class MealsProperties extends React.Component<MealsPropertiesProps> {
  render() {
    const data = this.props.data;
    const food = this.props.food;
    const border = this.props.border;

    const portion = data.getFoodPortion(food);
    const portionShare = data.getFoodPortionShare(food);
    const weight = data.getFoodPortionWeight(food);
    const calories = data.getFoodPortionCalories(food);

    const name = data.getFoodName(food);
    const servingQty = data.getFoodServingQty(food);
    const servingUnit = data.getFoodServingUnit(food);

    const expanded = data.getFoodExpanded(food);

    const setPortion = (value) => data.setFoodPortion(food, value);
    const setPortionShare = (value) => data.setFoodPortionShare(food, value);
    const incPortion = () => data.incFoodPortion(food);
    const decPortion = () => data.decFoodPortion(food);
    const setExpanded = (expanded) => data.setFoodExpanded(food, expanded);

    return (
      <IonList
        lines={'none'}
        style={{ width: '100%', borderBottom: border ? '1px dotted #999' : '' }}
        class="ion-no-padding"
      >
        <IonItem class="ion-no-padding">
          <IonGrid class="ion-no-padding">
            <IonRow class="ion-no-padding">
              <IonCol class="ion-no-padding">
                <IonText style={{ textTransform: 'capitalize' }}>
                  {name && <span>{name}</span>}
                </IonText>
                <IonButton
                  fill={'clear'}
                  size={'small'}
                  style={{ marginTop: '-5px' }}
                  onClick={() => setExpanded(!expanded)}
                >
                  <IonIcon
                    icon={
                      expanded
                        ? chevronDownCircleOutline
                        : chevronForwardCircleOutline
                    }
                    style={{ fontSize: '20px' }}
                  ></IonIcon>
                </IonButton>
              </IonCol>
              <IonCol className="ion-text-end ion-no-padding" size="4">
                <IonText>{calories} Cal.</IonText>
              </IonCol>
            </IonRow>
          </IonGrid>
        </IonItem>
        {expanded && (
          <IonItem className={'pop-in-animate'}>
            <MealsNutrition {...this.props}></MealsNutrition>
          </IonItem>
        )}
        {data.newData && (
          <>
            <IonItem class="ion-no-padding">
              <IonGrid class="ion-no-padding">
                <IonRow class="ion-no-padding">
                  <IonCol class="ion-no-padding">
                    <IonText>
                      {servingQty && (
                        <span>
                          Serving: {servingQty} {servingUnit}
                        </span>
                      )}
                    </IonText>
                  </IonCol>
                  <IonCol className="ion-text-end ion-no-padding" size="4">
                    <IonText>{weight && <span>{weight} g.</span>}</IonText>
                  </IonCol>
                </IonRow>
              </IonGrid>
            </IonItem>
            <IonItem class="ion-no-padding">
              <IonLabel style={{ marginRight: 5 }}>Portion</IonLabel>
              <IonRange
                min={0.1}
                max={1}
                step={0.1}
                value={portionShare}
                onIonChange={(e) => setPortionShare(e.detail.value as number)}
                snaps={true}
                color="primary"
                class="ion-no-padding"
                style={{ marginRight: 15 }}
              ></IonRange>
              <IonInput
                className="ion-text-center"
                value={portion}
                type={'number'}
                onIonChange={(e) => setPortion(Number(e.detail.value))}
              ></IonInput>
              <IonButton fill="clear" onClick={() => incPortion()}>
                <IonIcon
                  icon={addCircleOutline}
                  style={{ fontSize: '20px' }}
                ></IonIcon>
              </IonButton>
              <IonButton fill="clear" onClick={() => decPortion()}>
                <IonIcon
                  icon={removeCircleOutline}
                  style={{ fontSize: '20px' }}
                ></IonIcon>
              </IonButton>
            </IonItem>
          </>
        )}
      </IonList>
    );
  }
}
