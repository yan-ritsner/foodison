import React from 'react';
import {
  IonCard,
  IonCardContent,
  IonGrid,
  IonRow,
  IonCol,
  IonList,
  IonItem,
  IonCheckbox,
  IonLabel,
  IonToggle,
  IonText,
} from '@ionic/react';
import { xo } from '../../observable';
import { ViewProps } from '../../props';
import { PageAttribution } from '../page';
import { MealsViewModel } from './MealsViewModel';
import { MealsAcceptButton } from './MealsAcceptButton';
import { MealsModal } from './MealsModal';

/** Meals Selection component props */
export interface MealsSelectionProps extends ViewProps<MealsViewModel> {}

/** Meals Selection component */
@xo.observer
export class MealsSelection extends React.Component<MealsSelectionProps> {
  render() {
    const viewModel = this.props.viewModel;
    const data = viewModel.selectedData;
    if (!data) return '';

    const photo = data.photo;
    if (!photo) viewModel.loadPhoto(data);
    const photoUrl = data.getPhotoUrl(false);

    const foods = viewModel.sortByCalories
      ? data.getFoodsByCalories()
      : data.getFoods();
    const totalCalories = data.getSelectedFoodsCalories();

    return (
      <MealsModal
        title={'Meal Selection'}
        open={viewModel.showSelection}
        closeDisabled={viewModel.loading}
        element={viewModel.ref.current}
        onClose={() => viewModel.hideSelection()}
        onDismiss={() => viewModel.hideSelection()}
      >
        <IonCard style={{ marginBottom: 70 }}>
          {photoUrl && (
            <img
              alt={''}
              style={{ height: 300, width: '100%', objectFit: 'cover' }}
              src={photoUrl}
            ></img>
          )}
          <IonCardContent class="ion-no-padding">
            <IonList class="ion-no-padding" style={{ width: '100%' }}>
              <IonItem>
                <IonLabel style={{ fontSize: 14 }}>Sort by Calories</IonLabel>
                <IonToggle
                  checked={viewModel.sortByCalories}
                  onIonChange={(e) =>
                    viewModel.setSortByCalories(e.detail.checked)
                  }
                ></IonToggle>
                <PageAttribution
                  style={{ marginTop: -10, float: 'right' }}
                ></PageAttribution>
              </IonItem>
              <IonItem>
                <IonGrid style={{ paddingLeft: 0 }}>
                  <IonRow style={{ paddingLeft: 0 }}>
                    <IonCol color={'primary'} style={{ paddingLeft: 0 }}>
                      <IonText color="primary">Select below:</IonText>
                    </IonCol>
                    {totalCalories > 0 && (
                      <IonCol
                        color={'primary'}
                        className="ion-text-end pop-in-animate"
                        key={totalCalories}
                      >
                        <IonText color="primary">
                          Total: {totalCalories} Cal.
                        </IonText>
                      </IonCol>
                    )}
                  </IonRow>
                </IonGrid>
              </IonItem>
              {foods.map((food) => (
                <IonItem key={food.food_name}>
                  <IonCheckbox
                    color={'primary'}
                    style={{ margin: 0 }}
                    checked={data.getFoodSelected(food.index)}
                    onIonChange={(e) =>
                      data.setFoodSelected(food.index, e.detail.checked)
                    }
                  ></IonCheckbox>
                  <IonLabel style={{ margin: 0 }}>
                    <IonGrid>
                      <IonRow>
                        <IonCol style={{ textTransform: 'capitalize' }}>
                          {food.food_name}
                        </IonCol>
                        <IonCol className="ion-text-end">
                          {data.getFoodCalories(food.index)} Cal.
                        </IonCol>
                      </IonRow>
                    </IonGrid>
                  </IonLabel>
                </IonItem>
              ))}
            </IonList>
          </IonCardContent>
        </IonCard>
        {data.hasFoodsSelected() && (
          <MealsAcceptButton
            disabled={viewModel.loading}
            onAccept={() => viewModel.acceptSelection()}
          ></MealsAcceptButton>
        )}
      </MealsModal>
    );
  }
}
