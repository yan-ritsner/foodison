import React from 'react';
import { IonSearchbar } from '@ionic/react';
import { xo } from '../../observable';
import { ViewProps } from '../../props';
import { MealsViewModel } from './MealsViewModel';
import { MealsModal } from './MealsModal';
import { MealsSearchContent } from './MealsSearchContent';

/** Meals Search component props */
export interface MealsSearchProps extends ViewProps<MealsViewModel> {}

/** Meals Search component */
@xo.observer
export class MealsSearch extends React.Component<MealsSearchProps> {
  render() {
    const viewModel = this.props.viewModel;

    return (
      <MealsModal
        title={'Meal Search'}
        open={viewModel.showSearch}
        closeDisabled={viewModel.loading}
        element={viewModel.ref.current}
        onClose={() => viewModel.hideSearch()}
        onDismiss={() => viewModel.hideSearch()}
      >
        <div slot="fixed" style={{ width: '100%' }}>
          <IonSearchbar
            debounce={500}
            value={viewModel.searchText}
            onIonChange={(e) => viewModel.setSearchText(e.detail.value!)}
            onKeyPress={(e) => {
              if (e.key === 'Enter') {
                viewModel.doSearch();
                (e.target as any).blur();
              }
            }}
          ></IonSearchbar>
        </div>
        <MealsSearchContent viewModel={viewModel}></MealsSearchContent>
      </MealsModal>
    );
  }
}
