import React from 'react';
import {
  IonModal,
  IonHeader,
  IonToolbar,
  IonTitle,
  IonButtons,
  IonButton,
  IonContent,
  IonIcon,
} from '@ionic/react';
import { closeCircleOutline } from 'ionicons/icons';
import { PageLogo } from '../page';
import { System } from '../../system';

/** Meals Modal component props */
export interface MealsModalProps {
  title: string;
  open: boolean;
  closeDisabled: boolean;
  element: HTMLElement;
  children: JSX.Element[];
  onClose: () => void;
  onDismiss: () => void;
}

/** Meals Modal component */
export class MealsModal extends React.Component<MealsModalProps> {
  render() {
    const title = this.props.open;
    const open = this.props.open;
    const closeDisabled = this.props.closeDisabled;
    const element = this.props.element;
    const children = this.props.children;
    const onClose = this.props.onClose;
    const onDismiss = this.props.onDismiss;

    return (
      <IonModal
        isOpen={open}
        swipeToClose={!closeDisabled}
        presentingElement={element}
        onDidDismiss={() => onDismiss}
      >
        <IonHeader translucent>
          <IonToolbar color={System.theme.headerFooterColor}>
            <PageLogo marginLeft={'10px'}></PageLogo>
            <IonTitle>{title}</IonTitle>
            <IonButtons slot="end">
              <IonButton onClick={() => onClose()} disabled={closeDisabled}>
                <IonIcon
                  slot="icon-only"
                  icon={closeCircleOutline}
                  color="primary"
                ></IonIcon>
              </IonButton>
            </IonButtons>
          </IonToolbar>
        </IonHeader>
        <IonContent fullscreen class="app-background">
          {children}
        </IonContent>
      </IonModal>
    );
  }
}
