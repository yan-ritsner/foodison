import React from 'react';
import { IonRow, IonCol, IonGrid } from '@ionic/react';
import { xo } from '../../observable';
import { ViewProps } from '../../props';
import { FoodData } from '../../data';
import { MealsViewModel } from './MealsViewModel';

/** Meals Nutrition component props */
export interface MealsNutritionProps extends ViewProps<MealsViewModel> {
  data: FoodData;
  food?: number;
}

/** Meals Nutrition component */
@xo.observer
export class MealsNutrition extends React.Component<MealsNutritionProps> {
  render() {
    const data = this.props.data;
    const food = this.props.food;

    const foodData = data.getFoodData(food);
    const portion = data.getFoodPortion(food);

    return (
      <IonGrid class="ion-no-padding">
        {foodData.nf_total_fat > 0 && (
          <IonRow class="ion-no-padding">
            <IonCol class="ion-no-padding">Total Fat:</IonCol>
            <IonCol className="ion-text-end ion-no-padding">
              {Math.round(foodData.nf_total_fat * portion)} g.
            </IonCol>
          </IonRow>
        )}
        {foodData.nf_saturated_fat > 0 && (
          <IonRow class="ion-no-padding">
            <IonCol class="ion-no-padding">Saturated Fat:</IonCol>
            <IonCol className="ion-text-end ion-no-padding">
              {Math.round(foodData.nf_saturated_fat * portion)} g.
            </IonCol>
          </IonRow>
        )}
        {foodData.nf_cholesterol > 0 && (
          <IonRow class="ion-no-padding">
            <IonCol class="ion-no-padding">Cholesterol:</IonCol>
            <IonCol className="ion-text-end ion-no-padding">
              {Math.round(foodData.nf_cholesterol * portion)} mg.
            </IonCol>
          </IonRow>
        )}
        {foodData.nf_sodium > 0 && (
          <IonRow class="ion-no-padding">
            <IonCol class="ion-no-padding">Sodium:</IonCol>
            <IonCol className="ion-text-end ion-no-padding">
              {Math.round(foodData.nf_sodium * portion)} mg.
            </IonCol>
          </IonRow>
        )}
        {foodData.nf_potassium > 0 && (
          <IonRow class="ion-no-padding">
            <IonCol class="ion-no-padding">Potassium:</IonCol>
            <IonCol className="ion-text-end ion-no-padding">
              {Math.round(foodData.nf_potassium * portion)} mg.
            </IonCol>
          </IonRow>
        )}
        {foodData.nf_total_carbohydrate > 0 && (
          <IonRow class="ion-no-padding">
            <IonCol class="ion-no-padding">Total Carbohydrate:</IonCol>
            <IonCol className="ion-text-end ion-no-padding">
              {Math.round(foodData.nf_total_carbohydrate * portion)} g.
            </IonCol>
          </IonRow>
        )}
        {foodData.nf_dietary_fiber > 0 && (
          <IonRow class="ion-no-padding">
            <IonCol class="ion-no-padding">Dietary Fiber:</IonCol>
            <IonCol className="ion-text-end ion-no-padding">
              {Math.round(foodData.nf_dietary_fiber * portion)} g.
            </IonCol>
          </IonRow>
        )}
        {foodData.nf_sugars > 0 && (
          <IonRow class="ion-no-padding">
            <IonCol class="ion-no-padding">Sugars:</IonCol>
            <IonCol className="ion-text-end ion-no-padding">
              {Math.round(foodData.nf_sugars * portion)} g.
            </IonCol>
          </IonRow>
        )}
        {foodData.nf_protein > 0 && (
          <IonRow class="ion-no-padding">
            <IonCol class="ion-no-padding">Protein:</IonCol>
            <IonCol className="ion-text-end ion-no-padding">
              {Math.round(foodData.nf_protein * portion)} g.
            </IonCol>
          </IonRow>
        )}
      </IonGrid>
    );
  }
}
