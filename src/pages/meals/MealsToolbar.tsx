import React from 'react';
import { IonButtons, IonButton, IonIcon } from '@ionic/react';
import {
  listCircleOutline,
  ellipsisVerticalCircleOutline,
  informationCircleOutline,
} from 'ionicons/icons';
import { xo } from '../../observable';
import { ViewProps } from '../../props';
import { MealsViewModel } from './MealsViewModel';

/** Meals toolbar component props */
export interface MealsToolbarProps extends ViewProps<MealsViewModel> {}

/** Meals toolbar component props */
@xo.observer
export class MealsToolbar extends React.Component<MealsToolbarProps> {
  render() {
    const viewModel = this.props.viewModel;

    return (
      <IonButtons slot="end">
        <IonButton
          onClick={() => viewModel.toggleCardView()}
          className="card-view"
        >
          {viewModel.cardView && (
            <IonIcon
              slot="icon-only"
              icon={ellipsisVerticalCircleOutline}
              color="primary"
            />
          )}
          {!viewModel.cardView && (
            <IonIcon
              slot="icon-only"
              icon={listCircleOutline}
              color="primary"
            />
          )}
        </IonButton>
        <IonButton onClick={() => viewModel.viewInfo()} className="page-info">
          <IonIcon
            slot="icon-only"
            icon={informationCircleOutline}
            color="primary"
          />
        </IonButton>
      </IonButtons>
    );
  }
}
