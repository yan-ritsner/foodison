import React from 'react';
import {
  IonGrid,
  IonRow,
  IonCol,
  IonInfiniteScroll,
  IonInfiniteScrollContent,
  IonListHeader,
  IonItem,
  IonList,
  IonCard,
  IonIcon,
} from '@ionic/react';
import { informationCircleOutline } from 'ionicons/icons';
import { format } from 'date-fns';
import { xo } from '../../observable';
import { ViewProps } from '../../props';
import { MealsViewModel } from './MealsViewModel';
import { MealsPreview } from './MealsPreview';

/** Image Meals Content component props */
export interface MealsContentProps extends ViewProps<MealsViewModel> {}

/** Image Meals Content component */
@xo.observer
export class MealsContent extends React.Component<MealsContentProps> {
  render() {
    const viewModel = this.props.viewModel;
    const rows: JSX.Element[] = [];
    const cardView = viewModel.cardView;

    viewModel.dailyData.forEach((dailyData) => {
      /** Add data header */
      rows.push(
        <IonListHeader key={`header-${dailyData.timestamp.valueOf()}`}>
          <IonGrid>
            <IonRow style={{ borderBottom: '1px dotted #999' }}>
              <IonCol>{format(dailyData.timestamp, 'dd/MM/yyyy')}</IonCol>
              <IonCol className="ion-text-end">{`${dailyData.calories} Cal.`}</IonCol>
            </IonRow>
          </IonGrid>
        </IonListHeader>
      );

      /** Add data */
      if (cardView) {
        rows.push(
          <IonItem key={`data-${dailyData.timestamp.valueOf()}`}>
            <IonGrid>
              <IonRow>
                {dailyData.data.map((data) => (
                  <IonCol size={viewModel.itemColumnSize} key={data.id}>
                    <MealsPreview
                      viewModel={viewModel}
                      data={data}
                      cardView={true}
                    />
                  </IonCol>
                ))}
              </IonRow>
            </IonGrid>
          </IonItem>
        );
      } else {
        dailyData.data.forEach((data) => {
          rows.push(
            <IonItem key={`data-${data.id}`}>
              <MealsPreview
                viewModel={viewModel}
                data={data}
                cardView={false}
              />
            </IonItem>
          );
        });
      }
    });

    if (rows.length === 0) {
      rows.push(
        <IonItem lines="none" key="info">
          <IonIcon
            color={'primary'}
            icon={informationCircleOutline}
            style={{ marginRight: 5 }}
          ></IonIcon>
          Add meal and get calories consumed
        </IonItem>
      );
    }

    return (
      <IonCard>
        <IonList lines="none">{rows}</IonList>
        <IonInfiniteScroll onIonInfinite={(e) => viewModel.loadMoreData(e)}>
          <IonInfiniteScrollContent
            loadingSpinner="circular"
            loadingText="Loading more data..."
          ></IonInfiniteScrollContent>
        </IonInfiniteScroll>
      </IonCard>
    );
  }
}
