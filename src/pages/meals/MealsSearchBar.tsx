import React from 'react';
import { IonSearchbar } from '@ionic/react';
import { xo } from '../../observable';
import { ViewProps } from '../../props';
import { MealsViewModel } from './MealsViewModel';

/** Meals Search Bar component props */
export interface MealsSearchBarProps extends ViewProps<MealsViewModel> {}

/** Meals Search Bar component */
@xo.observer
export class MealsSearchBar extends React.Component<MealsSearchBarProps> {
  render() {
    const viewModel = this.props.viewModel;

    return (
      <div slot="fixed" style={{ width: '100%' }}>
        <IonSearchbar
          debounce={500}
          value={viewModel.searchText}
          onIonChange={(e) => viewModel.setSearchText(e.detail.value!)}
          onKeyPress={(e) => {
            if (e.key === 'Enter') {
              viewModel.doSearch();
              (e.target as any).blur();
            }
          }}
        ></IonSearchbar>
      </div>
    );
  }
}
