import React from 'react';
import { format } from 'date-fns';
import {
  IonCard,
  IonCardHeader,
  IonCardSubtitle,
  IonCol,
  IonGrid,
  IonRow,
} from '@ionic/react';
import { xo } from '../../observable';
import { ViewProps } from '../../props';
import { FoodData } from '../../data';
import { MealsViewModel } from './MealsViewModel';

/** Meals Preview component props */
export interface MealsPreviewProps extends ViewProps<MealsViewModel> {
  data: FoodData;
  cardView: boolean;
}

/** Meals Preview component */
@xo.observer
export class MealsPreview extends React.Component<MealsPreviewProps> {
  render() {
    const viewModel = this.props.viewModel;
    const data = this.props.data;
    const card = this.props.cardView;
    const photo = data.photo;

    if (!photo) viewModel.loadPhoto(data);
    const photoUrl = data.getPhotoUrl(!card);

    if (card) {
      return (
        <IonCard onClick={() => viewModel.selectData(data)}>
          {photoUrl && (
            <img
              style={{ height: 100, width: '100%', objectFit: 'cover' }}
              src={photoUrl}
              alt={''}
            />
          )}
          <IonCardHeader>
            <IonCardSubtitle>
              <span>
                {data.timestamp ? format(data.timestamp, 'HH:mm') : ''}
              </span>
              <span style={{ float: 'right' }}>{`${data.calories} Cal.`}</span>
              <div style={{ textTransform: 'capitalize' }}>{data.name}</div>
            </IonCardSubtitle>
          </IonCardHeader>
        </IonCard>
      );
    } else {
      return (
        <IonGrid onClick={() => viewModel.selectData(data)}>
          <IonRow>
            <IonCol size="2">
              <img
                alt=""
                height={40}
                width={40}
                src={photoUrl}
                style={{ marginRight: 10, borderRadius: 20, marginTop: -10 }}
              ></img>
            </IonCol>
            <IonCol size="5" style={{ textTransform: 'capitalize' }}>
              {data.name}
            </IonCol>
            <IonCol size="2" className="ion-text-end">
              {data.timestamp ? format(data.timestamp, 'HH:mm') : ''}
            </IonCol>
            <IonCol size="3" className="ion-text-end">
              {`${data.calories} Cal.`}
            </IonCol>
          </IonRow>
        </IonGrid>
      );
    }
  }
}
