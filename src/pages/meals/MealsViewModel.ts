import Driver from 'driver.js';
import { xo } from '../../observable';
import { ServiceType, DataSource } from '../../enums';
import { System } from '../../system/System';
import { PageViewModel } from '../page';
import {
  IPhotoStorageService,
  IPhotoClassifierService,
  IFoodNutritionService,
  IFoodDataService,
  IDataInputService,
  IFoodPredictionService,
} from '../../services';
import {
  FoodData,
  FoodDailyData,
  SearchResult,
  NutritionInstant,
  SettingKeys,
} from '../../data';

/**
 * Meals view model
 */
export class MealsViewModel extends PageViewModel {
  /** Selected data */
  @xo.observable
  public selectedData: FoodData | null;

  /** Show actions */
  @xo.observable
  public showActions: boolean = false;
  /** Show details */
  @xo.observable
  public showDetails: boolean = false;
  /** Show search */
  @xo.observable
  public showSearch: boolean = false;
  /** Show selection */
  @xo.observable
  public showSelection: boolean = false;

  /** Search text */
  @xo.observable
  public searchText: string;
  /** Search results */
  @xo.observable
  public searchResults: Map<string, SearchResult[]>;

  /** Loading indicator */
  @xo.observable
  public loading: boolean = false;
  /** Loading indicator text */
  @xo.observable
  public loadingText: string | null = null;

  /** Show error */
  @xo.observable
  public showError: boolean = false;
  /** Error text */
  @xo.observable
  public errorText: string | null = null;

  /** Maximum items in a row */
  @xo.observable
  public itemsInRow: number;

  /** Sort by calories */
  @xo.observable
  public sortByCalories: boolean;

  /** Add button activated */
  @xo.observable
  public addButtonActivated: boolean;

  /** Show info */
  @xo.observable
  public showInfo: boolean = false;

  /** Card or list view */
  @xo.observable
  public cardView: boolean = false;

  /** Info driver */
  private infoDriver: Driver;

  /** Item column size */
  @xo.computed
  public get itemColumnSize() {
    return (12 / this.itemsInRow).toString();
  }

  /** Photo storage service */
  public get photoStorageService(): IPhotoStorageService {
    return this.getService<IPhotoStorageService>(ServiceType.PhotoStorage);
  }

  /** Photo classifier service */
  public get photoClassService(): IPhotoClassifierService {
    return this.getService<IPhotoClassifierService>(
      ServiceType.PhotoClassifier
    );
  }

  /** Data input service */
  public get dataInputService(): IDataInputService {
    return this.getService<IDataInputService>(ServiceType.DataInput);
  }

  /** Food prediction service */
  public get foodPredictionService(): IFoodPredictionService {
    return this.getService<IFoodPredictionService>(ServiceType.FoodPrediction);
  }

  /** Food nutrition service */
  public get foodNutritionService(): IFoodNutritionService {
    return this.getService<IFoodNutritionService>(ServiceType.FoodNutrition);
  }

  /** Food data service */
  public get foodDataService(): IFoodDataService {
    return this.getService<IFoodDataService>(ServiceType.FoodData);
  }

  /** Stored food data array */
  public get data(): FoodData[] {
    return this.foodDataService.data.data;
  }

  /** Stored food data splitted into days */
  public get dailyData(): FoodDailyData[] {
    return this.foodDataService.dailyData;
  }

  /**
   * @constructor
   */
  public constructor() {
    super();
    this.itemsInRow = 2;
    this.sortByCalories = true;
    this.addButtonActivated = false;
    this.cardView = true;
    this.setLoading(true, 'Loading...');
    setTimeout(() => this.init(), 100);
  }

  /** Init component */
  public async init() {
    this.loadSettings();
    this.initInfoDriver();
    await this.photoStorageService.get();
    await this.foodDataService.getInitial();
    this.setLoading(false);
  }

  /** Inits info driver */
  private initInfoDriver() {
    this.infoDriver = new Driver({
      opacity: 0,
      onReset: () => this.hideInfo(),
    });
    this.infoDriver.defineSteps([
      {
        element: '.add-by-photo',
        popover: {
          className: 'info-top-popover',
          title: 'Add meal by taking photo',
          description:
            'We will detect any food or drink on the image, ' +
            'and give you a list of meals and their ingredients detected, ' +
            'including calories for your further selection. ',
          position: 'top',
        },
      },
      {
        element: '.add-by-barcode',
        popover: {
          className: 'info-top-popover',
          title: 'Add meal by scanning barcode',
          description:
            'We will search in our database for an existing grocery food by its barcode.',
          position: 'top',
        },
      },
      {
        element: '.add-by-input',
        popover: {
          title: 'Add meal manually',
          description: 'Search food or drink by typing its name',
          position: 'top',
        },
      },
      {
        element: '.add-by-voice',
        popover: {
          className: 'info-right-popover',
          title: 'Add meal by voice',
          description:
            'Tell us the name of foods and drinks and we will show you matching results.<br>' +
            'For example say: Hamburger and coke.',
          position: 'top',
        },
      },
      {
        element: '.main-menu',
        popover: {
          title: 'Show main menu',
          description:
            'Allows to view and edit user settings,' +
            'such as gender, age, height, width.<br> ' +
            'And also to set up user preferences such as Light/Dark ui mode',
          position: 'right',
        },
      },
      {
        element: '.card-view',
        popover: {
          title: 'Switch view',
          description: 'Switch between card and list view',
          position: 'left',
        },
      },
      {
        element: '.page-info',
        popover: {
          title: 'Show page help',
          description: 'Shows this page help and guide',
          position: 'left',
        },
      },
    ]);
  }

  /** Start info driver*/
  private startInfoDriver() {
    this.infoDriver.start();
  }

  /** Loads settings */
  private async loadSettings() {
    const cardView = await System.userSettings.get<boolean>(
      SettingKeys.cardView
    );
    if (cardView != null) this.cardView = cardView;
  }

  /** Saves settings */
  private async saveSettings() {
    await System.userSettings.set(SettingKeys.cardView, this.cardView);
  }

  /** Loads more data */
  public async loadMoreData(e: any) {
    if (!this.foodDataService.hasMore()) {
      e.target.complete();
      e.target.disabled = true;
    } else {
      await this.foodDataService.getMore();
      e.target.complete();
    }
  }

  /** Adds new food data */
  public async addData(dataSource: DataSource) {
    switch (dataSource) {
      case DataSource.Photo:
        await this.addDataByPhoto();
        break;
      case DataSource.Input:
        await this.addDataByInput();
        break;
      case DataSource.Barcode:
        await this.addDataByBarcode();
        break;
      case DataSource.Voice:
        await this.addDataByVoice();
        break;
    }
    System.googleAnalytics.event('Meals', `AddBy${DataSource[dataSource]}`);
  }

  /** Deletes food data */
  public async deleteData() {
    if (this.selectedData == null) return;
    if (!this.selectedData.photo) {
      await this.loadPhoto(this.selectedData);
    }
    const photo = this.selectedData.photo;
    if (photo) await this.photoStorageService.delete(photo);
    await this.foodDataService.delete(this.selectedData);
    this.unselectData();
  }

  /** Add data by taking photo */
  public async addDataByPhoto() {
    // take photo
    const photo = await this.photoStorageService.add();
    if (photo == null) return;

    this.setLoading(true, 'Processing photo...');

    // get predicted concepts from photo
    const d1 = new Date();
    console.log(`Length:${photo.base64.length / 1000000} MB`);
    let concepts = await this.foodPredictionService.predict(photo.base64);
    if (concepts == null) {
      this.setLoading(false);
      this.photoStorageService.delete(photo);
      return;
    }
    const d2 = new Date();
    console.log(`Time:${d2.valueOf() - d1.valueOf()}`);

    // filter concepts by probability > 80%
    concepts = concepts.filter((c) => c.value > 0.8);

    // get nutrition data for concepts
    const query = concepts.map((c) => c.name).join('\r\n');
    const nutrition = await this.foodNutritionService.getNutritionDataByNlp(
      query,
      true,
      true
    );

    // show error
    if (nutrition == null) {
      this.viewError('No matching results found');
      this.setLoading(false);
      return;
    }

    // create data and set properties
    const foodData = new FoodData();
    foodData.initData(DataSource.Photo, nutrition, photo);

    // hide loading indicator
    this.setLoading(false);
    // view data
    this.selectData(foodData, false, false, true);
  }

  /** Add data by nlp search */
  private async addDataByNlp(query: string, source: DataSource) {
    // get nutrition data
    const nutrition = await this.foodNutritionService.getNutritionDataByNlp(
      query
    );

    // show error
    if (nutrition == null) {
      this.viewError('No matching results found');
      this.setLoading(false);
      return;
    }

    // create data and set properties
    const foodData = new FoodData();
    foodData.initData(source, nutrition);

    // hide loading indicator
    this.setLoading(false);
    // view new data
    this.selectData(foodData, false, false, true);
  }

  /** Add data by upc search */
  private async addDataByUpc(upc?: string, nix_item_id?: string) {
    // get nutrition data
    const nutrition = await this.foodNutritionService.getNutritionDataByUpc(
      upc,
      nix_item_id
    );

    // show error
    if (nutrition == null) {
      this.viewError('No matching results found');
      this.setLoading(false);
      return;
    }

    // create data and set properties
    const foodData = new FoodData();
    foodData.initData(DataSource.Barcode, nutrition);

    // hide loading indicator
    this.setLoading(false);
    // view new data
    this.selectData(foodData, false, false, true);
  }

  /** Add data by manual input */
  public async addDataByInput() {
    this.viewSearch();
  }

  /** Add data by scanning barcode */
  public async addDataByBarcode() {
    // scan barcode
    const upc = await this.dataInputService.scanBarCode();
    if (!upc) return;

    //show loading indicator
    this.setLoading(true, 'Processing barcode...');

    // create data
    await this.addDataByUpc(upc);
  }

  /** Add data by voice recognition */
  public async addDataByVoice() {
    // recognize speech
    await this.dataInputService.recognizeSpeech(
      async (matches) => {
        //show loading indicator
        this.setLoading(true, 'Processing voice...');

        // create data
        await this.addDataByNlp(matches[0], DataSource.Voice);
      },
      (error) => {
        console.log(error);
        // hide loading indicator
        this.setLoading(false);
      }
    );

    //show loading indicator
    this.setLoading(true, 'Processing speech...');
  }

  /** Gets assigned photo for food data */
  public async loadPhoto(data: FoodData) {
    if (data.photoId == null) return;
    if (data.photo != null) return;
    if (data.loadingPhoto || data.loadedPhoto) return;
    data.setLoadingPhoto();
    const photo = await this.photoStorageService.getById(data.photoId);
    data.setPhoto(photo);
  }

  /** Selects data */
  @xo.action
  public selectData(
    data: FoodData,
    viewActions: boolean = true,
    viewData: boolean = false,
    viewSelection: boolean = false
  ): void {
    this.selectedData = data;
    if (viewActions) this.viewActions();
    if (viewData) this.viewData();
    if (viewSelection) this.viewSelection();
  }

  /** Unselects data */
  @xo.action
  public unselectData(): void {
    this.selectedData = null;
    this.hideActions();
  }

  /** View data actions */
  @xo.action
  public viewActions() {
    this.showActions = true;
  }

  /** Hide data actions */
  @xo.action
  public hideActions() {
    this.showActions = false;
  }

  /** View data details */
  @xo.action
  public viewData() {
    if (this.selectedData == null) return;
    this.hideActions();
    this.viewDetails();
  }

  /** Hide data details */
  @xo.action
  public hideData() {
    this.selectedData = null;
    this.hideDetails();
    this.hideSelection();
  }

  /** Hide data details */
  @xo.action
  public async acceptSelection() {
    this.hideSelection();
    this.viewData();
  }

  /** Accepts new data */
  public async acceptData() {
    this.selectedData.acceptsData();
    await this.foodDataService.add(this.selectedData);
    this.hideData();
  }

  /** Cancels new data */
  public async cancelData() {
    if (this.selectedData.photoId) {
      const photo = await this.photoStorageService.getById(
        this.selectedData.photoId
      );
      if (photo) await this.photoStorageService.delete(photo);
    }
    this.hideData();
  }

  /** View data details */
  @xo.action
  public viewDetails() {
    this.showDetails = true;
  }

  /** Hide data details */
  @xo.action
  public hideDetails() {
    this.showDetails = false;
  }

  /** View data search */
  @xo.action
  public viewSearch() {
    this.showSearch = true;
  }

  /** Hide data search */
  @xo.action
  public hideSearch() {
    this.showSearch = false;
    this.setSearchText(null);
  }

  /** View selection */
  @xo.action
  public viewSelection() {
    this.showSelection = true;
  }

  /** Hide selection */
  @xo.action
  public hideSelection() {
    this.showSelection = false;
  }

  /** Sets search text */
  @xo.action
  public setSearchText(searchText: string) {
    this.searchText = searchText;
    this.doSearch();
  }

  /** Performs search */
  public async doSearch() {
    if (!this.searchText) {
      this.searchResults = null;
      return;
    }
    const results = await this.getSearchResults();
    if (!results) {
      this.searchResults = null;
      return;
    }
    this.setSearchResults(results);
  }

  /** Gets search results */
  private async getSearchResults() {
    const results = await this.foodNutritionService.getNutritionInstant(
      this.searchText
    );
    return results;
  }

  /** Sets search results */
  @xo.action
  private setSearchResults(results: NutritionInstant) {
    const searchResults: Map<string, SearchResult[]> = new Map();
    searchResults.set(
      'Common',
      results.common.map((r) => ({
        id: r.food_name,
        group: 'Common',
        name: r.food_name,
        image: r.photo?.thumb,
      }))
    );
    searchResults.set(
      'Branded',
      results.branded.map((r) => ({
        id: r.nix_item_id,
        group: 'Branded',
        name: r.food_name,
        image: r.photo?.thumb,
      }))
    );
    this.searchResults = searchResults;
  }

  /** Selects search result */
  public async selectSearchResult(searchResult: SearchResult) {
    this.hideSearch();
    if (searchResult.group === 'Common') {
      await this.addDataByNlp(searchResult.name, DataSource.Input);
    } else if (searchResult.group === 'Branded') {
      await this.addDataByUpc(undefined, searchResult.id);
    }
  }

  /** Sets loading indicator and text */
  @xo.action
  public setLoading(loading: boolean, loadingText: string | null = null) {
    this.loading = loading;
    this.loadingText = loadingText;
  }

  /** Sets sort by calories */
  @xo.action
  public setSortByCalories(sortByCalories: boolean) {
    this.sortByCalories = sortByCalories;
  }

  /** Shows error */
  @xo.action
  public viewError(error: string) {
    this.errorText = error;
    this.showError = true;
  }

  /** Hides error */
  @xo.action
  public hideError() {
    this.errorText = null;
    this.showError = false;
  }

  /** Shows info */
  @xo.action
  public viewInfo() {
    this.showInfo = true;
    this.addButtonActivated = true;
    setTimeout(() => {
      this.startInfoDriver();
    }, 200);
  }

  /** Hides info */
  @xo.action
  public hideInfo() {
    this.showInfo = false;
    this.addButtonActivated = false;
  }

  /** Toggles card view */
  @xo.action
  public toggleCardView() {
    this.cardView = !this.cardView;
    this.saveSettings();
  }
}
