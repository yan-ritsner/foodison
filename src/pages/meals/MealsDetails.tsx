import React from 'react';
import {
  IonCard,
  IonCardContent,
  IonCardHeader,
  IonCardTitle,
} from '@ionic/react';
import { xo } from '../../observable';
import { ViewProps } from '../../props';
import { PageAttribution } from '../page';
import { MealsViewModel } from './MealsViewModel';
import { MealsAcceptButton } from './MealsAcceptButton';
import { MealsProperties } from './MealsProperties';
import { MealsModal } from './MealsModal';

/** Meals Modal component props */
export interface MealsModalProps extends ViewProps<MealsViewModel> {}

/** Meals Modal component */
@xo.observer
export class MealsDetails extends React.Component<MealsModalProps> {
  render() {
    const viewModel = this.props.viewModel;
    const data = viewModel.selectedData;
    if (!data) return '';
    const photo = data.photo;
    if (!photo) viewModel.loadPhoto(data);
    const photoUrl = data.getPhotoUrl(false);
    const foods = data.getSelectedFoods();

    return (
      <MealsModal
        title={'Meal Details'}
        open={viewModel.showDetails}
        closeDisabled={viewModel.loading}
        element={viewModel.ref.current}
        onClose={() =>
          data.newData ? viewModel.cancelData() : viewModel.hideData()
        }
        onDismiss={() => viewModel.hideData()}
      >
        <IonCard style={{ marginBottom: 70 }}>
          {photoUrl && (
            <img
              alt={''}
              style={{ height: 300, width: '100%', objectFit: 'cover' }}
              src={photoUrl}
            ></img>
          )}
          <IonCardHeader>
            <IonCardTitle>
              <PageAttribution
                style={{ marginTop: -21, float: 'right' }}
              ></PageAttribution>
            </IonCardTitle>
          </IonCardHeader>
          <IonCardContent>
            {foods.map((food, index) => (
              <MealsProperties
                key={food.index}
                viewModel={viewModel}
                data={data}
                food={food.index}
                border={index < foods.length - 1}
              ></MealsProperties>
            ))}
          </IonCardContent>
        </IonCard>
        {data.newData && (
          <MealsAcceptButton
            disabled={viewModel.loading}
            onAccept={() => viewModel.acceptData()}
          ></MealsAcceptButton>
        )}
      </MealsModal>
    );
  }
}
