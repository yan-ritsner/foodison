import React from 'react';
import { IonFab, IonFabButton, IonIcon } from '@ionic/react';
import { checkmark } from 'ionicons/icons';

/** Meals Accept Button component props */
export interface MealsAcceptButtonProps {
  onAccept: () => void;
  disabled: boolean;
}

/** Meals Accept Button component */
export class MealsAcceptButton extends React.Component<MealsAcceptButtonProps> {
  render() {
    const onAccept = this.props.onAccept;
    const disabled = this.props.disabled;
    return (
      <IonFab vertical="bottom" horizontal="center" slot="fixed">
        <IonFabButton onClick={() => onAccept()} disabled={disabled}>
          <IonIcon icon={checkmark}></IonIcon>
        </IonFabButton>
      </IonFab>
    );
  }
}
