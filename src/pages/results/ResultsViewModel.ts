import { startOfDay, startOfWeek, startOfMonth, addDays } from 'date-fns';
import { xo } from '../../observable';
import { System } from './../../system/';
import { PageViewModel } from '../page';
import {
  UnitsType,
  WeightGoal,
  ReportingPeriod,
  ServiceType,
  UnitsFactor,
} from '../../enums';
import { ResultsSettings, SettingKeys } from './../../data';
import { IFoodDataService, IActivityDataService } from '../../services';

/** Results view model */
export class ResultsViewModel extends PageViewModel {
  /** Selected weight goal */
  @xo.observable
  public weightGoal: WeightGoal;
  /** Selected weight */
  @xo.observable
  public weight: number;
  /** Reporting period */
  @xo.observable
  public reportingPeriod: ReportingPeriod;

  /** Loading indicator */
  @xo.observable
  public loading: boolean = false;
  /** Loading indicator text */
  @xo.observable
  public loadingText: string | null = null;

  /** Weight values */
  public get weights(): number[] {
    return [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 1, 2, 4, 5, 6, 7, 8, 9, 10];
  }

  /** Weight units */
  public get weightUnits(): string {
    if (!System.user.anthropometry) return '';
    switch (System.user.anthropometry.units) {
      case UnitsType.English:
        return 'Lbs';
      case UnitsType.Metric:
        return 'Kg';
      default:
        return '-';
    }
  }

  /** Progress start date */
  @xo.computed
  public get startDate() {
    let date: Date;
    switch (this.reportingPeriod) {
      case ReportingPeriod.Day:
        date = startOfDay(new Date());
        break;
      case ReportingPeriod.Week:
        date = startOfWeek(new Date());
        break;
      case ReportingPeriod.Month:
        date = startOfMonth(new Date());
        break;
    }
    return date;
  }

  /** Progress in calories */
  @xo.computed
  public get progressInCalories() {
    let total = 0;
    let date = this.startDate;
    const endDate = startOfDay(new Date());
    const bmr = System.user.anthropometry ? System.user.anthropometry.bmr : 0;
    while (date <= endDate) {
      const dateValue = date.valueOf();
      const burnedData = this.activityDataService.dailyDataMap.get(dateValue);
      const consumedData = this.foodDataService.dailyDataMap.get(dateValue);
      const burnedCalories = burnedData ? burnedData.calories + bmr : 0;
      const consumedCalories = consumedData ? consumedData.calories : 0;
      total += consumedCalories - burnedCalories;
      date = addDays(date, 1);
    }
    return total;
  }

  /** Progress in weight */
  @xo.computed
  public get progressInWeight() {
    const calories = this.progressInCalories;
    if (calories === 0) return 0;
    const pounds = calories / 3500;
    const weight =
      System.user.anthropometry?.units === UnitsType.Metric
        ? pounds / UnitsFactor.WeightKgToLb
        : pounds;
    return Math.round(weight * 100) / 100;
  }

  /** Progress is remained */
  @xo.computed
  public get progressRemained() {
    const weight = this.weight - Math.abs(this.progressInWeight);
    return Math.round(weight * 100) / 100;
  }

  /** Progress is positive */
  @xo.computed
  public get progressPositive() {
    const progress = this.progressInWeight;
    switch (this.weightGoal) {
      case WeightGoal.Lose:
        if (progress < 0) return true;
        break;
      case WeightGoal.Maintain:
        if (Math.abs(progress) < 0.1) return true;
        break;
      case WeightGoal.Gain:
        if (progress > 0) return true;
        break;
    }
    return false;
  }

  /** Food data service */
  public get foodDataService(): IFoodDataService {
    return this.getService<IFoodDataService>(ServiceType.FoodData);
  }
  /** Activity data service */
  public get activityDataService(): IActivityDataService {
    return this.getService<IActivityDataService>(ServiceType.ActivityData);
  }

  /** @constructor */
  public constructor() {
    super();
    this.weightGoal = WeightGoal.Maintain;
    this.weight = 1;
    this.reportingPeriod = ReportingPeriod.Week;
    this.setLoading(true, 'Loading...');
    setTimeout(() => this.init(), 100);
  }

  /** Init component */
  public async init() {
    await this.loadSettings();
    await this.getInitialData();
    await this.getMoreData();
    this.setLoading(false);
  }

  /** Get more data */
  private async getInitialData() {
    await this.foodDataService.getInitial();
    await this.activityDataService.getInitial();
  }

  /** Get more data */
  private async getMoreData() {
    await this.getMoreConsumedData();
    await this.getMoreBurnedData();
  }

  /** Get more consumed data */
  private async getMoreConsumedData() {
    const startDate = this.startDate ?? startOfMonth(new Date());
    const service = this.foodDataService;
    while (service.hasMore() && service.startDate() >= startDate) {
      await service.getMore();
    }
  }

  /** Get more burned data */
  private async getMoreBurnedData() {
    const startDate = this.startDate ?? startOfMonth(new Date());
    const service = this.activityDataService;
    while (service.hasMore() && service.startDate() >= startDate) {
      await service.getMore();
    }
    this.setLoading(false);
  }

  /** Sets weight goal value */
  @xo.action
  public setWeightGoal(weightGoal: WeightGoal) {
    this.weightGoal = weightGoal;
    this.saveSettings();
  }
  /** Sets weight value */
  @xo.action
  public setWeight(weight: number) {
    this.weight = weight;
    this.saveSettings();
  }
  /** Sets reporting period value */
  @xo.action
  public setReportingPeriod(reportingPeriod: ReportingPeriod) {
    this.reportingPeriod = reportingPeriod;
    this.getMoreData();
    this.saveSettings();
  }

  /** Sets loading indicator and text */
  @xo.action
  public setLoading(loading: boolean, loadingText: string | null = null) {
    this.loading = loading;
    this.loadingText = loadingText;
  }

  @xo.action
  private setSettings(settings: ResultsSettings) {
    this.weightGoal = settings.weightGoal;
    this.weight = settings.weight;
    this.reportingPeriod = settings.reportingPeriod;
  }

  /** Saves settings */
  private async saveSettings() {
    const settings: ResultsSettings = {
      weightGoal: this.weightGoal,
      weight: this.weight,
      reportingPeriod: this.reportingPeriod,
    };
    await System.userSettings.set(SettingKeys.resultSettings, settings);
  }

  /** Loads settings */
  private async loadSettings() {
    const settings = await System.userSettings.get<ResultsSettings>(
      SettingKeys.resultSettings
    );
    if (settings != null) {
      this.setSettings(settings);
    }
  }
}
