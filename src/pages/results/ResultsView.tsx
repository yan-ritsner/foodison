import * as React from 'react';
import {
  IonContent,
  IonSegment,
  IonSegmentButton,
  IonLabel,
  IonList,
  IonItem,
  IonSelect,
  IonSelectOption,
  IonLoading,
  IonGrid,
  IonRow,
  IonCol,
  IonText,
  IonCard,
  IonListHeader,
} from '@ionic/react';
import { format } from 'date-fns';
import { xo } from '../../observable';
import { System } from '../../system';
import { ViewProps } from '../../props';
import { PageAnthropometry } from '../page';
import {
  weightGoals,
  reportingPeriods,
  WeightGoal,
  ReportingPeriod,
} from '../../enums';
import { ResultsViewModel } from './ResultsViewModel';

export interface ResultsViewProps extends ViewProps<ResultsViewModel> {}

@xo.observer
export class ResultsView extends React.Component<ResultsViewProps> {
  render() {
    const viewModel = this.props.viewModel;
    return (
      <IonContent class="app-background">
        <PageAnthropometry></PageAnthropometry>
        <IonCard>
          <IonList lines="none">
            <IonListHeader>
              <IonLabel>Weight Goal</IonLabel>
            </IonListHeader>
            <IonItem>
              <IonSegment
                value={viewModel.weightGoal}
                onIonChange={(e) =>
                  viewModel.setWeightGoal(e.detail.value as WeightGoal)
                }
              >
                {weightGoals.map((v) => (
                  <IonSegmentButton key={v} value={v}>
                    <IonLabel>{v}</IonLabel>
                  </IonSegmentButton>
                ))}
              </IonSegment>
            </IonItem>
            {viewModel.weightGoal !== WeightGoal.Maintain && (
              <IonItem>
                <IonLabel position="fixed">{`Weight ${viewModel.weightUnits}:`}</IonLabel>
                <IonSelect
                  value={viewModel.weight}
                  okText="Ok"
                  cancelText="Cancel"
                  interface="action-sheet"
                  onIonChange={(e) =>
                    viewModel.setWeight(Number(e.detail.value))
                  }
                >
                  {viewModel.weights.map((weight) => (
                    <IonSelectOption key={weight} value={weight}>
                      {weight}
                    </IonSelectOption>
                  ))}
                </IonSelect>
              </IonItem>
            )}
            <IonListHeader>
              <IonLabel>Reporting Period</IonLabel>
            </IonListHeader>
            <IonItem>
              <IonSegment
                value={viewModel.reportingPeriod}
                onIonChange={(e) =>
                  viewModel.setReportingPeriod(
                    e.detail.value as ReportingPeriod
                  )
                }
              >
                {reportingPeriods.map((v) => (
                  <IonSegmentButton key={v} value={v}>
                    <IonLabel>{v}</IonLabel>
                  </IonSegmentButton>
                ))}
              </IonSegment>
            </IonItem>
            <IonListHeader>
              <IonLabel>{`Progress since ${format(
                viewModel.startDate,
                'dd/MM/yyyy'
              )}`}</IonLabel>
            </IonListHeader>
            <IonItem>
              <IonGrid class="ion-no-padding">
                <IonRow class="ion-no-padding">
                  <IonCol class="ion-no-padding">Calories:</IonCol>
                  <IonCol className="ion-no-padding ion-text-end">
                    <IonText
                      color={viewModel.progressPositive ? 'primary' : 'danger'}
                    >
                      {`${viewModel.progressInCalories} Cal`}
                    </IonText>
                  </IonCol>
                </IonRow>
              </IonGrid>
            </IonItem>
            <IonItem>
              <IonGrid class="ion-no-padding">
                <IonRow class="ion-no-padding">
                  <IonCol class="ion-no-padding">Weight:</IonCol>
                  <IonCol className="ion-no-padding ion-text-end">
                    <IonText
                      color={viewModel.progressPositive ? 'primary' : 'danger'}
                    >
                      {`${viewModel.progressInWeight} ${viewModel.weightUnits}`}
                    </IonText>
                  </IonCol>
                </IonRow>
              </IonGrid>
            </IonItem>
            {System.user.anthropometry &&
              viewModel.progressPositive &&
              viewModel.progressRemained > 0 &&
              viewModel.weightGoal !== WeightGoal.Maintain && (
                <>
                  <IonItem>
                    <IonText color={'primary'}>You are doing great!</IonText>
                  </IonItem>
                  <IonItem>
                    <IonText color={'primary'}>
                      {`${viewModel.progressRemained} ${viewModel.weightUnits} Remained.`}
                    </IonText>
                  </IonItem>
                </>
              )}
            {System.user.anthropometry &&
              viewModel.progressPositive &&
              viewModel.progressRemained <= 0 &&
              viewModel.weightGoal !== WeightGoal.Maintain && (
                <IonItem>
                  <IonText color={'primary'}>Good Job! Goal Achieved!</IonText>
                </IonItem>
              )}
          </IonList>
          <IonLoading
            isOpen={viewModel.loading}
            message={viewModel.loadingText}
          />
        </IonCard>
      </IonContent>
    );
  }
}
