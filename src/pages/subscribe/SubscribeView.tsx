import React from 'react';

import {
  IonContent,
  IonCard,
  IonCardContent,
  IonLoading,
  IonCol,
  IonRow,
  IonGrid,
  IonBadge,
  IonButton,
  IonToast,
} from '@ionic/react';

import { xo } from '../../observable';
import { ViewProps } from '../../props';

import { SubscribeViewModel } from './SubscribeViewModel';
import './Subscribe.css';

/** Cleanup View component props */
export interface SubscribeViewProps extends ViewProps<SubscribeViewModel> {}

/** Subscribe page */
@xo.observer
export class SubscribeView extends React.Component<SubscribeViewProps> {
  render() {
    const viewModel = this.props.viewModel;
    return (
      <IonContent class="app-background">
        {!viewModel.loading && (
          <IonCard className={'fade-in'}>
            <IonCardContent>
              <IonGrid>
                {viewModel.view && (
                  <IonRow>
                    <IonCol size="12" className={'subscribe-container'}>
                      You have an active subscription
                    </IonCol>
                  </IonRow>
                )}
                {!viewModel.view && (
                  <>
                    <IonRow>
                      <IonCol size="12" className={'subscribe-container'}>
                        {viewModel.offers.length === 0 && (
                          <div>No subscription plans found</div>
                        )}
                        {viewModel.offers.length > 0 && (
                          <div>Please choose your subscription plan</div>
                        )}
                      </IonCol>
                    </IonRow>
                    <IonRow className={'offering-container'}>
                      {viewModel.offers.map((offer) => (
                        <IonCol key={offer.displayName} size="6">
                          {offer.badge && (
                            <IonBadge
                              className={'offering-badge'}
                              color="primary"
                            >
                              {offer.badge}
                            </IonBadge>
                          )}
                          <IonCard
                            className={`offering ${
                              viewModel.selectedOffer === offer
                                ? 'offering-selected pop-in-animate'
                                : ''
                            }`}
                            onClick={() => viewModel.selectOffer(offer)}
                          >
                            <div className={'offering-row'}>
                              {offer.duration}
                            </div>
                            <div className={'offering-row'}>
                              {offer.periodName}
                            </div>
                            <div className={'offering-row offering-price'}>
                              {offer.priceString}
                            </div>
                            <div
                              className={
                                'offering-row offering-price-per-month'
                              }
                            >
                              {offer.priceDetails}
                            </div>
                          </IonCard>
                        </IonCol>
                      ))}
                    </IonRow>
                    <IonRow>
                      <IonCol size="12" className={'subscribe-container'}>
                        <IonButton
                          onClick={() => viewModel.subscribe()}
                          disabled={!viewModel.selectedOffer}
                        >
                          Continue
                        </IonButton>
                      </IonCol>
                    </IonRow>
                    <IonRow>
                      <IonCol
                        size="12"
                        className={'subscribe-container subscribe-details'}
                      >
                        Subscriptions will be charged to your credit card <br />
                        through your {viewModel.storeName} account.
                        <br />
                        Your subscription will automatically renew unless
                        canceled
                        <br />
                        at least 24 hours before the end of the current period.
                        <br />
                        Manage your subscriptions in {viewModel.storeName}
                        <br /> account settings after purchase.
                      </IonCol>
                    </IonRow>
                    <IonRow>
                      <IonCol size="12" className={'subscribe-container'}>
                        By continuing, you agree to our <br />
                        <a href="https://www.foodison.net/terms-conditions">
                          Terms of Service
                        </a>
                        &nbsp;and&nbsp;
                        <a href="https://www.foodison.net/privacy-policy">
                          Privacy Policy
                        </a>
                      </IonCol>
                    </IonRow>
                  </>
                )}
              </IonGrid>
            </IonCardContent>
          </IonCard>
        )}
        <IonLoading
          isOpen={viewModel.loading}
          message={viewModel.loadingText}
        />
        <IonToast
          isOpen={viewModel.showError}
          onDidDismiss={() => viewModel.hideError()}
          message={viewModel.errorText}
          position={'bottom'}
          buttons={[
            {
              text: 'Close',
              role: 'cancel',
              handler: () => viewModel.hideError(),
            },
          ]}
        />
      </IonContent>
    );
  }
}
