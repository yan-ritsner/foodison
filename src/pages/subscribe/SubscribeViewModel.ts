import { isPlatform } from '@ionic/react';
import { xo } from '../../observable';
import { System } from '../../system';
import { PageViewModel } from '../page';
import { OfferData, SubscribeData } from './../../data/';
import { OfferPeriod, ServiceType } from '../../enums';
import { ISubscribeService } from '../../services';

/** Subscribe view model */
export class SubscribeViewModel extends PageViewModel {
  /** Offers array */
  @xo.observable
  public offers: OfferData[];
  /** Selected offer */
  @xo.observable
  public selectedOffer: OfferData;

  /** Loading indicator */
  @xo.observable
  public loading: boolean = false;
  /** Loading indicator text */
  @xo.observable
  public loadingText: string | null = null;

  /** Show error */
  @xo.observable
  public showError: boolean = false;
  /** Error text */
  @xo.observable
  public errorText: string | null = null;

  /** View mode */
  @xo.observable
  public view: boolean = false;

  /** Returns true if platform is ios */
  public get isIos() {
    return isPlatform('ios');
  }
  /** Returns true if platform is android */
  public get isAndroid() {
    return isPlatform('android');
  }
  /** App Store name */
  public get storeName() {
    if (this.isIos) {
      return 'Apple App Store';
    } else if (this.isAndroid) {
      return 'Google Play Store';
    }
    return '';
  }

  /** Subscription service */
  public get subscribeService(): ISubscribeService {
    return this.getService<ISubscribeService>(ServiceType.Subscribe);
  }

  /**
   * @constructor
   */
  constructor() {
    super();
    this.offers = [];
    this.setLoading(true, 'Loading...');
    setTimeout(() => this.init(), 100);
  }

  /** Init page */
  private async init() {
    const subscribed = await this.checkSubscription();
    if (subscribed) {
      this.setLoading(false);
      const view = this.search?.get('view');
      if (view) this.setView(true);
      else this.redirectToApp();
    } else {
      await this.initOffers();
      this.setLoading(false);
    }
  }

  /** Init offers */
  private async initOffers() {
    this.subscribeService.init(System.user.info.uid);
    const result = await this.subscribeService.getOfferings();
    if (result instanceof SubscribeData) {
      this.viewError(
        `An error occurred, please contact support.<br/>${JSON.stringify(
          result.error
        )}`
      );
    } else {
      this.processOffers(result);
      this.setOffers(result);
    }
  }

  /** Process offers */
  private processOffers(offers: OfferData[]) {
    const monthly = offers.find(
      (offer) => offer.duration === 1 && offer.period === OfferPeriod.Month
    );
    if (!monthly) return;
    offers.forEach((offer) => this.updateOffer(offer, monthly));
    this.selectOffer(monthly);
  }

  /** Update offer */
  private updateOffer(offer: OfferData, monthly: OfferData) {
    let months = this.getOfferMonths(offer);
    let monthlyPrice = offer.price / months;
    monthlyPrice = Math.floor(monthlyPrice * 100) / 100;
    offer.priceDetails = `${monthlyPrice.toFixed(2)} / mo`;
    if (monthlyPrice < monthly.price) {
      const monthlyPercent = Math.round(
        100 - (monthlyPrice / monthly.price) * 100
      );
      offer.badge = `SAVE ${monthlyPercent}%`;
    } else if (monthlyPrice > monthly.price) {
      offer.badge = `TRY IT!`;
    }
  }

  /** Returns offer months */
  private getOfferMonths(offer: OfferData) {
    switch (offer.period) {
      case OfferPeriod.Week: {
        return (1 / 4.333) * offer.duration;
      }
      case OfferPeriod.Month: {
        return offer.duration;
      }
      case OfferPeriod.Year: {
        return offer.duration * 12;
      }
    }
  }

  /** Sets offers */
  @xo.action
  private setOffers(offers: OfferData[]) {
    this.offers = offers;
  }

  /** Select offer */
  @xo.action
  public selectOffer(offer: OfferData) {
    this.selectedOffer = offer;
  }

  /** Set view mode */
  @xo.action
  public setView(view: boolean) {
    this.view = view;
  }

  /** Subscribe to selected offer */
  @xo.action
  public async subscribe() {
    this.setLoading(true, 'Subscribing...');
    const subscription = await this.subscribeService.purchasePackage(
      this.selectedOffer
    );
    this.setLoading(false);
    if (subscription == null) return;
    if (subscription.isActive) {
      this.redirectToApp();
    } else {
      this.viewError(
        `An error occurred, please contact support.<br/>${JSON.stringify(
          subscription.error
        )}`
      );
    }
  }

  /** Checks for active subscription */
  private async checkSubscription() {
    this.subscribeService.init(System.user?.info?.uid);
    const subscription = await this.subscribeService.getSubscription();
    return subscription != null && subscription.isActive;
  }

  /** Redirect to app on successful subscription */
  private redirectToApp() {
    System.user.setSubscribed(true);
    this.history.push('/meals');
  }

  /** Sets loading indicator and text */
  @xo.action
  private setLoading(loading: boolean, loadingText: string | null = null) {
    this.loading = loading;
    this.loadingText = loadingText;
  }

  /** Shows error */
  @xo.action
  public viewError(error: string) {
    this.errorText = error;
    this.showError = true;
  }

  /** Hides error */
  @xo.action
  public hideError() {
    this.errorText = null;
    this.showError = false;
  }
}
