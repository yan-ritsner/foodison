import * as React from 'react';
import { IonFab, IonFabButton, IonIcon } from '@ionic/react';
import { checkmark } from 'ionicons/icons';
import { xo } from '../../observable';
import { ViewProps } from '../../props';
import { AnthropometryViewModel } from './AnthropometryViewModel';

/** Anthropometry Accept Button component props */
export interface AnthropometryAcceptButtonProps
  extends ViewProps<AnthropometryViewModel> {}

/** Anthropometry Accept Button component */
@xo.observer
export class AnthropometryAcceptButton extends React.Component<
  AnthropometryAcceptButtonProps
> {
  render() {
    const viewModel = this.props.viewModel;
    return (
      <IonFab vertical="bottom" horizontal="center" slot="fixed">
        <IonFabButton
          disabled={!viewModel.hasChanges}
          onClick={() => viewModel.saveSettings()}
        >
          <IonIcon icon={checkmark} />
        </IonFabButton>
      </IonFab>
    );
  }
}
