import * as React from 'react';
import {
  IonItem,
  IonLabel,
  IonRadioGroup,
  IonListHeader,
  IonRadio,
} from '@ionic/react';
import { xo } from '../../observable';
import { ViewProps } from '../../props';
import { genderTypes } from '../../enums';
import { AnthropometryViewModel } from './AnthropometryViewModel';

/** Anthropometry Gender component props */
export interface AnthropometryGenderProps
  extends ViewProps<AnthropometryViewModel> {}

/** Anthropometry Gender component */
@xo.observer
export class AnthropometryGender extends React.Component<
  AnthropometryGenderProps
> {
  render() {
    const viewModel = this.props.viewModel;
    return (
      <IonRadioGroup
        value={viewModel.gender}
        onIonChange={(e) => viewModel.setGender(e.detail.value)}
      >
        <IonListHeader>
          <IonLabel>Gender</IonLabel>
        </IonListHeader>
        {genderTypes.map((gender) => (
          <IonItem key={gender}>
            <IonLabel>{gender}</IonLabel>
            <IonRadio slot="start" value={gender} />
          </IonItem>
        ))}
      </IonRadioGroup>
    );
  }
}
