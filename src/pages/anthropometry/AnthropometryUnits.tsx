import * as React from 'react';
import {
  IonItem,
  IonLabel,
  IonRadioGroup,
  IonListHeader,
  IonRadio,
} from '@ionic/react';
import { xo } from '../../observable';
import { ViewProps } from '../../props';
import { unitsTypes } from '../../enums';
import { AnthropometryViewModel } from './AnthropometryViewModel';

/** Anthropometry Units component props */
export interface AnthropometryUnitsProps
  extends ViewProps<AnthropometryViewModel> {}

/** Anthropometry Units component */
@xo.observer
export class AnthropometryUnits extends React.Component<
  AnthropometryUnitsProps
> {
  render() {
    const viewModel = this.props.viewModel;
    return (
      <IonRadioGroup
        value={viewModel.units}
        onIonChange={(e) => viewModel.setUnits(e.detail.value)}
      >
        <IonListHeader>
          <IonLabel>Units</IonLabel>
        </IonListHeader>
        {unitsTypes.map((units) => (
          <IonItem key={units}>
            <IonLabel>{units}</IonLabel>
            <IonRadio slot="start" value={units} />
          </IonItem>
        ))}
      </IonRadioGroup>
    );
  }
}
