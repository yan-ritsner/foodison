import * as React from 'react';
import { IonLabel, IonListHeader, IonItem } from '@ionic/react';
import { xo } from '../../observable';
import { ViewProps } from '../../props';
import { AnthropometryViewModel } from './AnthropometryViewModel';

/** Anthropometry BMR component props */
export interface AnthropometryBmrProps
  extends ViewProps<AnthropometryViewModel> {}

/** Anthropometry BMR component */
@xo.observer
export class AnthropometryBmr extends React.Component<AnthropometryBmrProps> {
  render() {
    const viewModel = this.props.viewModel;
    if (!viewModel.bmr) return '';
    const label = `Daily Calorie Needs: ${viewModel.bmr} Cal.`;
    return (
      <>
        <IonListHeader>
          <IonLabel>BMR</IonLabel>
        </IonListHeader>
        <IonItem>
          <IonLabel>{label}</IonLabel>
        </IonItem>
      </>
    );
  }
}
