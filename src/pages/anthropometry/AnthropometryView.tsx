import * as React from 'react';
import {
  IonContent,
  IonList,
  IonLabel,
  IonListHeader,
  IonCard,
} from '@ionic/react';
import { xo } from '../../observable';
import { ViewProps } from '../../props';
import { AnthropometryBmr } from './AnthropometryBmr';
import { AnthropometryViewModel } from './AnthropometryViewModel';
import { AnthropometryGender } from './AnthropometryGender';
import { AnthropometryUnits } from './AnthropometryUnits';
import { AnthropometryActivity } from './AnthropometryActivity';
import { AnthropometryDetails } from './AnthropometryDetails';
import { AnthropometryAlert } from './AnthropometryAlert';
import { AnthropometryAcceptButton } from './AnthropometryAcceptButton';

import './Anthropometry.css';

/** Anthropometry View component props */
export interface AnthropometryViewProps
  extends ViewProps<AnthropometryViewModel> {}

/** Anthropometry View component */
@xo.observer
export class AnthropometryView extends React.Component<AnthropometryViewProps> {
  render() {
    const viewModel = this.props.viewModel;
    return (
      <IonContent class="app-background">
        <IonCard>
          <IonList lines="none">
            <AnthropometryBmr viewModel={viewModel}></AnthropometryBmr>
            <AnthropometryGender viewModel={viewModel}></AnthropometryGender>
            <AnthropometryUnits viewModel={viewModel}></AnthropometryUnits>
            <IonListHeader>
              <IonLabel>Anthropometry</IonLabel>
            </IonListHeader>
            <AnthropometryDetails viewModel={viewModel}></AnthropometryDetails>
            <AnthropometryActivity
              viewModel={viewModel}
            ></AnthropometryActivity>
          </IonList>
        </IonCard>
        <AnthropometryAcceptButton
          viewModel={viewModel}
        ></AnthropometryAcceptButton>
        <AnthropometryAlert viewModel={viewModel}></AnthropometryAlert>
      </IonContent>
    );
  }
}
