import * as React from 'react';
import {
  IonItem,
  IonLabel,
  IonRadioGroup,
  IonListHeader,
  IonRadio,
} from '@ionic/react';
import { xo } from '../../observable';
import { ViewProps } from '../../props';
import { activityTypes } from '../../enums';
import { AnthropometryViewModel } from './AnthropometryViewModel';

/** Anthropometry Activity component props */
export interface AnthropometryActivityProps
  extends ViewProps<AnthropometryViewModel> {}

/** Anthropometry Activity component */
@xo.observer
export class AnthropometryActivity extends React.Component<
  AnthropometryActivityProps
> {
  render() {
    const viewModel = this.props.viewModel;
    return (
      <IonRadioGroup
        value={viewModel.activity}
        onIonChange={(e) => viewModel.setActivity(e.detail.value)}
      >
        <IonListHeader>
          <IonLabel>Regular Activity (Non Workout)</IonLabel>
        </IonListHeader>
        {activityTypes.map((activity) => (
          <IonItem key={activity}>
            <IonLabel>{activity}</IonLabel>
            <IonRadio slot="start" value={activity} />
          </IonItem>
        ))}
      </IonRadioGroup>
    );
  }
}
