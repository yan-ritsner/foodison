import * as React from 'react';
import { IonAlert } from '@ionic/react';
import { xo } from '../../observable';
import { ViewProps } from '../../props';
import { AnthropometryViewModel } from './AnthropometryViewModel';

/** Anthropometry Alert component props */
export interface AnthropometryAlertProps
  extends ViewProps<AnthropometryViewModel> {}

/** Anthropometry Alert component */
@xo.observer
export class AnthropometryAlert extends React.Component<
  AnthropometryAlertProps
> {
  render() {
    const viewModel = this.props.viewModel;
    return (
      <IonAlert
        isOpen={viewModel.alert}
        onDidDismiss={() => viewModel.setAlert(false)}
        message={viewModel.alertText}
        buttons={[
          {
            text: 'Ok',
            handler: () => {
              viewModel.setAlert(false);
            },
          },
        ]}
      />
    );
  }
}
