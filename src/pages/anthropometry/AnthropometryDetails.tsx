import * as React from 'react';
import { IonItem, IonLabel, IonInput, IonDatetime } from '@ionic/react';
import { xo } from '../../observable';
import { ViewProps } from '../../props';
import { AnthropometryViewModel } from './AnthropometryViewModel';

/** Anthropometry Details component props */
export interface AnthropometryDetailsProps
  extends ViewProps<AnthropometryViewModel> {}

/** Anthropometry View component */
@xo.observer
export class AnthropometryDetails extends React.Component<
  AnthropometryDetailsProps
> {
  render() {
    const viewModel = this.props.viewModel;
    return (
      <>
        <IonItem>
          <IonLabel>Date of birth</IonLabel>
          <IonDatetime
            displayFormat="MMM DD, YYYY"
            placeholder="Select Date"
            value={viewModel.dob?.toString()}
            onIonChange={(e) => {
              if (e.detail.value) {
                viewModel.setDob(new Date(e.detail.value));
              }
            }}
          ></IonDatetime>
        </IonItem>
        <IonItem>
          <IonLabel>{`Height ${viewModel.heightUnits}`}</IonLabel>
          <IonInput
            type={'number'}
            step={'1'}
            min={'0'}
            max={'250'}
            className="ion-text-end"
            placeholder="Input Height"
            value={viewModel.height}
            onIonChange={(e) => {
              const num = parseInt(e.detail.value!, 0);
              if (!Number.isNaN(num)) {
                viewModel.setHeight(parseInt(e.detail.value!, 0));
              }
            }}
            onKeyPress={(e) => {
              if (e.key === 'Enter') {
                (e.target as any).blur();
              }
            }}
          ></IonInput>
        </IonItem>
        <IonItem>
          <IonLabel>{`Weight ${viewModel.weightUnits}`}</IonLabel>
          <IonInput
            type={'number'}
            step={'1'}
            min={'0'}
            max={'250'}
            className="ion-text-end"
            placeholder="Input Weigth"
            value={viewModel.weight}
            onIonChange={(e) => {
              const num = parseInt(e.detail.value!, 0);
              if (!Number.isNaN(num)) {
                viewModel.setWeight(parseInt(e.detail.value!, 0));
              }
            }}
            onKeyPress={(e) => {
              if (e.key === 'Enter') {
                (e.target as any).blur();
              }
            }}
          ></IonInput>
        </IonItem>
      </>
    );
  }
}
