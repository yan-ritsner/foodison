import { xo } from '../../observable';
import { System } from '../../system';
import { PageViewModel } from '../page';
import { GenderType, UnitsType, ActivityType, UnitsFactor } from '../../enums';
import { Anthropometry } from '../../data';

/**
 * Anthropometry view model
 */
export class AnthropometryViewModel extends PageViewModel {
  /** Gender */
  @xo.observable
  public gender: GenderType;

  /** Units */
  @xo.observable
  public units: UnitsType;

  /** Activity */
  @xo.observable
  public activity: ActivityType;

  /** Date of birth */
  @xo.observable
  public dob: Date;

  /** Height */
  @xo.observable
  public height: number;

  /** Weight */
  @xo.observable
  public weight: number;

  /** Has changes */
  @xo.observable
  public hasChanges: boolean;

  /** Alert is visible  */
  @xo.observable
  public alert: boolean;

  /** Alert text */
  @xo.observable
  public alertText: string;

  /** BMR value */
  @xo.observable
  public bmr: number;

  /** Height units */
  @xo.computed
  public get heightUnits(): string {
    switch (this.units) {
      case UnitsType.Metric:
        return 'cm.';
      case UnitsType.English:
        return 'in.';
      default:
        return '';
    }
  }
  /** Weight units */
  @xo.computed
  public get weightUnits(): string {
    switch (this.units) {
      case UnitsType.Metric:
        return 'kg.';
      case UnitsType.English:
        return 'lbs.';
      default:
        return '';
    }
  }

  /**
   * @constructor
   */
  public constructor() {
    super();
    this.units = UnitsType.Metric;
    this.activity = ActivityType.Sedentary;
    this.hasChanges = false;
    this.alert = false;
    this.setSettings();
  }

  /** Saves settings */
  public async saveSettings() {
    const errors = this.validateSettings();
    if (errors.length > 0) {
      this.setAlert(true, errors.join('<br>'));
      return;
    }

    const settings = this.getSettings();
    this.bmr = settings.bmr;

    System.user.setAnthropometry(settings);
    this.setHasChanges(false);

    if (this.history) this.history.goBack();
  }

  /** Sets settings data */
  @xo.action
  private setSettings() {
    const settings = System.user.anthropometry;
    if (!settings) return;
    this.gender = settings.gender;
    this.units = settings.units;
    this.activity = settings.activity;
    this.dob = settings.dob;
    this.height = settings.height;
    this.weight = settings.weight;
    this.bmr = settings.bmr;
  }

  /** Gets settings data */
  private getSettings(): Anthropometry {
    const settings = new Anthropometry({
      gender: this.gender,
      units: this.units,
      activity: this.activity,
      dob: this.dob,
      height: this.height,
      weight: this.weight,
    });
    return settings;
  }

  /** Validates settings */
  private validateSettings() {
    const errors: string[] = [];
    if (this.gender == null) {
      errors.push('Gender is not specified');
    }
    if (this.dob == null) {
      errors.push('Date of birth is not specified');
    }
    if (this.height == null) {
      errors.push('Height is not specified');
    }
    if (this.weight == null) {
      errors.push('Weight is not specified');
    }
    return errors;
  }

  /** Sets gender */
  @xo.action
  public setGender(gender: GenderType) {
    this.gender = gender;
    this.hasChanges = true;
  }

  /** Sets units */
  @xo.action
  public setUnits(units: UnitsType) {
    this.units = units;
    this.hasChanges = true;
    switch (units) {
      case UnitsType.Metric:
        if (this.height) {
          this.height = Math.round(this.height / UnitsFactor.HeightCmToIn);
        }
        if (this.weight) {
          this.weight = Math.round(this.weight / UnitsFactor.WeightKgToLb);
        }
        break;
      case UnitsType.English:
        if (this.height) {
          this.height = Math.round(this.height * UnitsFactor.HeightCmToIn);
        }
        if (this.weight) {
          this.weight = Math.round(this.weight * UnitsFactor.WeightKgToLb);
        }
    }
  }

  /** Sets units */
  @xo.action
  public setActivity(activity: ActivityType) {
    this.activity = activity;
    this.hasChanges = true;
  }

  /** Sets date of birth */
  @xo.action
  public setDob(dob: Date) {
    this.dob = dob;
    this.hasChanges = true;
  }

  /** Sets height */
  @xo.action
  public setHeight(height: number) {
    this.height = height;
    this.hasChanges = true;
  }

  /** Sets weight */
  @xo.action
  public setWeight(weight: number) {
    this.weight = weight;
    this.setHasChanges(true);
  }

  /** Sets has changes */
  @xo.action
  public setHasChanges(hasChanges: boolean) {
    this.hasChanges = hasChanges;
  }

  /** Shows/hides alert */
  @xo.action
  public setAlert(show: boolean, text: string = '') {
    this.alert = show;
    this.alertText = text;
  }
}
