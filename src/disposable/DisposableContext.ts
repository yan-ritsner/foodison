import { Observable } from '../observable';
import { IDisposable } from './IDisposable';
import { IDisposableContext } from './IDisposableContext';

/**
 * Disposable context implementation
 */
export class DisposableContext implements IDisposableContext {
  /** Disposable object list */
  private disposables: IDisposable[] = [];
  /** Previous disposable context */
  private prevContext: IDisposableContext | null;

  /**
   * @constructor
   */
  public constructor() {
    this.disposables = [];
  }

  /** Begins context - begins tracking disposables */
  public begin() {
    this.prevContext = Observable.disposableContext;
    Observable.disposableContext = this;
  }

  /** Ends context - ends tracking disposables */
  public end() {
    Observable.disposableContext = this.prevContext;
  }

  /**
   * Adds disposable to current context
   * @param disposable - disposable
   */
  public addDisposable(disposable: IDisposable): void {
    this.disposables.push(disposable);
  }

  /**
   * Disposes all disposables in the context
   **/
  public dispose() {
    this.disposables.forEach((disposable) => disposable.dispose());
    this.disposables = [];
  }
}
