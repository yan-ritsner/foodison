import { IDisposable } from './IDisposable';

/**
 * Disposable context.
 * Stores disposables and disposes them when the context is being disposed.
 **/
export interface IDisposableContext extends IDisposable {
  /**
   * Adds disposable object to the context
   * @param disposable - disposable object
   */
  addDisposable(disposable: IDisposable): void;
}
