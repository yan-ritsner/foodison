/** Interface for disposable objects */
export interface IDisposable {
  /** Disposes object */
  dispose(): any;
}
