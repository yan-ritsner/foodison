import { xo } from './xo';

export class ObservableTest {
  @xo.observable
  o1: number = 0;
  @xo.observable
  o2: number = 0;
  @xo.observable
  o3: number = 0;

  @xo.computed
  get c1(): number {
    return this.o1 + this.o2;
  }

  @xo.computed
  get c2(): number {
    return this.c1 + this.o3;
  }

  constructor() {
    xo.subscribe(this, 'c1', v => console.log(`c1:${v}`));
    xo.subscribe(this, 'c2', v => console.log(`c2:${v}`));
  }

  @xo.action
  test1() {
    this.o1 = 1;
    this.o2 = 1;
    this.o3 = 1;
  }

  @xo.action
  test2() {
    this.o1 = 2;
    this.o2 = 2;
    this.o3 = 2;
  }
}
