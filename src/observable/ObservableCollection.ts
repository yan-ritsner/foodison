import { xo } from './xo';
import { ObservableArray } from './ObservableArray';

/** Stores observable collection entry */
export class Entry<TV = any> {
  /** Entry value */
  @xo.observable
  value: TV;
  /** Entry index in the array */
  index: number;
  /** Previous entry in the array */
  prev?: Entry<TV>;
  /** Next entry in the array */
  next?: Entry<TV>;

  /**
   * @constructor
   * @param value - entry value
   * @param index - entry index
   */
  constructor(value: TV, index: number) {
    this.value = value;
    this.index = index;
  }
}

/**
 * Collection that allows to observe collection changes
 * but also to get, update and delete items by key.
 *
 * Stores items in an internal observable array,
 * also stores in a map by key, index of the item in the original array
 * and pointers to the next and prev item.
 *
 * Add and Update operations Efficiency = O(1).
 * Delete operation efficiency = O(n).
 */
export class ObservableCollection<TK = any, TV = any> {
  /** Data observable array */
  data = new ObservableArray<TV>();
  /** Data map: [Key-> CollectionEntry] */
  @xo.observable
  dataMap = new Map<TK, Entry>();
  /** Item key getter callback */
  keyGetter: (value: TV) => TK;

  /**
   * @constructor
   * @param keyGetter - item key getter callback
   */
  constructor(keyGetter: (value: TV) => TK) {
    this.keyGetter = keyGetter;
  }

  /**
   * Clears collection
   */
  clear() {
    this.data.clear();
    this.dataMap.clear();
    xo.notify(this, 'dataMap');
  }

  /**
   * Return true if specified item key exist in the collection
   * @param item - item to check
   */
  exists(item: TV): any {
    const key = this.keyGetter(item);
    return this.get(key) != null;
  }

  /**
   * Gets item by key
   * @param key - item key
   */
  get(key: TK): TV | null {
    const entry = this.dataMap.get(key);
    return entry != null ? entry.value : null;
  }

  /**
   * Adds value to the collection.
   * If item with the same key already exist, it will be updated.
   * @param value - item value
   * @param insertIndex - optional insertion index, last if not set
   */
  add(value: TV, insertIndex?: number) {
    const key = this.keyGetter(value);
    this.addByKey(key, value, insertIndex);
  }

  /**
   * Updates value in the collection
   * @param value - item value
   */
  update(value: TV) {
    const key = this.keyGetter(value);
    this.updateByKey(key, value);
  }

  /**
   * Deletes value from the collection
   * @param value - item value
   */
  delete(value: TV) {
    const key = this.keyGetter(value);
    this.deleteByKey(key);
  }

  /**
   * Adds value to the collection by key
   * @param key - item key
   * @param value - item value
   * @param insertIndex - optional insertion index, last if not set
   */
  addByKey(key: TK, value: TV, insertIndex?: number) {
    // key already exists - update
    if (this.dataMap.has(key)) {
      this.update(value);
      return;
    }
    const index = insertIndex != null ? insertIndex : this.data.length;
    const entry = new Entry(value, index);
    // assign prev and next entries fields
    if (index > 0) {
      const prevValue = this.data.get(index - 1);
      const prevKey = this.keyGetter(prevValue);
      const prev = this.dataMap.get(prevKey);
      if (prev) {
        entry.prev = prev;
        prev.next = entry;
      }
    }
    if (index <= this.data.length - 1) {
      const nextValue = this.data.get(index);
      const nextKey = this.keyGetter(nextValue);
      const next = this.dataMap.get(nextKey);
      if (next) {
        entry.next = next;
        next.prev = entry;
      }
    }
    // store
    this.dataMap.set(key, entry);
    if (index === this.data.length) {
      this.data.push(value);
    } else {
      this.data.splice(index, 0, value);
    }
    // update indexes
    let next = entry.next;
    let nextIndex = index + 1;
    while (next != null) {
      next.index = nextIndex;
      next = next.next;
      nextIndex = nextIndex + 1;
    }
    xo.notify(this, 'dataMap');
  }

  /**
   *w
   * @param key
   * @param value
   */
  updateByKey(key: TK, value: TV) {
    const entry = this.dataMap.get(key);
    if (entry != null) {
      entry.value = value;
      this.data.set(entry.index, entry.value);
    }
  }

  /**
   * Deletes value from the collection by key
   * @param key - item key
   */
  deleteByKey(key: TK) {
    const entry = this.dataMap.get(key);
    if (entry == null) return;
    // update prev and next
    let next = entry.next;
    const prev = entry.prev;
    if (next) next.prev = prev;
    if (prev) prev.next = next;
    // update indexes
    while (next) {
      next.index -= 1;
      next = next.next;
    }
    // update stores
    this.dataMap.delete(key);
    this.data.splice(entry.index, 1);
    xo.notify(this, 'dataMap');
  }

  /**
   * Adds multiple values to the collection as transaction.
   * @param values - array of values
   */
  @xo.action
  addRange(values: TV[]) {
    values.forEach(value => this.add(value));
  }

  /**
   * Deletes all values that match predicate
   * @param predicate - predicate callback
   */
  @xo.action
  deleteRange(predicate: (value: TV) => boolean) {
    // remove all matching values
    for (let index = this.data.length - 1; index >= 0; index -= 1) {
      const value = this.data.get(index);
      const remove = predicate(value);
      if (remove) this.data.splice(index, 1);
    }
    // rebuild index map
    this.rebuildMap();
  }

  /**
   * Rebuilds index map
   */
  rebuildMap() {
    this.dataMap.clear();
    let prev: Entry<TV> | null = null;
    for (let index = 0; index < this.data.length; index += 1) {
      const value = this.data.get(index);
      const key = this.keyGetter(value);
      const entry = new Entry(value, index);
      this.dataMap.set(key, entry);
      if (prev != null) {
        prev.next = entry;
        entry.prev = prev;
      }
      prev = entry;
    }
    xo.notify(this, 'dataMap');
  }
}
