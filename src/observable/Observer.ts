import { ComputedData } from './ComputedData';
import { Fields } from './Fields';
import { Observable } from './Observable';

/**
 * Static observer component decorator implementation.
 */
export class Observer {
  /**
   * @xo.observer decorator implementation.
   * Overrides component renderer function the way that it is aromatically re-renders on
   * changes of any observables accessed inside original render.
   * @param constructor - component class constructor function
   */
  static observer(constructor: Function): any {
    const proto = constructor.prototype;
    const render = proto.render;
    const componentWillUnmount = proto.componentWillUnmount;

    // Overrides component render function
    // Wraps original render into computed value - calls force refresh on change
    proto.render = function () {
      let computedRender = this[Fields.ComputedRender] as ComputedData;
      if (!computedRender) {
        computedRender = Observable.reaction(render.bind(this), () => {
          this.forceUpdate();
        });
        this[Fields.ComputedRender] = computedRender;
      } else {
        computedRender.compute(true);
      }
      return computedRender.value;
    };

    // Overrides component unmount function - disposes computed wrapper
    proto.componentWillUnmount = function () {
      const computedRender = this[Fields.ComputedRender] as ComputedData;
      if (computedRender) computedRender.dispose();
      if (componentWillUnmount) componentWillUnmount.call(this);
      this[Fields.ComputedRender] = null;
    };

    // Overrides component should update function - uses props shallow equality compare
    proto.shouldComponentUpdate = function (nextProps: any, nextState: any) {
      if (this.state !== nextState) return true;
      const update = !Observer.shallowEqual(this.props, nextProps);
      return update;
    };
  }

  /** Shallow equality comparer */
  static shallowEqual(objA: any, objB: any) {
    if (Observer.is(objA, objB)) return true;
    if (
      typeof objA !== 'object' ||
      objA === null ||
      typeof objB !== 'object' ||
      objB === null
    ) {
      return false;
    }
    const keysA = Object.keys(objA);
    const keysB = Object.keys(objB);
    if (keysA.length !== keysB.length) return false;
    for (let i = 0; i < keysA.length; i += 1) {
      if (
        !Object.hasOwnProperty.call(objB, keysA[i]) ||
        !Observer.is(objA[keysA[i]], objB[keysA[i]])
      ) {
        return false;
      }
    }
    return true;
  }

  /** Returns true if x and y are actually same objects */
  static is(x: any, y: any) {
    if (x === y) {
      return x !== 0 || 1 / x === 1 / y;
    }
    // eslint-disable-next-line
    return x !== x && y !== y;
  }
}
