export enum CleanUpMode {
  DeleteAllData = 'Delete all data',
  DeleteAllPhotos = 'Delete all photos',
  DeleteDataMonthOld = 'Delete data older than a month',
  DeletePhotoMonthOld = 'Delete photos older than a month',
  DeleteDataWeekOld = 'Delete data older than a week',
  DeletePhotoWeekOld = 'Delete photos older than a week',
}

export const cleanUpModes = [
  CleanUpMode.DeleteAllData,
  CleanUpMode.DeleteAllPhotos,
  CleanUpMode.DeleteDataMonthOld,
  CleanUpMode.DeletePhotoMonthOld,
  CleanUpMode.DeleteDataWeekOld,
  CleanUpMode.DeletePhotoWeekOld,
];
