/** Units system type */
export enum UnitsType {
  Metric = 'Metric (kg/cm)',
  English = 'English (lbs/in)',
}

/** Units system types */
export const unitsTypes = [UnitsType.Metric, UnitsType.English];
