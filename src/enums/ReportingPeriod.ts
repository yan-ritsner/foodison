/** Reporting period enum */
export enum ReportingPeriod {
  Day = 'Day',
  Week = 'Week',
  Month = 'Month',
}

/** Reporting periods list */
export const reportingPeriods = [
  ReportingPeriod.Day,
  ReportingPeriod.Week,
  ReportingPeriod.Month,
];
