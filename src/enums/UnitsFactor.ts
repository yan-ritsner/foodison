/** Various conversion factors */
export enum UnitsFactor {
  /** Factor to convert Cm. to In. */
  HeightCmToIn = 0.394,
  /** Factor to convert Kg to Lb. */
  WeightKgToLb = 2.20462,
}
