/** Gender type */
export enum GenderType {
  Female = 'Female',
  Male = 'Male',
}
/** Gender types list */
export const genderTypes = [GenderType.Female, GenderType.Male];
