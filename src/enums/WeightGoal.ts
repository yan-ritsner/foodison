/** Weight goal enum */
export enum WeightGoal {
  Lose = 'Lose',
  Maintain = 'Maintain',
  Gain = 'Gain',
}

/** Weight goals list */
export const weightGoals = [
  WeightGoal.Lose,
  WeightGoal.Maintain,
  WeightGoal.Gain,
];
