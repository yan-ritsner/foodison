/** Data input source */
export enum DataSource {
  /** By taking a photo */
  Photo,
  /** By scanning barcode */
  Barcode,
  /** By voice recognition */
  Voice,
  /** Manual input */
  Input,
}
