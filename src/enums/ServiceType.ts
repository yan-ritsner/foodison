/**
 * System service types available
 */
export enum ServiceType {
  /** User settings service */
  UserSettings,
  /** Photo storage service */
  PhotoStorage,
  /** Photo classifier service */
  PhotoClassifier,
  /** Food prediction service */
  FoodPrediction,
  /** Nutrition data service */
  FoodNutrition,
  /** Food data service */
  FoodData,
  /** Activity data service */
  ActivityData,
  /** Data input service */
  DataInput,
  /** Google analytics service  */
  GoogleAnalytics,
  /** Subscription service  */
  Subscribe,
}
