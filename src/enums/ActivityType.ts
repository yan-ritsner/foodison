/** Activity type */
export enum ActivityType {
  None = 'None',
  Sedentary = 'Sedentary',
  LightActivity = 'Light Activity',
  ModerateActivity = 'Moderate Activity',
  VeryActive = 'Very Active',
  ExtraActive = 'Extra Active',
}
/** Activity types list */
export const activityTypes = [
  ActivityType.None,
  ActivityType.Sedentary,
  ActivityType.LightActivity,
  ActivityType.ModerateActivity,
  ActivityType.VeryActive,
  ActivityType.ExtraActive,
];
