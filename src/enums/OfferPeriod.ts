/** Offering period */
export enum OfferPeriod {
  Week = 'WEEK',
  Month = 'MONTH',
  Year = 'YEAR',
}
